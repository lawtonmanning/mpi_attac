/** \copyright
 * Copyright 2021 Hussam Al Daas, Grey Ballard, and Peter Benner
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/** \file timer.c
 * \author Hussam Al Daas
 * \brief timer
 * \date 01/10/2019
 * \details 
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include "timer.h"

attacTimer _Timer = {.t = NULL, .n = 0, .maxn = 100};
#if TIME_BREAKDOWN
tsqrTimer _TSQRTimer = {.comm = NULL, .compLeaf = NULL, .compTree = NULL, .idle = NULL, .idx = 0, .size = 1000};
tsqrTimer _TSQRTimerRoot = {.comm = NULL, .compLeaf = NULL, .compTree = NULL, .idle = NULL, .idx = 0, .size = 1000};
tsqrTimer _ApplyTSQRTimer = {.comm = NULL, .compLeaf = NULL, .compTree = NULL, .idle = NULL, .idx = 0, .size = 1000};
tsqrTimer _ApplyTSQRTimerRoot = {.comm = NULL, .compLeaf = NULL, .compTree = NULL, .idle = NULL, .idx = 0, .size = 1000};
bcastTimer _BCASTTimer = {.bcast = 0};
compTimer _COMPTimer = {.comp = 0};
#endif

void attacInitTimer(){
	_Timer.t = (attacFuncTimer**) malloc(_Timer.maxn * sizeof(attacFuncTimer*));
#if TIME_BREAKDOWN
	int rank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	_TSQRTimer.comm     = (double*) malloc(_TSQRTimer.size  * sizeof(double));
	_TSQRTimer.compLeaf = (double*) malloc(_TSQRTimer.size  * sizeof(double));
	_TSQRTimer.compTree = (double*) malloc(_TSQRTimer.size  * sizeof(double));
	_TSQRTimer.idle     = (double*) malloc(_TSQRTimer.size  * sizeof(double));
	_ApplyTSQRTimer.comm     = (double*) malloc(_ApplyTSQRTimer.size  * sizeof(double));
	_ApplyTSQRTimer.compLeaf = (double*) malloc(_ApplyTSQRTimer.size  * sizeof(double));
	_ApplyTSQRTimer.compTree = (double*) malloc(_ApplyTSQRTimer.size  * sizeof(double));
	_ApplyTSQRTimer.idle     = (double*) malloc(_ApplyTSQRTimer.size  * sizeof(double));

	if(rank == 0){
		_TSQRTimerRoot.comm     = (double*) malloc(_TSQRTimerRoot.size * sizeof(double));
		_TSQRTimerRoot.compLeaf = (double*) malloc(_TSQRTimerRoot.size * sizeof(double));
		_TSQRTimerRoot.compTree = (double*) malloc(_TSQRTimerRoot.size * sizeof(double));
		_TSQRTimerRoot.idle     = (double*) malloc(_TSQRTimerRoot.size * sizeof(double));
		_ApplyTSQRTimerRoot.comm     = (double*) malloc(_ApplyTSQRTimerRoot.size * sizeof(double));
		_ApplyTSQRTimerRoot.compLeaf = (double*) malloc(_ApplyTSQRTimerRoot.size * sizeof(double));
		_ApplyTSQRTimerRoot.compTree = (double*) malloc(_ApplyTSQRTimerRoot.size * sizeof(double));
		_ApplyTSQRTimerRoot.idle     = (double*) malloc(_ApplyTSQRTimerRoot.size * sizeof(double));
	}
#endif
}

void attacFinalizeTimer(){
	free(_Timer.t);
#if TIME_BREAKDOWN
	int rank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if(rank == 0){
		free(_TSQRTimerRoot.comm);
		free(_TSQRTimerRoot.compLeaf);
		free(_TSQRTimerRoot.compTree);
		free(_TSQRTimerRoot.idle);
		free(_ApplyTSQRTimerRoot.comm);
		free(_ApplyTSQRTimerRoot.compLeaf);
		free(_ApplyTSQRTimerRoot.compTree);
		free(_ApplyTSQRTimerRoot.idle);
	}
	free(_TSQRTimer.comm);
	free(_TSQRTimer.compLeaf);
	free(_TSQRTimer.compTree);
	free(_TSQRTimer.idle);
	free(_ApplyTSQRTimer.comm);
	free(_ApplyTSQRTimer.compLeaf);
	free(_ApplyTSQRTimer.compTree);
	free(_ApplyTSQRTimer.idle);
#endif
}

void attacResetTimer(){
	for(int i = 0; i < _Timer.n; ++i){
		memset(_Timer.t[i]->ncall, 0, MAXNTIC * sizeof(double));
		memset(_Timer.t[i]->subTimer, 0, MAXNTIC * sizeof(double));
	}
#if TIME_BREAKDOWN
	int rank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	memset(_TSQRTimer.comm, 0, _TSQRTimer.size * sizeof(double));
	memset(_TSQRTimer.compLeaf, 0, _TSQRTimer.size * sizeof(double));
	memset(_TSQRTimer.compTree, 0, _TSQRTimer.size * sizeof(double));
	memset(_TSQRTimer.idle, 0, _TSQRTimer.size * sizeof(double));
	memset(_ApplyTSQRTimer.comm, 0, _ApplyTSQRTimer.size * sizeof(double));
	memset(_ApplyTSQRTimer.compLeaf, 0, _ApplyTSQRTimer.size * sizeof(double));
	memset(_ApplyTSQRTimer.compTree, 0, _ApplyTSQRTimer.size * sizeof(double));
	memset(_ApplyTSQRTimer.idle, 0, _ApplyTSQRTimer.size * sizeof(double));
	_BCASTTimer.bcast = 0;
	_COMPTimer.comp = 0;

	if(rank == 0){
		memset(_TSQRTimerRoot.comm,     0, _TSQRTimerRoot.size * sizeof(double));
		memset(_TSQRTimerRoot.compLeaf, 0, _TSQRTimerRoot.size * sizeof(double));
		memset(_TSQRTimerRoot.compTree, 0, _TSQRTimerRoot.size * sizeof(double));
		memset(_TSQRTimerRoot.idle,     0, _TSQRTimerRoot.size * sizeof(double));
		memset(_ApplyTSQRTimerRoot.comm,     0, _ApplyTSQRTimerRoot.size * sizeof(double));
		memset(_ApplyTSQRTimerRoot.compLeaf, 0, _ApplyTSQRTimerRoot.size * sizeof(double));
		memset(_ApplyTSQRTimerRoot.compTree, 0, _ApplyTSQRTimerRoot.size * sizeof(double));
		memset(_ApplyTSQRTimerRoot.idle,     0, _ApplyTSQRTimerRoot.size * sizeof(double));
	}
	_TSQRTimer.idx = 0;
	_TSQRTimerRoot.idx = 0;
	_ApplyTSQRTimer.idx = 0;
	_ApplyTSQRTimerRoot.idx = 0;
#endif
}

void attacAddTimer(attacFuncTimer* t){
	if(_Timer.n == _Timer.maxn){
		_Timer.t = (attacFuncTimer**) realloc(_Timer.t, (_Timer.maxn + 10) * sizeof(attacFuncTimer*));
	}
	_Timer.maxn += 10;
	_Timer.t[_Timer.n] = t;
	_Timer.n++;
}

void attacPrintTime(attacFuncTimer *t, FILE* fd){
  if(t->ncall[0] > 1){
    fprintf(fd, "\t%s_: %f s\t(ncall %d)\n", t->funcName, t->subTimer[0], t->ncall[0]);
  }else if(t->ncall[0] > 0){
    fprintf(fd, "\t%s_: %f s\t(ncall %d)\n", t->funcName, t->subTimer[0], t->ncall[0]);
  }

  for(int i = 1; i < MAXNTIC; ++i){
    if(t->subTimerName[i] && t->ncall[i] > 0){
      if(t->ncall[i] > 1){
        fprintf(fd, "\t\t%s\t%s: %f s\t(ncall %d)\n", t->funcName, t->subTimerName[i], t->subTimer[i], t->ncall[i]);
      }else{
        fprintf(fd, "\t\t%s\t%s: %f s\n", t->funcName, t->subTimerName[i], t->subTimer[i]);
      }
    }
  }
}

void attacPrintTimer(const char* filename){
  FILE* fd = NULL;

	if(filename != NULL){
		fd = fopen(filename, "a");
  }else{
    fd = stdout;
  }
  fprintf(fd,
    "*****************************\n"
    "*            TIMER          *\n"
    "*****************************\n"
    );

  if(_Timer.n){
    for(int i = 0; i < _Timer.n; i++){
      attacPrintTime((_Timer.t[i]), fd);
    }
  }else{
    fprintf(fd, "TIMER : no timer set\n");
  }

	if(filename != NULL){
		fclose(fd);
	}
}


#if TIME_BREAKDOWN
void TSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag){
	int idx = _TSQRTimer.idx;
	if(idx == _TSQRTimer.size){
		_TSQRTimer.comm     = (double*) realloc(_TSQRTimer.comm    , _TSQRTimer.size * 2 * sizeof(double));
		_TSQRTimer.compLeaf = (double*) realloc(_TSQRTimer.compLeaf, _TSQRTimer.size * 2 * sizeof(double));
		_TSQRTimer.compTree = (double*) realloc(_TSQRTimer.compTree, _TSQRTimer.size * 2 * sizeof(double));
		_TSQRTimer.idle     = (double*) realloc(_TSQRTimer.idle    , _TSQRTimer.size * 2 * sizeof(double));
		_TSQRTimer.size *= 2;

		int rank = 0;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		if(rank == 0){
			_TSQRTimerRoot.comm     = (double*) realloc(_TSQRTimerRoot.comm    , _TSQRTimerRoot.size * 2 * sizeof(double));
			_TSQRTimerRoot.compLeaf = (double*) realloc(_TSQRTimerRoot.compLeaf, _TSQRTimerRoot.size * 2 * sizeof(double));
			_TSQRTimerRoot.compTree = (double*) realloc(_TSQRTimerRoot.compTree, _TSQRTimerRoot.size * 2 * sizeof(double));
			_TSQRTimerRoot.idle     = (double*) realloc(_TSQRTimerRoot.idle    , _TSQRTimerRoot.size * 2 * sizeof(double));
			_TSQRTimerRoot.size *= 2;
		}
	}
	_TSQRTimer.comm[idx] += comm;
	_TSQRTimer.compLeaf[idx] += compLeaf;
	_TSQRTimer.compTree[idx] += compTree;
	_TSQRTimer.idle[idx] += idle;
	if(flag) _TSQRTimer.idx++;
}

void ApplyTSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag){
	int idx = _ApplyTSQRTimer.idx;
	if(idx == _TSQRTimer.size){
		_ApplyTSQRTimer.comm     = (double*) realloc(_ApplyTSQRTimer.comm    , _ApplyTSQRTimer.size * 2 * sizeof(double));
		_ApplyTSQRTimer.compLeaf = (double*) realloc(_ApplyTSQRTimer.compLeaf, _ApplyTSQRTimer.size * 2 * sizeof(double));
		_ApplyTSQRTimer.compTree = (double*) realloc(_ApplyTSQRTimer.compTree, _ApplyTSQRTimer.size * 2 * sizeof(double));
		_ApplyTSQRTimer.idle     = (double*) realloc(_ApplyTSQRTimer.idle    , _ApplyTSQRTimer.size * 2 * sizeof(double));
		_ApplyTSQRTimer.size *= 2;

		int rank = 0;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		if(rank == 0){
			_ApplyTSQRTimerRoot.comm     = (double*) realloc(_ApplyTSQRTimerRoot.comm    , _ApplyTSQRTimerRoot.size * 2 * sizeof(double));
			_ApplyTSQRTimerRoot.compLeaf = (double*) realloc(_ApplyTSQRTimerRoot.compLeaf, _ApplyTSQRTimerRoot.size * 2 * sizeof(double));
			_ApplyTSQRTimerRoot.compTree = (double*) realloc(_ApplyTSQRTimerRoot.compTree, _ApplyTSQRTimerRoot.size * 2 * sizeof(double));
			_ApplyTSQRTimerRoot.idle     = (double*) realloc(_ApplyTSQRTimerRoot.idle    , _ApplyTSQRTimerRoot.size * 2 * sizeof(double));
			_ApplyTSQRTimerRoot.size *= 2;
		}
	}
	_ApplyTSQRTimer.comm[idx] += comm;
	_ApplyTSQRTimer.compLeaf[idx] += compLeaf;
	_ApplyTSQRTimer.compTree[idx] += compTree;
	_ApplyTSQRTimer.idle[idx] += idle;
	if(flag) _ApplyTSQRTimer.idx++;
}

void addCOMPTime(double val){
	_COMPTimer.comp += val;
}

void addBCASTTime(double val){
	_BCASTTimer.bcast += val;
}

void printTSQRTimer(const char* filename, MPI_Comm comm){
  FILE* fd = NULL;
	int rank = 0, size = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status status;

	_TSQRTimerRoot.idx = _TSQRTimer.idx;
	if(rank == 0 && filename != NULL){
		fd = fopen(filename, "a");
  }else{
    fd = stdout;
  }
	if(rank == 0){
		fprintf(fd, "****TSQR TIMER****\n%d\n", size);
		for(int i = 0; i < _TSQRTimer.idx; i++){
			fprintf(fd, "%.8f ", _TSQRTimer.comm[i]);
			fprintf(fd, "%.8f ", _TSQRTimer.compLeaf[i]);
			fprintf(fd, "%.8f ", _TSQRTimer.compTree[i]);
			fprintf(fd, "%.8f ", _TSQRTimer.idle[i]);
		}
		fprintf(fd, "\n");
		for(int i = 0; i < _ApplyTSQRTimer.idx; i++){
			fprintf(fd, "%.8f ", _ApplyTSQRTimer.comm[i]);
			fprintf(fd, "%.8f ", _ApplyTSQRTimer.compLeaf[i]);
			fprintf(fd, "%.8f ", _ApplyTSQRTimer.compTree[i]);
			fprintf(fd, "%.8f ", _ApplyTSQRTimer.idle[i]);
		}
		fprintf(fd, "\n");
		fprintf(fd, "%.8f ", _BCASTTimer.bcast);
		fprintf(fd, "\n");
		fprintf(fd, "%.8f ", _COMPTimer.comp);
		fprintf(fd, "\n");
	}

	for(int i = 1; i < size; i++){
		if(rank == i){
			MPI_Send(_TSQRTimer.compLeaf, _TSQRTimer.idx,  MPI_DOUBLE, 0, 1, comm);
			MPI_Send(_TSQRTimer.compTree, _TSQRTimer.idx,  MPI_DOUBLE, 0, 2, comm);
			MPI_Send(_TSQRTimer.comm,     _TSQRTimer.idx,  MPI_DOUBLE, 0, 3, comm);
			MPI_Send(_TSQRTimer.idle,     _TSQRTimer.idx,  MPI_DOUBLE, 0, 4, comm);
			MPI_Send(&_BCASTTimer.bcast,               1,  MPI_DOUBLE, 0, 5, comm);
			MPI_Send(&_COMPTimer.comp,                 1,  MPI_DOUBLE, 0, 6, comm);
			MPI_Send(_ApplyTSQRTimer.compLeaf, _ApplyTSQRTimer.idx,  MPI_DOUBLE, 0, 7, comm);
			MPI_Send(_ApplyTSQRTimer.compTree, _ApplyTSQRTimer.idx,  MPI_DOUBLE, 0, 8, comm);
			MPI_Send(_ApplyTSQRTimer.comm,     _ApplyTSQRTimer.idx,  MPI_DOUBLE, 0, 9, comm);
			MPI_Send(_ApplyTSQRTimer.idle,     _ApplyTSQRTimer.idx,  MPI_DOUBLE, 0, 10, comm);
		}else if(rank == 0){
			MPI_Recv(_TSQRTimerRoot.compLeaf, _TSQRTimer.idx,  MPI_DOUBLE, i, 1, comm, &status);
			MPI_Recv(_TSQRTimerRoot.compTree, _TSQRTimer.idx,  MPI_DOUBLE, i, 2, comm, &status);
			MPI_Recv(_TSQRTimerRoot.comm,     _TSQRTimer.idx,  MPI_DOUBLE, i, 3, comm, &status);
			MPI_Recv(_TSQRTimerRoot.idle,     _TSQRTimer.idx,  MPI_DOUBLE, i, 4, comm, &status);
			MPI_Recv(&_BCASTTimer.bcast,       1            ,  MPI_DOUBLE, i, 5, comm, &status);
			MPI_Recv(&_COMPTimer.comp,         1            ,  MPI_DOUBLE, i, 6, comm, &status);
			MPI_Recv(_ApplyTSQRTimerRoot.compLeaf, _ApplyTSQRTimer.idx,  MPI_DOUBLE, i, 7, comm, &status);
			MPI_Recv(_ApplyTSQRTimerRoot.compTree, _ApplyTSQRTimer.idx,  MPI_DOUBLE, i, 8, comm, &status);
			MPI_Recv(_ApplyTSQRTimerRoot.comm,     _ApplyTSQRTimer.idx,  MPI_DOUBLE, i, 9, comm, &status);
			MPI_Recv(_ApplyTSQRTimerRoot.idle,     _ApplyTSQRTimer.idx,  MPI_DOUBLE, i, 10, comm, &status);
			for(int i = 0; i < _TSQRTimer.idx; i++){
				fprintf(fd, "%.8f ", _TSQRTimerRoot.comm[i]);
				fprintf(fd, "%.8f ", _TSQRTimerRoot.compLeaf[i]);
				fprintf(fd, "%.8f ", _TSQRTimerRoot.compTree[i]);
				fprintf(fd, "%.8f ", _TSQRTimerRoot.idle[i]);
			}
			fprintf(fd, "\n");
			for(int i = 0; i < _ApplyTSQRTimer.idx; i++){
				fprintf(fd, "%.8f ", _ApplyTSQRTimerRoot.comm[i]);
				fprintf(fd, "%.8f ", _ApplyTSQRTimerRoot.compLeaf[i]);
				fprintf(fd, "%.8f ", _ApplyTSQRTimerRoot.compTree[i]);
				fprintf(fd, "%.8f ", _ApplyTSQRTimerRoot.idle[i]);
			}
			fprintf(fd, "\n");
			fprintf(fd, "%.8f ", _BCASTTimer.bcast);
			fprintf(fd, "\n");
			fprintf(fd, "%.8f ", _COMPTimer.comp);
			fprintf(fd, "\n");
		}
	}

	if(rank == 0 && filename != NULL){
		fclose(fd);
	}
	MPI_Barrier(comm);
}
#else
void TSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag){}
void ApplyTSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag){}
void addCOMPTime(double val){}
void addBCASTTime(double val){}
void printTSQRTimer(const char* filename, MPI_Comm comm){}
#endif
