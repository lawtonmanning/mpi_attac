/** \copyright
 * Copyright 2021 Hussam Al Daas, Grey Ballard, and Peter Benner
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/** \file pattac.h
 * \author Hussam Al Daas
 * \brief MPI Algorithms for Tensor Train Arithmetic and Computations
 * \date 16/07/2019
 * \details 
*/
#ifndef PATTAC_H
#define PATTAC_H

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

extern void dgeqlf_(const int*, const int*, double*, const int*, double*, double*, const int*, int*);
extern double dlange_(const char*, const int*, const int*, const double*, const int*, double*);
extern void dlacpy_(const char*, const int*, const int*, const double*, const int*, double*, const int*);
extern void dtrmm_(const char*, const char*, const char*, const char*, const int*, const int*, const double*, double*, const int*, double*, const int*);
extern void dgemv_(const char*, const int*, const int*, double*, double*, const int*, double*, const int*, double*, double*, const int*);
extern void dgemm_(const char*, const char*, const int*, const int*, const int*, const double*, const double*, const int*, const double*, const int*, const double*, double*, const int*);
extern void dsyrk_(const char*, const char*, const int*, const int*, const double*, const double*, const int*, const double*, double*, const int*);
extern void dgesvd_(const char*, const char*, const int*, const int*, double*, const int*, double*, double*, const int*, double*, const int*, double*, const int*, int*);
extern int LAPACKE_dgesvd(const int, const char, const char, const int, const int, double*, const int, double*, double*, int, double*, const int, double*);
#define LAPACK_COL_MAJOR 102

void pattacScale(const double alpha, int mode, const int d, const int* n, const int* rx, double **x);
double pattacValue(const MPI_Comm comm, const int* index, const int* procHolder, const int d, const int* n, const int* rx, double *const *x, double* work);

void pattacFormalSum(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z);
void pattacFormalAXPBY(const int d, const int *n, const double alpha, const int *rx, double * const *x, const double beta, const int *ry, double *const *y, int* rz, double **z);
void pattacHadamard(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z);

double pattacNormFromHadamard(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* work);
double pattacNorm_2(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* workd, int* worki);
double pattacNorm(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
double pattacNormOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);

void pattacLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
void pattacLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
void pattacROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
void pattacRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
void pattacCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);

void pattacBFLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
void pattacBFLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
void pattacBFROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
void pattacBFRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
void pattacBFCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);

void pattacLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
void pattacLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
void pattacROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
void pattacRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
void pattacCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);

void pattacBFLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
void pattacBFLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
void pattacBFROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
void pattacBFRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
void pattacBFCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);

void pattacBFLOrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work);
void pattacBFLCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work);
void pattacBFROrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work);
void pattacBFRCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work);
void pattacBFCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, int *ry, double **y, double *work);

//void pattacBFLCompressOutplaceImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, int *ry, double **y, double *work);
//void pattacBFRCompressOutplaceImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, int *ry, double **y, double *work);
//void pattacBFCompressOutplaceImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, int *ry, double **y, double *work);

double pattacInnerProduct(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, double *work);
double pattacNormSquaredHadamardOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work);
double pattacNormSquaredHadamardSymOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work, int *worki);

void pattacGramRL(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GR, double *work);
void pattacGramLR(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GL, double *work);
void pattacGramCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work);
void pattacGramSeqLRLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work);
void pattacGramSeqRLRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work);
#endif

