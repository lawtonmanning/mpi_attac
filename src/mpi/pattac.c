/** \copyright
 * Copyright 2021 Hussam Al Daas, Grey Ballard, and Peter Benner
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/** \file pattac.c
 * \author Hussam Al Daas
 * \brief MPI Algorithms for Tensor Train Arithmetic and Computations
 * \date 16/07/2019
 * \details 
*/
#include <math.h>
#include <unistd.h>
#include "timer.h"
#include "utilities.h"
#include "putilities.h"
#include "attac.h"
#include "pattac.h"

/** \fn void pattacScale(const double alpha, int mode, const int d, const int* n, const int* rx, double *const *x);
 * \brief
 * \details
 * \param alpha
 * \param mode
 * \param d
 * \param n
 * \param rx
 * \param x
 * \remarks
 * \warning
*/
void pattacScale(const double alpha, int mode, const int d, const int* n, const int* rx, double **x){
	ATTAC_START_TIMER
	if(mode < 0 || mode > d-1){
		/**< Scale first mode */
    for(int i = 0; i < n[0] * rx[1]; i++){
      x[0][i] *= alpha;
    }
	}else{
		int i = mode;
    for(int j = 0; j < rx[i] * n[i] * rx[i + 1]; j++){
      x[i][j] *= alpha;
    }
	}
	ATTAC_STOP_TIMER
}

/** \fn double pattacValue(const MPI_Comm comm, const int* isRedundant, const int* index, const int* procHolder, const int d, const int* n, const int* rx, double *const *x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param index
 * \param procHolder
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \remarks
 * \warning
*/
double pattacValue(const MPI_Comm comm, const int* index, const int* procHolder, const int d, const int* n, const int* rx, double *const *x, double* work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Status status;
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}

	/**< Temporary variables for computation */
	double *a = work, *b = work + rmax, *temp = NULL;

	if(rank == procHolder[d - 1]){
		memcpy(a, x[d - 1] + index[d - 1] * rx[d - 1], rx[d - 1] * sizeof(double));
	}
  int one = 1;
  double done = 1.0;
  double zero = 0.0;
	for(int i = d - 2; i > -1; i--){
    int rxn = rx[i] * n[i];
		/**< procHolder[i + 1] sends the vector to procHolder[i] */
		if(rank == procHolder[i + 1] && rank != procHolder[i]){
			ierr = MPI_Send(a, rx[i + 1], MPI_DOUBLE, procHolder[i], 0, comm);
		}
		/**< procHolder[i] receives the vector from procHolder[i + 1] */
		if(rank == procHolder[i] && rank != procHolder[i + 1]){
			ierr = MPI_Recv(a, rx[i + 1], MPI_DOUBLE, procHolder[i + 1], 0, comm, &status);
		}
		if(rank == procHolder[i]){
      dgemv_("N", &rx[i], &rx[i + 1], &done, x[i] + index[i] * rx[i], &rxn, a, &one, &zero, b, &one);
			temp = a;
			a = b;
			b = temp;
		}
	}
	ierr = MPI_Bcast(a, 1, MPI_DOUBLE, procHolder[0], comm);
	ATTAC_STOP_TIMER
	return a[0];
}

/** \fn void pattacFormalSum(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z);
 * \brief
 * \details
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param ry
 * \param y
 * \param rz
 * \param z
 * \remarks
 * \warning
*/
void pattacFormalSum(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z){
	ATTAC_START_TIMER
	attacFormalSum(d, n, rx, x, ry, y, rz, z);
	ATTAC_STOP_TIMER
}

/** \fn void pattacFormalAXPBY(const int d, const int *n, const double alpha, const int *rx, double * const *x, const double beta, const int *ry, double *const *y, int* rz, double **z);
 * \brief
 * \details
 * \param d
 * \param n
 * \param alpha
 * \param rx
 * \param x
 * \param beta
 * \param ry
 * \param y
 * \param rz
 * \param z
 * \remarks
 * \warning
*/
void pattacFormalAXPBY(const int d, const int *n, const double alpha, const int *rx, double * const *x, const double beta, const int *ry, double *const *y, int* rz, double **z){
	ATTAC_START_TIMER
	attacFormalAXPBY(d, n, alpha, rx, x, beta, ry, y, rz, z);
	ATTAC_STOP_TIMER
}

/** \fn void pattacHadamard(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z);
 * \brief
 * \details
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param ry
 * \param y
 * \param rz
 * \param z
 * \remarks
 * \warning
*/
void pattacHadamard(const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, int* rz, double **z){
	ATTAC_START_TIMER
	attacHadamard(d, n, rx, x, ry, y, rz, z);
	ATTAC_STOP_TIMER
}

/** \fn double pattacNorm_2(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* workd, int* worki);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param workd
 * \param worki
 * \remarks
 * \warning
*/
double pattacNorm_2(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* workd, int* worki){
	printf("%s:: This routine is not available yet\n", __func__);
	int ierr;
	ierr = 0;
	MPI_Abort(MPI_COMM_WORLD, -99);
	return -99.;
}

/** \fn double pattacNorm(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \remarks
 * \warning
*/
double pattacNorm(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	double norm = 0, ierr = 0;
	int rank = -1;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
  // Avoid TSQR 
  // if x[0]     is not distributed
  // if x[d - 1] is not distributed and """"N[0] * rx[1] * rx[1] < """"N[d - 1] * rx[d - 1] * rx[d-1]  requires the global N
  if(isRedundant[0] == 1){
	  /**< Left orthogonalization of the TT tensor */
	  pattacBFLOrthogonalization(comm, isRedundant, d, n, rx, x, work);
	  /**< Frobenius norm of the last TT core */
    norm = dlange_("F", &rx[d - 1], &n[d - 1], x[d - 1], &rx[d - 1], NULL);
    if(isRedundant[d - 1] == 0){
	    /**< Squared norm */
	    norm *= norm;
	    ierr = MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, comm);
	    /**< Norm */
	    norm = sqrt(norm);
    }
  }else{
	  /**< Right orthogonalization of the TT tensor */
	  pattacBFROrthogonalization(comm, isRedundant, d, n, rx, x, work);
	  /**< Frobenius norm of the first TT core */
    norm = dlange_("F", &n[0], &rx[1], x[0], &n[0], NULL);
    if(isRedundant[d - 1] == 0){
	    /**< Squared norm */
	    norm *= norm;
	    ierr = MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, comm);
	    /**< Norm */
	    norm = sqrt(norm);
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return norm;
}

/** \fn double pattacNormOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input
 * \param x    input On output x data is destroyed
 * \param work minimal space: \f$r (2r + 1) + r^2 + 2 Srr (1+CLog2P) r^2 + \max(rx[i] n[i] rx[i+1])\f$, where P is number of proc, \f$r = \max(rx[i])\f$, \f$Srr = \sum_{i=1}^{d-1}rx[i]^2\f$
 * \remarks
 * \warning
*/
double pattacNormOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	double norm = 0, ierr = 0;
	int size = 0, rank = -1;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
	int Srr = 0;
	for(int i = 1; i < d; i++){
		Srr += rx[i] * rx[i];
	}
	int lQT = 2 * Srr * (1 + CLog2P);

  if(isRedundant[0] == 1){
	  /**< Left orthogonalization of the TT tensor */
	  pattacBFLOrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
	  /**< Frobenius norm of the last TT core */
    norm = dlange_("F", &rx[d - 1], &n[d - 1], x[d - 1], &rx[d - 1], NULL);
    if(isRedundant[d - 1] == 0){
	    /**< Squared norm */
	    norm *= norm;
	    ierr = MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, comm);
	    /**< Norm */
	    norm = sqrt(norm);
    }
  }else{
	  /**< Right orthogonalization of the TT tensor */
	  pattacBFROrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
	  /**< Frobenius norm of the first TT core */
    norm = dlange_("F", &n[0], &rx[1], x[0], &n[0], NULL);
    if(isRedundant[0] == 0){
	    /**< Squared norm */
	    norm *= norm;
	    ierr = MPI_Allreduce(MPI_IN_PLACE, &norm, 1, MPI_DOUBLE, MPI_SUM, comm);
	    /**< Norm */
	    norm = sqrt(norm);
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return norm;
}

/** \fn void pattacLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x
 * \param x    input/output tensor x to be left orthogonalized on output
 * \param work minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + 2 r^2 + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and P is number of procs
 * \remarks
 * \warning
*/
void pattacLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0, ierr;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	double* R = work + 2 * rmax * rmax;
	double* v = R + rmax * rmax;
	double* t = v + (1 + CLog2P) * rmax * rmax; 
	double* c = t + (1 + CLog2P) * rmax * rmax;
	for(int i = 0; i < d - 1; i++){
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i] * n[i];
		ATTAC_TIC(1, "C Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }else{
		  attacTSQR(comm, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		  if(!rank){
		  	packR('C', rx[i + 1], R, rx[i + 1], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i + 1] * (rx[i + 1] + 1))/2, MPI_DOUBLE, 0, comm);
		  if(rank){
		  	unpackR('C', rx[i + 1], work, R, rx[i + 1]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i + 1] = R x[i + 1] */
		ATTAC_TIC(3, "Update next core")
    int nprxp = n[i + 1] * rx[i + 2];
    double done = 1.0;
    dtrmm_("L", "U", "N", "N", &rx[i + 1], &nprxp, &done, R, &rx[i + 1], x[i + 1], &rx[i + 1]);
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Copy and memset")
    dlacpy_("A", &rxn, &rx[i + 1], x[i], &rxn, c, &rxn);
		memset(x[i], 0, rxn * rx[i + 1] * sizeof(double));
		for(int k = 0; k < rx[i + 1]; k++){
			x[i][k + rxn * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxip1 = rx[i + 1];
		ATTAC_TIC(5, "C Maj Apply Q")
    if(isRedundant[i]){
		  attacTSQRApplyQ(MPI_COMM_SELF, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], c, rxn, v, t, x[i], rxn, work);
    }else{
		  attacTSQRApplyQ(comm, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], c, rxn, v, t, x[i], rxn, work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x replaced by the truncated rank
 * \param x    input/output left orthogonal tensor x to be compressed and right orthogonalized on output
 * \param tau  Truncation threshold
 * \param work minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + 2 r^2 + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and P is number of procs
 * \remarks    The L in the routine name refers that this should be called after calling pattacLOrthogonalization
 * \warning
*/
void pattacLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *xTemp = NULL;
	double *R = NULL;

	int rankSVD = 0;
	
	R = work + 2 * rmax * rmax;
	v = R + rmax * rmax;
	t = v + (1 + CLog2P) * rmax * rmax;
	sig = t + (1 + CLog2P) * rmax * rmax;
  superb = sig + rmax;
  xTemp = superb + rmax;
	for(int i = d - 1; i > 0; i--){
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "R Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'R', rx[i + 1] * n[i], rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacTSQR(comm,          'R', rx[i + 1] * n[i], rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		/**< Pack R and broadcast it */
		  if(!rank){
		  	packR('R', rx[i], R, rx[i], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i] * (rx[i] + 1))/2, MPI_DOUBLE, 0, comm);
		  /**
		  * \remark: R on proc 0 is already unpacked and has 0 in its lower triangular part
		  */
		  if(rank){
		  	memset(R, 0, rx[i] * rx[i] * sizeof(double));
		  	unpackR('R', rx[i], work, R, rx[i]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[i], rx[i], R, rx[i], sig, NULL, 1, work, rx[i], superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}

		/**< Copy the leaf of implicit Q to xtemp */
		/**
		* \remark: Since applying Q is in place, we copy Q to apply it on V^T (will be stored in x), where \f$R \approx U \Sigma V^T\f$
		*/
		ATTAC_TIC(3, "Copy and memset")
    int nrx = n[i] * rx[i + 1];
    dlacpy_("A", &rx[i], &nrx, x[i], &rx[i], xTemp, &rx[i]);
		ATTAC_TAC(3)

		/**< Copy U to x[i] */
		ATTAC_TIC(3, "Copy and memset")
		memset(x[i], 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
    dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], x[i], &rankSVD);
		ATTAC_TAC(3)

		/**< Scale the column j in U by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i]; k++){
				R[j * rx[i] + k] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "R Maj Apply Q")
    if(isRedundant[i]){
		  attacTSQRApplyQ(MPI_COMM_SELF, 'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], xTemp, rx[i], v, t, x[i], rankSVD, work);
    }else{
		  attacTSQRApplyQ(comm,          'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], xTemp, rx[i], v, t, x[i], rankSVD, work);
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the previous core x[i - 1] */
		ATTAC_TIC(6, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rxmnm, &rankSVD, &rx[i], &done, x[i - 1], &rxmnm, R, &rx[i], &zero, xTemp, &rxmnm);
		ATTAC_TAC(6)
		ATTAC_TIC(3, "Copy and memset")
    int rxnm1 = rx[i - 1] * n[i - 1];
    dlacpy_("A", &rxnm1, &rankSVD, xTemp, &rxnm1, x[i - 1], &rxnm1);
		ATTAC_TAC(3)

		rx[i] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x
 * \param x    input/output tensor to be right orthogonalized
 * \param work minimum space: \f$2 (1 + CLog2P) r^2 + r^2 + 2r^2 + \max(n[i] rx[i] rx[i + 1])\f$, where \f$P\f$ is the number of processors and \f$r\f$ is \f$\max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	double* R = work + 2 * rmax * rmax;
	double* v = R + rmax * rmax;
	double* t = v + (1 + CLog2P) * rmax * rmax; 
	double* c = t + (1 + CLog2P) * rmax * rmax;
	for(int i = d - 1; i > 0; i--){
		/**
		 * \remarks We want to compute an LQ factorization of x[i]_{(0)} (unfolding in mode 0). To do so
		 *	        we unfold in the 0 mode in order to perform a QR decomposition with row major layout 
		 *          of x[i]. Note that the data in memory are in comlumn major layout
		 */
		int rxn = rx[i + 1] * n[i];
		ATTAC_TIC(1, "R Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacTSQR(comm,          'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		  if(!rank){
		  	packR('R', rx[i], R, rx[i], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i] * (rx[i] + 1))/2, MPI_DOUBLE, 0, comm);
		  if(rank){
		  	unpackR('R', rx[i], work, R, rx[i]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i - 1] := x[i - 1] R */
		ATTAC_TIC(3, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    dtrmm_("R", "L", "N", "N", &rxmnm, &rx[i], &done, R, &rx[i], x[i - 1], &rxmnm);
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Copy and memset")
    memcpy(c, x[i], rxn * rx[i] * sizeof(double));
		memset(x[i], 0, rxn * rx[i] * sizeof(double));
		for(int k = 0; k < rx[i]; k++){
			x[i][k + rx[i] * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxi = rx[i];
		ATTAC_TIC(5, "R Maj Apply Q")
    if(isRedundant[i]){
		  attacTSQRApplyQ(MPI_COMM_SELF, 'R', rxn, &(rxi), rx[i], rx[i], c, rx[i], v, t, x[i], rx[i], work);
    }else{
		  attacTSQRApplyQ(comm,          'R', rxn, &(rxi), rx[i], rx[i], c, rx[i], v, t, x[i], rx[i], work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x
 * \param x    input/output tensor to be right orthogonalized
 * \param tau  Truncation threshold
 * \param work minimum space: \f$2 (1 + CLog2P) r^2 + r^2 + 2r^2 + 2r + \max(n[i] rx[i] rx[i + 1])\f$, where \f$P\f$ is the number of processors and \f$r\f$ is \f$\max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *xTemp = NULL;
	double *R = NULL;

	int rankSVD = 0;

	R = work + 2 * rmax * rmax;
	sig = R + rmax * rmax;
	superb = sig + rmax;
	v = superb + rmax;
	t = v + (1 + CLog2P) * rmax * rmax;
	xTemp = t +  (1 + CLog2P) * rmax * rmax;
	for(int i = 0; i < d - 1; i++){
    int rxn = rx[i] * n[i];
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "C Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
    }else{
		  attacTSQR(comm,          'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		  /**< pack R and broadcast it */
		  if(!rank){
		  	packR('C', rx[i + 1], R, rx[i + 1], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i + 1] * (rx[i + 1] + 1))/2, MPI_DOUBLE, 0, comm);
		  /**
		  * \remark: R on proc 0 is already unpacked and has 0 in its lower triangular part
		  */
		  if(rank){
		  	unpackR('C', rx[i + 1], work, R, rx[i + 1]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Copy the leaf of implicit Q to xtemp */
		/**
		* \remark: Since applying Q is in place, we copy Q to apply it on V^T (will be stored in x), where \f$R \approx U \Sigma V^T\f$
		*/
		ATTAC_TIC(3, "Copy and memset")
    dlacpy_("A", &rxn, &rx[i + 1], x[i], &rxn, xTemp, &rxn);
		ATTAC_TAC(3)

		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[i + 1], rx[i + 1], R, rx[i + 1], sig, work, rx[i + 1], NULL, 1, superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i + 1]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}

		/**< Copy U to x[i] */
		ATTAC_TIC(3, "Copy and memset")
		memset(x[i], 0, rx[i] * n[i] * rankSVD * sizeof(double));
    dlacpy_("A", &rx[i + 1], &rankSVD, work, &rx[i + 1], x[i], &rxn);
		ATTAC_TAC(3)

		/**< Scale the column j in V by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i + 1]; k++){
				R[j + k * rx[i + 1]] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "C Maj Apply Q")
    if(isRedundant[i]){
		  attacTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], xTemp, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
    }else{
		  attacTSQRApplyQ(comm,          'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], xTemp, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the following core x[i + 1] */
		ATTAC_TIC(6, "Update following core")
    int nprxp = n[i + 1] * rx[i + 2];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rankSVD, &nprxp, &rx[i + 1], &done, R, &rx[i + 1], x[i + 1], &rx[i + 1], &zero, xTemp, &rankSVD);
		ATTAC_TAC(6)
		ATTAC_TIC(3, "Copy and memset")
    int ranknp1 = rankSVD * n[i + 1];
    dlacpy_("A", &ranknp1, &rx[i + 2], xTemp, &ranknp1, x[i + 1], &ranknp1);
		ATTAC_TAC(3)
		rx[i + 1] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx    input/output ranks of x
 * \param x     input/output tensor to be truncated
 * \param tau   Truncation threshold
 * \param ortho Orthogonalization state of the tensor at the end of the procedure
 * \param work  minimum space: \f$2 (1 + CLog2P) r^2 + r^2 + 2 r^2 + 2 r + \max(n[i] rx[i] rx[i + 1])\f$, where \f$P\f$ is the number of processors and \f$r\f$ is \f$\max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work){
	ATTAC_START_TIMER
	int rank = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	if(ortho == 'L'){
		ATTAC_TIC(1, "R Compress")
		/**< Right orthogonalization */
		pattacROrthogonalization(comm, isRedundant, d, n, rx, x, work);
		/**< Compress a right orthogonal TT tensor */
		pattacRCompress(comm, isRedundant, d, n, rx, x, tau, work);
		ATTAC_TAC(1)
	}else if(ortho == 'R'){
		ATTAC_TIC(2, "L Compress")
		/**< Left orthogonalization */
		pattacLOrthogonalization(comm, isRedundant, d, n, rx, x, work);
		/**< Compress a left orthogonal TT tensor */
		pattacLCompress(comm, isRedundant, d, n, rx, x, tau, work);
		ATTAC_TAC(2)
	}else{
		fprintf(stderr, "%s:Line %d %s::Not known orthogonalization side\n", __FILE__, __LINE__, __func__);
		MPI_Abort(comm, -99);
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x
 * \param x    input/output tensor x to be left orthogonalized on output
 * \param work minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + r (2 r + 1) + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFLOrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Butterfly Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	double* R = work + rmax * (2 * rmax + 1);
	double* v = R + rmax * rmax;
	double* t = v + (1 + CLog2P) * rmax * rmax; 
	double* c = t + (1 + CLog2P) * rmax * rmax;

	for(int i = 0; i < d - 1; i++){
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i] * n[i];
		ATTAC_TIC(1, "C Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }else{
		  attacBFTSQR(comm, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i + 1] = R x[i + 1] */
		ATTAC_TIC(3, "Update next core")
    double done = 1.0;
    int nprxp = n[i + 1] * rx[i + 2];
    dtrmm_("L", "U", "N", "N", &rx[i + 1], &nprxp, &done, R, &rx[i + 1], x[i + 1], &rx[i + 1]);
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Copy and memset")
    dlacpy_("A", &rxn, &rx[i + 1], x[i], &rxn, c, &rxn);
		memset(x[i], 0, rxn * rx[i + 1] * sizeof(double));
		for(int k = 0; k < rx[i + 1]; k++){
			x[i][k + rxn * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxip1 = rx[i + 1];
		ATTAC_TIC(5, "C Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], c, rxn, v, t, x[i], rxn, work);
    }else{
		  attacBFTSQRApplyQ(comm, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], c, rxn, v, t, x[i], rxn, work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x replaced by the truncated rank
 * \param x    input/output left orthogonal tensor x to be compressed and right orthogonalized on output
 * \param tau  Truncation threshold
 * \param work minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + r (2 r + 1) + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks    The L in the routine name refers that this should be called after calling pattacBFLOrthogonalization
 * \warning
*/
void pattacBFLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Butterfly Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *xTemp = NULL;
	double *R = NULL;

	int rankSVD = 0;
	R = work + rmax * (2 * rmax + 1);
	sig = R + rmax * rmax;
	superb = sig + rmax;
	v = superb + rmax;
	t = v + (1 + CLog2P) * rmax * rmax;
	xTemp = t + (1 + CLog2P) * rmax * rmax;
	for(int i = d - 1; i > 0; i--){
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "R Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'R', rx[i + 1] * n[i], rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacBFTSQR(comm, 'R', rx[i + 1] * n[i], rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Copy the leaf of implicit Q to xtemp */
		/**
		* \remark: Since applying Q is in place, we copy Q to apply it on V^T (will be stored in x), where \f$R \approx U \Sigma V^T\f$
		*/
    int nrx = n[i] * rx[i + 1];
		ATTAC_TIC(3, "Copy and memset")
    dlacpy_("A", &rx[i], &nrx, x[i], &rx[i], xTemp, &rx[i]);
		ATTAC_TAC(3)

		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[i], rx[i], R, rx[i], sig, NULL, 1, work, rx[i], superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}

		/**< Copy U to x[i] */
		ATTAC_TIC(3, "Copy and memset")
		memset(x[i], 0, rankSVD * rx[i + 1] * n[i] * sizeof(double));
    dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], x[i], &rankSVD);
		ATTAC_TAC(3)

		/**< Scale the column j in V by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i]; k++){
				R[j * rx[i] + k] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "R Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], xTemp, rx[i], v, t, x[i], rankSVD, work);
    }else{
		  attacBFTSQRApplyQ(comm,          'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], xTemp, rx[i], v, t, x[i], rankSVD, work);
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the previous core x[i - 1] */
		ATTAC_TIC(6, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rxmnm, &rankSVD, &rx[i], &done, x[i - 1], &rxmnm, R, &rx[i], &zero, xTemp, &rxmnm);
		ATTAC_TAC(6)
		ATTAC_TIC(3, "Copy and memset")
    int rxnm1 = rx[i - 1] * n[i - 1];
    dlacpy_("A", &rxnm1, &rankSVD, xTemp, &rxnm1, x[i - 1], &rxnm1);
		ATTAC_TAC(3)

		rx[i] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x
 * \param x    input/output tensor to be right orthogonalized
 * \param work minimum space: \f$2 (1 + CLog2P) r^2 + r^2 + r (2r + 1) + \max(n[i] rx[i] rx[i + 1])\f$, where \f$P\f$ is the number of processors and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFROrthogonalization(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Butterfly Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	double* R = work + rmax * (2 * rmax + 1);
	double* v = R + rmax * rmax;
	double* t = v + (1 + CLog2P) * rmax * rmax; 
	double* c = t + (1 + CLog2P) * rmax * rmax; 
	for(int i = d - 1; i > 0; i--){
		/**
		 * \remarks We want to compute an LQ factorization of x[i]_{(0)} (unfolding in mode 0). To do so
		 *	        we unfold in the 0 mode in order to perform a QR decomposition with row major layout 
		 *          of x[i]. Note that the data in memory are in comlumn major layout
		 */
		int rxn = rx[i + 1] * n[i];
		ATTAC_TIC(1, "R Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacBFTSQR(comm,          'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i - 1] := x[i - 1] R */
		ATTAC_TIC(3, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    dtrmm_("R", "L", "N", "N", &rxmnm, &rx[i], &done, R, &rx[i], x[i - 1], &rxmnm);
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Copy and memset")
    dlacpy_("A", &rx[i], &rxn, x[i], &rx[i], c, &rx[i]);
		memset(x[i], 0, rxn * rx[i] * sizeof(double));
		for(int k = 0; k < rx[i]; k++){
			x[i][k + rx[i] * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxi = rx[i];
		ATTAC_TIC(5, "R Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', rxn, &(rxi), rx[i], rx[i], c, rx[i], v, t, x[i], rx[i], work);
    }else{
		  attacBFTSQRApplyQ(comm,          'R', rxn, &(rxi), rx[i], rx[i], c, rx[i], v, t, x[i], rx[i], work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of x replaced by the truncated rank
 * \param x    input/output right orthogonal tensor x to be compressed and left orthogonalized on output
 * \param tau  Truncation threshold
 * \param work minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + r (2 r + 1) + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks    The R in the routine name refers that this should be called after calling pattacBFROrthogonalization
 * \warning
*/
void pattacBFRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *xTemp = NULL;
	double *R = NULL;

	int rankSVD = 0;
	R = work + rmax * (2 * rmax + 1);
	sig = R + rmax * rmax;
	superb = sig + rmax;
	v = superb + rmax;
	t = v + (1 + CLog2P) * rmax * rmax;
	xTemp = t + (1 + CLog2P) * rmax * rmax;
	for(int i = 0; i < d - 1; i++){
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "C Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
    }else{
		  attacBFTSQR(comm,          'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Copy the leaf of implicit Q to xtemp */
		/**
		* \remark: Since applying Q is in place, we copy Q to apply it on V^T (will be stored in x), where \f$R \approx U \Sigma V^T\f$
		*/
		ATTAC_TIC(3, "Copy and memset")
    int rxn = rx[i] * n[i];
    dlacpy_("A", &rxn, &rx[i + 1], x[i], &rxn, xTemp, &rxn);
		ATTAC_TAC(3)

		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[i + 1], rx[i + 1], R, rx[i + 1], sig, work, rx[i + 1], NULL, 1, superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i + 1]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}
		/**< Copy U to x[i] */
		ATTAC_TIC(3, "Copy and memset")
		memset(x[i], 0, rx[i] * n[i] * rx[i + 1] * sizeof(double));
    dlacpy_("A", &rx[i + 1], &rankSVD, work, &rx[i + 1], x[i], &rxn);
		ATTAC_TAC(3)

		/**< Scale the column j in V by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i + 1]; k++){
				R[j + k * rx[i + 1]] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "C Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], xTemp, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
    }else{
		  attacBFTSQRApplyQ(comm,          'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], xTemp, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the following core x[i + 1] */
		ATTAC_TIC(6, "Update following core")
    int nprxp = n[i + 1] * rx[i + 2];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rankSVD, &nprxp, &rx[i + 1], &done, R, &rx[i + 1], x[i + 1], &rx[i + 1], &zero, xTemp, &rankSVD);
		ATTAC_TAC(6)
		ATTAC_TIC(3, "Copy and memset")
    int rankSVDnp1 = rankSVD * n[i + 1];
    dlacpy_("A", &rankSVDnp1, &rx[i + 2], xTemp, &rankSVDnp1, x[i + 1], &rankSVDnp1);
		ATTAC_TAC(3)

		rx[i + 1] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx    input/output ranks of x replaced by the truncated rank
 * \param x     input/output tensor x to be compressed
 * \param tau   Truncation threshold
 * \param ortho Orthogonalization state of the tensor at the end of the procedure
 * \param work  minimal space: \f$2 (1 + CLog2P) r^2 + r^2 + r (2 r + 1) + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work){
	ATTAC_START_TIMER
	int rank = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	if(ortho == 'L'){
		ATTAC_TIC(1, "R Compress")
		/**< Right orthogonalization */
		pattacBFROrthogonalization(comm, isRedundant, d, n, rx, x, work);
		/**< Compress a right orthogonal TT tensor */
		pattacBFRCompress(comm, isRedundant, d, n, rx, x, tau, work);
		ATTAC_TAC(1)
	}else if(ortho == 'R'){
		ATTAC_TIC(2, "L Compress")
		/**< Left orthogonalization */
		pattacBFLOrthogonalization(comm, isRedundant, d, n, rx, x, work);
		/**< Compress a left orthogonal TT tensor */
		pattacBFLCompress(comm, isRedundant, d, n, rx, x, tau, work);
		ATTAC_TAC(2)
	}else{
		fprintf(stderr, "%s:Line %d %s::Not known orthogonalization side\n", __FILE__, __LINE__, __func__);
		MPI_Abort(comm, -99);
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
 * \brief Left orthogonalization of a TT tensor with TT. Q factors of cores are given implicitly as TSQR tree.
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param QT   (output) Contains at position \f$(\sum_{i=0}^{k-1} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core k-1 and the corresponding Householder reflectors [Q, T]. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i])\f$. If you want to pass this argument to pattacLCompressImplicitQs the space required is the same previous relation with \f$(\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + 2r^2\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0, ierr;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	int QToffset = 0;
	for(int i = 0; i < d - 1; i++){
	  double* R = work + 2 * rx[i + 1] * rx[i + 1];
	  double* v = QT + QToffset;
	  double* t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i] * n[i];
		ATTAC_TIC(1, "C Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }else{
		  attacTSQR(comm, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		  if(!rank){
		  	packR('C', rx[i + 1], R, rx[i + 1], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i + 1] * (rx[i + 1] + 1))/2, MPI_DOUBLE, 0, comm);
		  if(rank){
		  	unpackR('C', rx[i + 1], work, R, rx[i + 1]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)

		/**
		 * \remarks Multiplies R by the right TT core.
		 */
		/**< x[i + 1] = R x[i + 1] */
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		ATTAC_TIC(3, "Update prev core")
    double done = 1.0;
    int nprxp = n[i + 1] * rx[i + 2];
    dtrmm_("L", "U", "N", "N", &rx[i + 1], &nprxp, &done, R, &rx[i + 1], x[i + 1], &rx[i + 1]);
		ATTAC_TAC(3)
		QToffset += 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  Truncation threshold
 * \param QT   (input) Contains at position \f$(\sum_{i=0}^{k-1} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k-1\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$, where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + 2r^2 + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	int rmax = 0;
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *z = NULL;
	double *R = NULL;

	int rankSVD = 0;
  int nmax = 0;
  for(int i = 0; i < d - 1; i++){
    nmax = (n[i] > nmax) ? n[i] : nmax;
  }

  // Let v and t point to the end of QT
  int QToffset = 0;
  for(int i = 0; i < d - 1; i++){
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    QToffset += 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    rmax = (rmax > rx[i + 1]) ? rmax : rx[i + 1];
  }
  v = QT + QToffset;
  t = v + (1 + CLog2P) * rx[d - 1] * rx[d - 1];
	
  // TSQR work space
  R = work + 2 * rmax * rmax;
  sig = R + rmax * rmax;
  superb = sig + rmax;
  // Place to compute V^T H(x[i]) and V(x[i]) U Sig
  z = superb + rmax;

  // Truncate last core
  /// TSQR: H(x[d - 1]) = LQ
  if(isRedundant[d - 1]){
    ATTAC_TIC(1, "R Maj TSQR")
		attacTSQR(MPI_COMM_SELF, 'R', n[d - 1], rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], R, v, t, work);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(1, "R Maj TSQR")
		attacTSQR(comm,          'R', n[d - 1], rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], R, v, t, work);
    ATTAC_TAC(1)
    ATTAC_TIC(2, "BCAST R")
    if(isRedundant[d - 2] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      if(!rank){
        packR('R', rx[d - 1], R, rx[d - 1], work);
      }
		  ierr = MPI_Bcast(work, (rx[d - 1] * (rx[d - 1] + 1))/2, MPI_DOUBLE, 0, comm);
      if(rank){
        unpackR('R', rx[d - 1], work, R, rx[d - 1]);
      }
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
    ATTAC_TAC(2)
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(3, "SVD")
  // SVD of the L factor named R here, R will contain the left singular vectors, work will contain the right singular vectors
	ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[d - 1], rx[d - 1], R, rx[d - 1], sig, NULL, 1, work, rx[d - 1], superb);
  ATTAC_TAC(3)
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
		MPI_Abort(MPI_COMM_WORLD, ierr);
#else
	 exit(1);
#endif
	}

  // Rank of L = U sig V^T
  for(int j = 0; j < rx[d - 1]; j++){
    if(sig[j] < tau){
      break;
    }else{
      rankSVD = j + 1;
    }
  }

  // Scale the first rankSVD columns of U 
  for(int j = 0; j < rankSVD; j++){
    for(int k = 0; k < rx[d - 1]; k++){
      R[k + j * rx[d - 1]] *= sig[j];
    }
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
  // Either broadcast rankSVD or memset z to 0 for rx[d - 1] * n[d - 1] and copy nothing out of root
  ATTAC_TIC(2, "BCAST R")
  ierr = MPI_Bcast(&rankSVD, 1, MPI_INT, 0, comm);
  ATTAC_TAC(2)
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
  
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  // put V^T in z
  memset(z, 0, rankSVD * n[d - 1] * sizeof(double));
  dlacpy_("A", &rankSVD, &rx[d - 1], work, &rx[d - 1], z, &rankSVD);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(5, "R Maj Apply Q")
  // Apply x[d - 1] on z
  if(isRedundant[d - 1]){
	  attacTSQRApplyQ(MPI_COMM_SELF, 'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, z, rankSVD, work);
  }else{
	  attacTSQRApplyQ(comm,          'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, z, rankSVD, work);
  }
  ATTAC_TAC(5)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  dlacpy_("A", &rankSVD, &n[d - 1], z, &rankSVD, x[d - 1], &rankSVD);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

  //Loop apply z = x[i] on U Sig. Then TSQR z and SVD of R
  for(int i = d - 2; i > -1; i--){
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
    // Set to apply x[i] on U Sig
    QToffset -= 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    ATTAC_TIC(4, "Copy and memset")
    // set z to 0
    memset(z, 0, rx[i] * n[i] * rx[i + 1] * sizeof(double)); // to change rx[i + 1] by rankSVD
    // copy U Sig to z
    int rxn = rx[i] * n[i];
    dlacpy_("A", &rx[i + 1], &rankSVD, R, &rx[i + 1], z, &rxn);
    ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    ATTAC_TIC(6, "C Maj Apply Q")
    // Apply Q
    if(isRedundant[i]){
	    attacTSQRApplyQ(MPI_COMM_SELF, 'C', n[i] * rx[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, z, n[i] * rx[i], work);
    }else{
	    attacTSQRApplyQ(comm,          'C', n[i] * rx[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, z, n[i] * rx[i], work);
    }
    ATTAC_TAC(6)
    // Set new rank
    rx[i + 1] = rankSVD;

    if(i > 0){
      //////////////////////////////////
      // Truncate current core from left
      //////////////////////////////////
      /// TSQR: H(z) = LQ
      t = v + (1 + CLog2P) * rx[i] * rx[i];
      if(isRedundant[i]){
        ATTAC_TIC(1, "R Maj TSQR")
	      attacTSQR(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], rx[i], rx[i], z, rx[i], R, v, t, work);
        ATTAC_TAC(1)
      }else{
        ATTAC_TIC(1, "R Maj TSQR")
	      attacTSQR(comm,          'R', n[i] * rx[i + 1], rx[i], rx[i], z, rx[i], R, v, t, work);
        ATTAC_TAC(1)
        ATTAC_TIC(2, "BCAST R")
        if(isRedundant[i - 1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
          if(!rank){
            packR('R', rx[i], R, rx[i], work);
          }
		      ierr = MPI_Bcast(work, (rx[i] * (rx[i] + 1))/2, MPI_DOUBLE, 0, comm);
          if(rank){
            unpackR('R', rx[i], work, R, rx[i]);
          }
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
        }
        ATTAC_TAC(2)
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(3, "SVD")
      // SVD of the L factor named R here, R will contain the left singular vectors, work will contain the right singular vectors
  	  ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[i], rx[i], R, rx[i], sig, NULL, 1, work, rx[i], superb);
      ATTAC_TAC(3)
  	  if(ierr != 0){
  		  fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
  		  MPI_Abort(MPI_COMM_WORLD, ierr);
#else
  		  exit(1);
#endif
  	  }
      // Rank of L = U sig V^T
      for(int j = 0; j < rx[i]; j++){
        if(sig[j] < tau){
          break;
        }else{
          rankSVD = j + 1;
        }
      }
      // Scale the first rankSVD columns of U 
      for(int j = 0; j < rankSVD; j++){
        for(int k = 0; k < rx[i]; k++){
          R[k + j * rx[i]] *= sig[j];
        }
      }
      ATTAC_TIC(2, "BCAST R")
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      // Either broadcast rankSVD or memset z to 0 for rx[i] * n[i] * rx[i + 1] and copy nothing out of root
      if(isRedundant[i] == 0){
        ierr = MPI_Bcast(&rankSVD, 1, MPI_INT, 0, comm);
      }
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      //Apply Q from z on x[i] after copying U to x[i]
      memset(x[i], 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
      dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], x[i], &rankSVD);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(5, "R Maj Apply Q")
      // Apply Q
      if(isRedundant[i]){
	      attacTSQRApplyQ(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], z, rx[i], v, t, x[i], rankSVD, work);
      }else{
	      attacTSQRApplyQ(comm,          'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], z, rx[i], v, t, x[i], rankSVD, work);
      }
      ATTAC_TAC(5)
    }else{
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      dlacpy_("A", &n[0], &rx[1], z, &n[0], x[0], &n[0]);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
 * \brief Rightt orthogonalization of a TT tensor with TT. Q factors of cores are given implicitly as TSQR tree.
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param QT   (output) Contains at position \f$(\sum_{i=d-2}^{k} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core k and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i])\f$. If you want to pass this argument to pattacRCompressImplicitQs the space required is the same previous relation with \f$(\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + 2r^2\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0, ierr;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	int QToffset = 0;
	for(int i = d - 1; i > 0; i--){
		double* R = work + 2 * rx[i] * rx[i];
		double* v = QT + QToffset;
		double* t = v + (1 + CLog2P) * rx[i] * rx[i];
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i + 1] * n[i];
		ATTAC_TIC(1, "R Maj TSQR")
    if(isRedundant[i]){
		  attacTSQR(MPI_COMM_SELF, 'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacTSQR(comm, 'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
		  if(!rank){
		  	packR('R', rx[i], R, rx[i], work);
		  }
		  ierr = MPI_Bcast(work, (rx[i] * (rx[i] + 1))/2, MPI_DOUBLE, 0, comm);
		  if(rank){
		  	unpackR('R', rx[i], work, R, rx[i]);
		  }
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif

		/**
		 * \remarks Multiplies R by the rightt TT core.
		 */
		/**< x[i - 1] = x[i - 1] R */
		ATTAC_TIC(3, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    dtrmm_("R", "L", "N", "N", &rxmnm, &rx[i], &done, R, &rx[i], x[i - 1], &rxmnm);
		ATTAC_TAC(3)
		QToffset += 2 * (1 + CLog2P) * rx[i] * rx[i];
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  threshold for truncation
 * \param QT   (input) Contains at position \f$(\sum_{i=d-2}^{k} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$.
 * \param work minimal space: \f$2 * r^2 + r^2 + 2 r + \max(n[i] r[i] r[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	int rmax = 0;
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *z = NULL;
	double *R = NULL;

	int rankSVD = 0;
  int nmax = 0;
  for(int i = 0; i < d - 1; i++){
    nmax = (n[i] > nmax) ? n[i] : nmax;
  }

  // Let v and t point to the end of QT
  int QToffset = 0;
  for(int i = d - 1; i > 0; i--){
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i] * rx[i];
    QToffset += 2 * (1 + CLog2P) * rx[i] * rx[i];
    rmax = (rmax > rx[i]) ? rmax : rx[i];
  }
  v = QT + QToffset;
  t = v + (1 + CLog2P) * rx[1] * rx[1];
	
  // Truncate last core
  /// TSQR: H(x[0]) = LQ
  R = work + 2 * rmax * rmax;
  sig = R + rmax * rmax;
  superb = sig + rmax;
  // Place to compute V^T H(x[i]) and V(x[i]) U Sig
  z = work + 2 * rmax * rmax + rmax * rmax + 2 * rmax;

  if(isRedundant[0]){
    ATTAC_TIC(1, "C Maj TSQR")
		attacTSQR(MPI_COMM_SELF, 'C', n[0], rx[1], rx[1], x[0], n[0], R, v, t, work);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(1, "C Maj TSQR")
		attacTSQR(comm, 'C', n[0], rx[1], rx[1], x[0], n[0], R, v, t, work);
    ATTAC_TAC(1)
    ATTAC_TIC(2, "BCAST R")
    if(isRedundant[1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      if(!rank){
        packR('R', rx[1], R, rx[1], work);
      }
		  ierr = MPI_Bcast(work, (rx[1] * (rx[1] + 1))/2, MPI_DOUBLE, 0, comm);
      if(rank){
        unpackR('R', rx[1], work, R, rx[1]);
      }
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
    ATTAC_TAC(2)
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(3, "SVD")
  // SVD of the R factor, R will contain the right singular vectors, work will contain the left singular vectors
	ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[1], rx[1], R, rx[1], sig, work, rx[1], NULL, 1, superb);
  ATTAC_TAC(3)
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
		MPI_Abort(MPI_COMM_WORLD, ierr);
#else
	 exit(1);
#endif
	}

  // Rank of R = U sig V^T
  for(int j = 0; j < rx[1]; j++){
    if(sig[j] < tau){
      break;
    }else{
      rankSVD = j + 1;
    }
  }

  // Scale the first rankSVD columns of V, rows of V^T
  for(int k = 0; k < rankSVD; k++){
    for(int j = 0; j < rx[1]; j++){
      R[k + j * rx[1]] *= sig[k];
    }
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(2, "BCAST R")
  // Either broadcast rankSVD or memset z to 0 for rx[1] * n[0] and copy nothing out of root
  if(isRedundant[0] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
    ierr = MPI_Bcast(&rankSVD, 1, MPI_INT, 0, comm);
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
  }
  ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  // put U in z
  memset(z, 0, rankSVD * n[0] * sizeof(double));
  dlacpy_("A", &rx[1], &rankSVD, work, &rx[1], z, &n[0]);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(5, "C Maj Apply Q")
  // Apply x[0] on z
  if(isRedundant[0]){
	  attacTSQRApplyQ(MPI_COMM_SELF, 'C', n[0], &rankSVD, rx[1], rx[1], x[0], n[0], v, t, z, n[0], work);
  }else{
	  attacTSQRApplyQ(comm,          'C', n[0], &rankSVD, rx[1], rx[1], x[0], n[0], v, t, z, n[0], work);
  }
  ATTAC_TAC(5)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  dlacpy_("A", &n[0], &rankSVD, z, &n[0], x[0], &n[0]);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

  //Loop apply z = x[i] on Sig V^T. Then TSQR z and SVD of R
  for(int i = 1; i < d; i++){
    // Set up to apply x[i] on Sig V^T
    QToffset -= 2 * (1 + CLog2P) * rx[i] * rx[i];
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i] * rx[i];
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
    ATTAC_TIC(4, "Copy and memset")
    // set z to 0
    memset(z, 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
    // copy Sig V^T to z
    dlacpy_("A", &rankSVD, &rx[i], R, &rx[i], z, &rankSVD);
    ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    ATTAC_TIC(6, "R Maj Apply Q")
    // Apply Q
    if(isRedundant[i]){
	    attacTSQRApplyQ(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], x[i], rx[i], v, t, z, rankSVD, work);
    }else{
	    attacTSQRApplyQ(comm,          'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], x[i], rx[i], v, t, z, rankSVD, work);
    }
    ATTAC_TAC(6)
    // Set new rank
    rx[i] = rankSVD;
    if(i < d - 1){
      ///////////////////////////////////
      // Truncate current core from right
      ///////////////////////////////////
      /// TSQR: V(z) = QR
      t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
      if(isRedundant[i]){
        ATTAC_TIC(1, "C Maj TSQR")
	      attacTSQR(MPI_COMM_SELF, 'C', n[i] * rx[i], rx[i + 1], rx[i + 1], z, rx[i] * n[i], R, v, t, work);
        ATTAC_TAC(1)
      }else{
        ATTAC_TIC(1, "C Maj TSQR")
	      attacTSQR(comm,          'C', n[i] * rx[i], rx[i + 1], rx[i + 1], z, rx[i] * n[i], R, v, t, work);
        ATTAC_TAC(1)
        ATTAC_TIC(2, "BCAST R")
        if(isRedundant[i + 1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
          if(!rank){
            packR('R', rx[i + 1], R, rx[i + 1], work);
          }
		      ierr = MPI_Bcast(work, (rx[i + 1] * (rx[i + 1] + 1))/2, MPI_DOUBLE, 0, comm);
          if(rank){
            unpackR('R', rx[i + 1], work, R, rx[i + 1]);
          }
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
        }
        ATTAC_TAC(2)
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(3, "SVD")
      // SVD of the R factor, R will contain the right singular vectors, work will contain the left singular vectors
  	  ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[i + 1], rx[i + 1], R, rx[i + 1], sig, work, rx[i + 1], NULL, 1, superb);
      ATTAC_TAC(3)
  	  if(ierr != 0){
  		  fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
  		  MPI_Abort(MPI_COMM_WORLD, ierr);
#else
  		  exit(1);
#endif
  	  }
      // Rank of R = U sig V^T
      for(int j = 0; j < rx[i + 1]; j++){
        if(sig[j] < tau){
          break;
        }else{
          rankSVD = j + 1;
        }
      }
      // Scale the first rankSVD columns of V, (first rankSVD rows of V^T) 
      for(int j = 0; j < rx[i + 1]; j++){
        for(int k = 0; k < rankSVD; k++){
          R[k + j * rx[i + 1]] *= sig[k];
        }
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(2, "BCAST R")
      // Either broadcast rankSVD or memset z to 0 for rx[i] * n[i] * rx[i + 1] and copy nothing out of root
      if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
        ierr = MPI_Bcast(&rankSVD, 1, MPI_INT, 0, comm);
#if TIME_BREAKDOWN
    MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
      }
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      //Apply Q from z on x[i] after copying U to x[i]
      memset(x[i], 0, rx[i] * n[i] * rankSVD * sizeof(double));
      int rxn = rx[i] * n[i];
      dlacpy_("A", &rx[i + 1], &rankSVD, work, &rx[i + 1], x[i], &rxn);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      // Apply Q
      ATTAC_TIC(5, "C Maj Apply Q")
      if(isRedundant[i]){
	      attacTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], z, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }else{
	      attacTSQRApplyQ(comm,          'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], z, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }
      ATTAC_TAC(5)
    }else{
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      dlacpy_("A", &rx[d - 1], &n[d - 1], z, &rx[d - 1], x[d - 1], &rx[d - 1]);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  threshold for truncation
 * \param ortho Orthogonalization state of the tensor at the end of the procedure
 * \param work minimal space: \f$2 * r^2 + r^2 + 2 r + \max(n[i] r[i] r[i+1]) + 2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$, \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work){
	ATTAC_START_TIMER
	int rank = 0, size = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
	int Srr = 0;
  int rmax = 0;
	for(int i = 0; i < d; i++){
    rmax = (rmax < rx[i + 1]) ? rx[i + 1] : rmax;
	}
	for(int i = 1; i < d; i++){
		Srr += rx[i] * rx[i];
	}
	Srr += rmax * rmax;
	int lQT = 2 * Srr * (1 + CLog2P);

	if(ortho == 'L'){
		ATTAC_TIC(1, "R Compress")
		/**< Right orthogonalization */
		pattacROrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
		/**< Compress a right orthogonal TT tensor */
		pattacRCompressImplicitQs(comm, isRedundant, d, n, rx, x, tau, work, work + lQT);
		ATTAC_TAC(1)
	}else if(ortho == 'R'){
		ATTAC_TIC(2, "L Compress")
		/**< Left orthogonalization */
		pattacLOrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
		/**< Compress a left orthogonal TT tensor */
		pattacLCompressImplicitQs(comm, isRedundant, d, n, rx, x, tau, work, work + lQT);
		ATTAC_TAC(2)
	}else{
		fprintf(stderr, "%s:Line %d %s::Not known orthogonalization side\n", __FILE__, __LINE__, __func__);
		MPI_Abort(comm, -99);
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
 * \brief Left orthogonalization of a TT tensor with TT. Q factors of cores are given implicitly as TSQR tree.
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  threshold for truncation
 * \param QT   (output) Contains at position \f$(\sum_{i=0}^{k-1} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k-1\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i])\f$. If you want to pass this argument to pattacBFLCompressImplicitQs the space required is the same previous relation with \f$(\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + r(2r + 1)\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFLOrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
	double* R = work + rmax * (2 * rmax + 1);

	int QToffset = 0;
	for(int i = 0; i < d - 1; i++){
		double* v = QT + QToffset;
		double* t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i] * n[i];
		ATTAC_TIC(1, "C Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }else{
		  attacBFTSQR(comm, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)

		/**
		 * \remarks Multiplies R by the rightt TT core.
		 */
		/**< x[i + 1] = R x[i + 1] */
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		ATTAC_TIC(3, "Update prev core")
    double done = 1.0;
    int nprxp = n[i + 1] * rx[i + 2];
    dtrmm_("L", "U", "N", "N", &rx[i + 1], &nprxp, &done, R, &rx[i + 1], x[i + 1], &rx[i + 1]);
		ATTAC_TAC(3)
		QToffset += 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  Truncation threshold
 * \param QT   (input) Contains at position \f$(\sum_{i=0}^{k-1} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k-1\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$, where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + 2r^2 + 2 r + \max(n[i] rx[i] rx[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFLCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	int rmax = 0;
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *z = NULL;
	double *R = NULL;

	int rankSVD = 0;
  int nmax = 0;
  for(int i = 0; i < d - 1; i++){
    nmax = (n[i] > nmax) ? n[i] : nmax;
  }

  // Let v and t point to the end of QT
  int QToffset = 0;
  for(int i = 0; i < d - 1; i++){
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    QToffset += 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    rmax = (rmax > rx[i + 1]) ? rmax : rx[i + 1];
  }
  v = QT + QToffset;
  t = v + (1 + CLog2P) * rx[d - 1] * rx[d - 1];
	
  // TSQR work space
  R = work + (2 * rmax + 1) * rmax;
  sig = R + rmax * rmax;
  superb = sig + rmax;
  // Place to compute V^T H(x[i]) and V(x[i]) U Sig
  z = superb + rmax;

  // Truncate last core
  /// TSQR: H(x[d - 1]) = LQ
  if(isRedundant[d - 1]){
    ATTAC_TIC(1, "R Maj BFTSQR")
		attacBFTSQR(MPI_COMM_SELF, 'R', n[d - 1], rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], R, v, t, work);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(1, "R Maj BFTSQR")
		attacBFTSQR(comm,          'R', n[d - 1], rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], R, v, t, work);
    ATTAC_TAC(1)
    ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[d - 2] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[d - 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
    ATTAC_TAC(2)
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(3, "SVD")
  // SVD of the L factor named R here, R will contain the left singular vectors, work will contain the right singular vectors
	ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[d - 1], rx[d - 1], R, rx[d - 1], sig, NULL, 1, work, rx[d - 1], superb);
  ATTAC_TAC(3)
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
		MPI_Abort(MPI_COMM_WORLD, ierr);
#else
	 exit(1);
#endif
	}

  // Rank of L = U sig V^T
  for(int j = 0; j < rx[d - 1]; j++){
    if(sig[j] < tau){
      break;
    }else{
      rankSVD = j + 1;
    }
  }

  // Scale the first rankSVD columns of U 
  for(int j = 0; j < rankSVD; j++){
    for(int k = 0; k < rx[d - 1]; k++){
      R[k + j * rx[d - 1]] *= sig[j];
    }
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(2, "BFBCAST R")
  // Either broadcast rankSVD or memset z to 0 for rx[d - 1] * n[d - 1] and copy nothing out of root
  if(isRedundant[d - 1] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
    attacBFOptBcastRank(comm, rankSVD);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
  }
  ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  // put V^T in z
  memset(z, 0, rankSVD * n[d - 1] * sizeof(double));
  dlacpy_("A", &rankSVD, &rx[d - 1], work, &rx[d - 1], z, &rankSVD);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(5, "R Maj BFApply Q")
  // Apply x[d - 1] on z
  if(isRedundant[d - 1]){
	  attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, z, rankSVD, work);
  }else{
    attacBFTSQRApplyQ(comm,          'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, z, rankSVD, work);
  }
  ATTAC_TAC(5)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  dlacpy_("A", &rankSVD, &n[d - 1], z, &rankSVD, x[d - 1], &rankSVD);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

  //Loop apply z = x[i] on U Sig. Then TSQR z and SVD of R
  for(int i = d - 2; i > -1; i--){
    // Set to apply x[i] on U Sig
    QToffset -= 2 * (1 + CLog2P) * rx[i + 1] * rx[i + 1];
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
    ATTAC_TIC(4, "Copy and memset")
    // set z to 0
    memset(z, 0, rx[i] * n[i] * rx[i + 1] * sizeof(double)); //TODO to change rx[i + 1] by rankSVD
    // copy U Sig to z
    int rxn = rx[i] * n[i];
    dlacpy_("A", &rx[i + 1], &rankSVD, R, &rx[i + 1], z, &rxn);
    ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    ATTAC_TIC(6, "C Maj BFApply Q")
    // Apply Q
    if(isRedundant[i]){
	    attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', n[i] * rx[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, z, n[i] * rx[i], work);
    }else{
	    attacBFTSQRApplyQ(comm,          'C', n[i] * rx[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, z, n[i] * rx[i], work);
    }
    ATTAC_TAC(6)
    // Set new rank
    rx[i + 1] = rankSVD;

    if(i > 0){
      //////////////////////////////////
      // Truncate current core from left
      //////////////////////////////////
      /// TSQR: H(z) = LQ
      t = v + (1 + CLog2P) * rx[i] * rx[i];
      if(isRedundant[i]){
        ATTAC_TIC(1, "R Maj BFTSQR")
	      attacBFTSQR(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], rx[i], rx[i], z, rx[i], R, v, t, work);
        ATTAC_TAC(1)
      }else{
        ATTAC_TIC(1, "R Maj BFTSQR")
	      attacBFTSQR(comm,          'R', n[i] * rx[i + 1], rx[i], rx[i], z, rx[i], R, v, t, work);
        ATTAC_TAC(1)
        ATTAC_TIC(2, "BFBCAST R")
        if(isRedundant[i - 1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
          attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
        }
        ATTAC_TAC(2)
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(3, "SVD")
      // SVD of the L factor named R here, R will contain the left singular vectors, work will contain the right singular vectors
  	  ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[i], rx[i], R, rx[i], sig, NULL, 1, work, rx[i], superb);
      ATTAC_TAC(3)
  	  if(ierr != 0){
  		  fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
  		  MPI_Abort(MPI_COMM_WORLD, ierr);
#else
  		  exit(1);
#endif
  	  }
      // Rank of L = U sig V^T
      for(int j = 0; j < rx[i]; j++){
        if(sig[j] < tau){
          break;
        }else{
          rankSVD = j + 1;
        }
      }
      // Scale the first rankSVD columns of U 
      for(int j = 0; j < rankSVD; j++){
        for(int k = 0; k < rx[i]; k++){
          R[k + j * rx[i]] *= sig[j];
        }
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(2, "BFBCAST R")
      if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      // Either broadcast rankSVD or memset z to 0 for rx[i] * n[i] * rx[i + 1] and copy nothing out of root
        attacBFOptBcastRank(comm, rankSVD);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
      }
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      //Apply Q from z on x[i] after copying U to x[i]
      memset(x[i], 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
      dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], x[i], &rankSVD);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(5, "R Maj BFApply Q")
      // Apply Q
      if(isRedundant[i]){
	      attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], z, rx[i], v, t, x[i], rankSVD, work);
      }else{
	      attacBFTSQRApplyQ(comm,          'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], z, rx[i], v, t, x[i], rankSVD, work);
      }
      ATTAC_TAC(5)
    }else{
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      dlacpy_("A", &n[0], &rx[1], z, &n[0], x[0], &n[0]);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work);
 * \brief Rightt orthogonalization of a TT tensor with TT. Q factors of cores are given implicitly as TSQR tree.
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param QT   (output) Contains at position \f$(\sum_{i=d-2}^{k} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i])\f$. If you want to pass this argument to pattacRCompressImplicitQs the space required is the same previous relation with \f$(\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$
 * \param work minimal space: \f$r^2 + r (2r + 1)\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFROrthogonalizationImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, double* QT, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
	double* R = work + rmax * (2 * rmax + 1);

	int QToffset = 0;
	for(int i = d - 1; i > 0; i--){
		double* v = QT + QToffset;
		double* t = v + (1 + CLog2P) * rx[i] * rx[i];
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i + 1] * n[i];
		ATTAC_TIC(1, "R Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacBFTSQR(comm,          'R', rxn, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif

		/**
		 * \remarks Multiplies R by the rightt TT core.
		 */
		/**< x[i - 1] = x[i - 1] R */
		ATTAC_TIC(3, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    dtrmm_("R", "L", "N", "N", &rxmnm, &rx[i], &done, R, &rx[i], x[i - 1], &rxmnm);
		ATTAC_TAC(3)
		QToffset += 2 * (1 + CLog2P) * rx[i] * rx[i];
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  threshold for truncation
 * \param QT   (input) Contains at position \f$(\sum_{i=d-2}^{k} (CLog2P + 1) * rx[i + 1] rx[i + 1])\f$ the TSQR tree Q factors of the TT core \f$k\f$ and the corresponding Householder reflectors \f$[Q, T]\f$. Minimal space required: \f$2 Srr (1 + CLog2P) r^2\f$ where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$.
 * \param work minimal space: \f$r (2 r + 1) + r^2 + 2 r + \max(n[i] r[i] r[i+1])\f$, where \f$r = \max(rx[i])\f$ and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFRCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, double* QT, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	int rmax = 0;
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *z = NULL;
	double *R = NULL;

	int rankSVD = 0;
  int nmax = 0;
  for(int i = 0; i < d - 1; i++){
    nmax = (n[i] > nmax) ? n[i] : nmax;
  }

  // Let v and t point to the end of QT
  int QToffset = 0;
  for(int i = d - 1; i > 0; i--){
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i] * rx[i];
    QToffset += 2 * (1 + CLog2P) * rx[i] * rx[i];
    rmax = (rmax > rx[i]) ? rmax : rx[i];
  }
  v = QT + QToffset;
  t = v + (1 + CLog2P) * rx[1] * rx[1];
	
  // Truncate last core
  /// TSQR: H(x[0]) = LQ
  R = work + (2 * rmax + 1) * rmax;
  sig = R + rmax * rmax;
  superb = sig + rmax;
  // Place to compute V^T H(x[i]) and V(x[i]) U Sig
  z = superb + rmax;

  if(isRedundant[0]){
    ATTAC_TIC(1, "C Maj BFTSQR")
		attacBFTSQR(MPI_COMM_SELF, 'C', n[0], rx[1], rx[1], x[0], n[0], R, v, t, work);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(1, "C Maj BFTSQR")
		attacBFTSQR(comm,          'C', n[0], rx[1], rx[1], x[0], n[0], R, v, t, work);
    ATTAC_TAC(1)
    ATTAC_TIC(2, "BFBCAST")
    if(isRedundant[1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
    ATTAC_TAC(2)
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(3, "SVD")
  // SVD of the R factor, R will contain the right singular vectors, work will contain the left singular vectors
	ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[1], rx[1], R, rx[1], sig, work, rx[1], NULL, 1, superb);
  ATTAC_TAC(3)
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
		MPI_Abort(MPI_COMM_WORLD, ierr);
#else
	 exit(1);
#endif
	}

  // Rank of R = U sig V^T
  for(int j = 0; j < rx[1]; j++){
    if(sig[j] < tau){
      break;
    }else{
      rankSVD = j + 1;
    }
  }

  // Scale the first rankSVD columns of V, rows of V^T
  for(int k = 0; k < rankSVD; k++){
    for(int j = 0; j < rx[1]; j++){
      R[k + j * rx[1]] *= sig[k];
    }
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(2, "BFBCAST")
  // Either broadcast rankSVD or memset z to 0 for rx[1] * n[0] and copy nothing out of root
  if(isRedundant[0] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
    attacBFOptBcastRank(comm, rankSVD);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
  }
  ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  // put U in z
  memset(z, 0, rankSVD * n[0] * sizeof(double));
  dlacpy_("A", &rx[1], &rankSVD, work, &rx[1], z, &n[0]);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  ATTAC_TIC(5, "C Maj BFApply Q")
  // Apply x[0] on z
  if(isRedundant[0]){
	  attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', n[0], &rankSVD, rx[1], rx[1], x[0], n[0], v, t, z, n[0], work);
  }else{
	  attacBFTSQRApplyQ(comm,          'C', n[0], &rankSVD, rx[1], rx[1], x[0], n[0], v, t, z, n[0], work);
  }
  ATTAC_TAC(5)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  ATTAC_TIC(4, "Copy and memset")
  dlacpy_("A", &n[0], &rankSVD, z, &n[0], x[0], &n[0]);
  ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

  //Loop apply z = x[i] on Sig V^T. Then TSQR z and SVD of R
  for(int i = 1; i < d; i++){
    // Set up to apply x[i] on Sig V^T
    QToffset -= 2 * (1 + CLog2P) * rx[i] * rx[i];
    v = QT + QToffset;
    t = v + (1 + CLog2P) * rx[i] * rx[i];
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
    ATTAC_TIC(4, "Copy and memset")
    // set z to 0
    memset(z, 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
    // copy Sig V^T to z
    dlacpy_("A", &rankSVD, &rx[i], R, &rx[i], z, &rankSVD);
    ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    ATTAC_TIC(6, "R Maj BFTSQR")
    // Apply Q
    if(isRedundant[i]){
	    attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], x[i], rx[i], v, t, z, rankSVD, work);
    }else{
	    attacBFTSQRApplyQ(comm,          'R', n[i] * rx[i + 1], &rankSVD, rx[i], rx[i], x[i], rx[i], v, t, z, rankSVD, work);
    }
    ATTAC_TAC(6)
    // Set new rank
    rx[i] = rankSVD;
    if(i < d - 1){
      ///////////////////////////////////
      // Truncate current core from right
      ///////////////////////////////////
      /// TSQR: V(z) = QR
      t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1];
      if(isRedundant[i]){
        ATTAC_TIC(1, "C Maj BFTSQR")
	      attacBFTSQR(MPI_COMM_SELF, 'C', n[i] * rx[i], rx[i + 1], rx[i + 1], z, rx[i] * n[i], R, v, t, work);
        ATTAC_TAC(1)
      }else{
        ATTAC_TIC(1, "C Maj BFTSQR")
	      attacBFTSQR(comm,          'C', n[i] * rx[i], rx[i + 1], rx[i + 1], z, rx[i] * n[i], R, v, t, work);
        ATTAC_TAC(1)
        ATTAC_TIC(2, "BFBCAST")
        if(isRedundant[i + 1] == 1){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
          attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
        }
        ATTAC_TAC(2)
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(3, "SVD")
      // SVD of the R factor, R will contain the right singular vectors, work will contain the left singular vectors
  	  ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[i + 1], rx[i + 1], R, rx[i + 1], sig, work, rx[i + 1], NULL, 1, superb);
      ATTAC_TAC(3)
  	  if(ierr != 0){
  		  fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
  		  MPI_Abort(MPI_COMM_WORLD, ierr);
#else
  		  exit(1);
#endif
  	  }
      // Rank of R = U sig V^T
      for(int j = 0; j < rx[i + 1]; j++){
        if(sig[j] < tau){
          break;
        }else{
          rankSVD = j + 1;
        }
      }
      // Scale the first rankSVD columns of V, (first rankSVD rows of V^T) 
      for(int j = 0; j < rx[i + 1]; j++){
        for(int k = 0; k < rankSVD; k++){
          R[k + j * rx[i + 1]] *= sig[k];
        }
      }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(2, "BFBCAST")
      // Either broadcast rankSVD or memset z to 0 for rx[i] * n[i] * rx[i + 1] and copy nothing out of root
      if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
        attacBFOptBcastRank(comm, rankSVD);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
      }
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      //Apply Q from z on x[i] after copying U to x[i]
      memset(x[i], 0, rx[i] * n[i] * rankSVD * sizeof(double));
      int rxn = rx[i] * n[i];
      dlacpy_("A", &rx[i + 1], &rankSVD, work, &rx[i + 1], x[i], &rxn);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
      ATTAC_TIC(5, "C Maj BFApply Q")
      // Apply Q
      if(isRedundant[i]){
	      attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], z, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }else{
	      attacBFTSQRApplyQ(comm,          'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], z, rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }
      ATTAC_TAC(5)
    }else{
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
      ATTAC_TIC(4, "Copy and memset")
      dlacpy_("A", &rx[d - 1], &n[d - 1], z, &rx[d - 1], x[d - 1], &rx[d - 1]);
      ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
    }
  }
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output
 * \param x    input/output
 * \param tau  threshold for truncation
 * \param ortho Orthogonalization state of the tensor at the end of the procedure
 * \param work minimal space: \f$2 Srr (1 + CLog2P) r^2 + r (2 r + 1) + r^2 + 2 r + \max(n[i] r[i] r[i+1])\f$, where \f$r = \max(rx[i])\f$, where \f$Srr = (\sum_{i=1}^{d-1} rx[i] rx[i] + r^2)\f$, and \f$P\f$ is number of procs
 * \remarks
 * \warning
*/
void pattacBFCompressImplicitQs(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, double *work){
	ATTAC_START_TIMER
	int rank = 0, size = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	/**< Butterfly Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
	int Srr = 0;
  int rmax = 0;
	for(int i = 0; i < d; i++){
    rmax = (rmax < rx[i + 1]) ? rx[i + 1] : rmax;
	}
	for(int i = 1; i < d; i++){
		Srr += rx[i] * rx[i];
	}
	Srr += rmax * rmax;
	int lQT = 2 * Srr * (1 + CLog2P);

	if(ortho == 'L'){
		ATTAC_TIC(1, "R Compress")
		/**< Right orthogonalization */
		pattacBFROrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
		/**< Compress a right orthogonal TT tensor */
		pattacBFRCompressImplicitQs(comm, isRedundant, d, n, rx, x, tau, work, work + lQT);
		ATTAC_TAC(1)
	}else if(ortho == 'R'){
		ATTAC_TIC(2, "L Compress")
		/**< Left orthogonalization */
		pattacBFLOrthogonalizationImplicitQs(comm, isRedundant, d, n, rx, x, work, work + lQT);
		/**< Compress a left orthogonal TT tensor */
		pattacBFLCompressImplicitQs(comm, isRedundant, d, n, rx, x, tau, work, work + lQT);
		ATTAC_TAC(2)
	}else{
		fprintf(stderr, "%s:Line %d %s::Not known orthogonalization side\n", __FILE__, __LINE__, __func__);
		MPI_Abort(comm, -99);
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLOrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx input destroyed after call
 * \param x  input destroyed after call
 * \param ry ouptut contain ranks of the left orthogonalized TT
 * \param y  output left orthogonalized TT
 * \param work minimum space: \f$r(2r + 1) + r^2 + 2 (1 + \ceil(Log2(P)) r^2\f$, where \f$P\f$ is the number of processors, and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFLOrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Butterfly Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

	for(int i = 0; i < d - 1; i++){
		double* R = work + rx[i + 1] * (2 * rx[i + 1] + 1);
		double* v = R + rx[i + 1] * rx[i + 1];
		double* t = v + (1 + CLog2P) * rx[i + 1] * rx[i + 1]; 
		/**
		 * \remarks Compute a QR factorization of x[i]_{(2)}^T.
		 */
		int rxn = rx[i] * n[i];
		ATTAC_TIC(1, "C Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }else{
		  attacBFTSQR(comm, 'C', rxn, rx[i + 1], rx[i + 1], x[i], rxn, R, v, t, work);
    }
		ATTAC_TAC(1)

		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i + 1] = R x[i + 1] */
		ATTAC_TIC(3, "Update next core")
    if(i < d - 2){
      double done = 1.0;
      int nprxp = n[i + 1] * rx[i + 2];
      dtrmm_("L", "U", "N", "N", &rx[i + 1], &nprxp, &done, R, &rx[i + 1], x[i + 1], &rx[i + 1]);
    }else{
      int nprxp = n[i + 1] * rx[i + 2];
      double done = 1.0;
      double zero = 0.0;
      dgemm_("N", "N", &rx[i + 1], &nprxp, &rx[i + 1], &done, R, &rx[i + 1], x[i + 1], &rx[i + 1], &zero, y[i + 1], &rx[i + 1]);
    }
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Copy and memset")
		memset(y[i], 0, rxn * rx[i + 1] * sizeof(double));
		for(int k = 0; k < rx[i + 1]; k++){
			y[i][k + rxn * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxip1 = rx[i + 1];
		ATTAC_TIC(5, "C Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], x[i], rxn, v, t, y[i], rxn, work);
    }else{
		  attacBFTSQRApplyQ(comm, 'C', rxn, &(rxip1), rx[i + 1], rx[i + 1], x[i], rxn, v, t, y[i], rxn, work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFLCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output on input contains ranks of left orthogoanl x. The output of pattacBFLOrthogonalizationOutplace
 * \param x    input/output on input contains left orthogonal tensor. The output of pattacBFLOrthogonalizationOutplace
 * \param tau  Threshold for truncation
 * \param ry   internally required
 * \param y    internally required
 * \param work minimum space: \f$r(2r + 1) + + r^2 + 2r + 2(1 + \ceil(Log2(P)) r^2\f$, where \f$P\f$ is the number of processors, and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFLCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = rx[1];
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *R = NULL;

	int rankSVD = 0;
  R = work + rmax * (2 * rmax + 1);
	sig = R + rmax * rmax;
  superb = sig + rmax;
  v = superb + rmax;
  t = v + (1 + CLog2P) * rmax * rmax;
	for(int i = d - 1; i > 0; i--){
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "R Maj BFTSQR")
    if(isRedundant[i]){
      if(i == d - 1){
		    attacBFTSQR(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], rx[i], rx[i], x[i], rx[i], R, v, t, work);
      }else{                                          
		    attacBFTSQR(MPI_COMM_SELF, 'R', n[i] * rx[i + 1], rx[i], rx[i], y[i], rx[i], R, v, t, work);
      }                                               
    }else{                                            
      if(i == d - 1){                                 
		    attacBFTSQR(comm,          'R', n[i] * rx[i + 1], rx[i], rx[i], x[i], rx[i], R, v, t, work);
      }else{                                          
		    attacBFTSQR(comm,          'R', n[i] * rx[i + 1], rx[i], rx[i], y[i], rx[i], R, v, t, work);
      }
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif

		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'O', 'S', rx[i], rx[i], R, rx[i], sig, NULL, 1, work, rx[i], superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}

		/**< Copy V^T to x[i] */
		ATTAC_TIC(3, "Copy and memset")
    if(i == d - 1){
		  memset(y[i], 0, rankSVD * n[i] * sizeof(double));
      dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], y[i], &rankSVD);
    }else{
		  memset(x[i], 0, rankSVD * n[i] * rx[i + 1] * sizeof(double));
      dlacpy_("A", &rankSVD, &rx[i], work, &rx[i], x[i], &rankSVD);
    }
		ATTAC_TAC(3)

		/**< Scale the column j in U by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i]; k++){
				R[j * rx[i] + k] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "R Maj BFApply Q")
    if(isRedundant[i]){
      if(i == d - 1){
		    attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, y[d - 1], rankSVD, work);
        dlacpy_("A", &rankSVD, &n[d - 1], y[d - 1], &rankSVD, x[d - 1], &rankSVD);
      }else{
		    attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], y[i], rx[i], v, t, x[i], rankSVD, work);
      }
    }else{
      if(i == d - 1){
		    attacBFTSQRApplyQ(comm, 'R', n[d - 1], &rankSVD, rx[d - 1], rx[d - 1], x[d - 1], rx[d - 1], v, t, y[d - 1], rankSVD, work);
        dlacpy_("A", &rankSVD, &n[d - 1], y[d - 1], &rankSVD, x[d - 1], &rankSVD);
      }else{
		    attacBFTSQRApplyQ(comm, 'R', rx[i + 1] * n[i], &rankSVD, rx[i], rx[i], y[i], rx[i], v, t, x[i], rankSVD, work);
      }
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the previous core x[i - 1] */
		ATTAC_TIC(6, "Update prev core")
    int rxmnm = rx[i - 1] * n[i - 1];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rxmnm, &rankSVD, &rx[i], &done, x[i - 1], &rxmnm, R, &rx[i], &zero, y[i - 1], &rxmnm);
		ATTAC_TAC(6)

		rx[i] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
  for(int i = 0; i < d + 1; i++){
    ry[i] = rx[i];
  }
	ATTAC_TIC(3, "Copy and memset")
  dlacpy_("A", &n[0], &rx[1], y[0], &n[0], x[0], &n[0]);
  ATTAC_TAC(3)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFROrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx input destroyed after call
 * \param x  input destroyed after call
 * \param ry ouptut contain ranks of the right orthogonalized TT
 * \param y  output right orthogonalized TT
 * \param work minimum space: \f$r (2r + 1) + 2 (1 + \ceil(Log2(P)) * r^2\f$, where \f$P\f$ is the number of processors, and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFROrthogonalizationOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double **x, int *ry, double **y, double* work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	/**< Butterfly Tree Depth*/
  int CLog2P = (int) ceil(log2((double)size));

#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
  for(int i = 0; i < d + 1; i++){
    ry[i] = rx[i];
  }
	double* R = work + rmax * (2 * rmax + 1);
	double* v = R + rmax * rmax;
	double* t = v + (1 + CLog2P) * rmax * rmax; 

	for(int i = d - 1; i > 0; i--){
		/**
		 * \remarks We want to compute an LQ factorization of x[i]_{(0)} (unfolding in mode 0). To do so
		 *	        we unfold in the 0 mode in order to perform a QR decomposition with row major layout 
		 *          of x[i]. Note that the data in memory are in comlumn major layout
		 */
		int nrx = n[i] * rx[i + 1];
		ATTAC_TIC(1, "R Maj BFTSQR")
    if(isRedundant[i]){
		  attacBFTSQR(MPI_COMM_SELF, 'R', nrx, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }else{
		  attacBFTSQR(comm,          'R', nrx, rx[i], rx[i], x[i], rx[i], R, v, t, work);
    }
		ATTAC_TAC(1)
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'R', rx[i], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**
		 * \remarks Multiplies R by the left TT core.
		 */
		/**< x[i - 1] := x[i - 1] R */
		ATTAC_TIC(3, "Update prev core")
    if(i != 1){
      int rxmnm = rx[i - 1] * n[i - 1];
      double done = 1.0;
      dtrmm_("R", "L", "N", "N", &rxmnm, &rx[i], &done, R, &rx[i], x[i - 1], &rxmnm);
    }else{
      int rxmnm = rx[i - 1] * n[i - 1];
      double done = 1.0;
      double zero = 0.0;
		  dgemm_("N", "N", &rxmnm, &rx[i], &rx[i], &done, x[i - 1], &rxmnm, R, &rx[i], &zero, y[i - 1], &rxmnm);
    }
		ATTAC_TAC(3)

		/**< Generate Q of x[i]_{(2)}^T*/
		ATTAC_TIC(4, "Memset")
		memset(y[i], 0, rx[i] * nrx * sizeof(double));
		for(int k = 0; k < rx[i]; k++){
			y[i][k + rx[i] * k] = 1.;
		}
		ATTAC_TAC(4)
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
		int rxi = rx[i];
		ATTAC_TIC(5, "R Maj BFApply Q")
    if(isRedundant[i]){
		  attacBFTSQRApplyQ(MPI_COMM_SELF, 'R', nrx, &(rxi), rx[i], rx[i], x[i], rx[i], v, t, y[i], rx[i], work);
    }else{
		  attacBFTSQRApplyQ(comm,          'R', nrx, &(rxi), rx[i], rx[i], x[i], rx[i], v, t, y[i], rx[i], work);
    }
		ATTAC_TAC(5)
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFRCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx   input/output ranks of compressed tensor
 * \param x    input/output compressed tensor
 * \param tau  Truncation threshold
 * \param ry   interally needed
 * \param y    internally needed
 * \param work minimum space: \f$r(2r + 1) + + r^2 + 2r + 2(1 + \ceil(Log2(P)) r^2\f$, where \f$P\f$ is the number of processors, and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFRCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, int *ry, double **y, double *work){
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
	/**< Binary Tree Depth */
  int CLog2P = (int) ceil(log2((double)size));

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int ierr = 0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 1; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double *sig = NULL;
	double *superb = NULL;
	double *v = NULL;
	double *t = NULL;
	double *R = NULL;

	int rankSVD = 0;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	
	R = work + rmax * (2 * rmax + 1);
	sig = R + rmax * rmax;
	superb = sig + rmax;
	v = superb + rmax;
	t = v + (1 + CLog2P) * rmax * rmax;
	for(int i = 0; i < d - 1; i++){
		/**< Compute the R factor and implicit Q factor */
		ATTAC_TIC(1, "C Maj BFTSQR")
    if(isRedundant[i]){
      if(i == 0){
		    attacBFTSQR(MPI_COMM_SELF, 'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
      }else{
		    attacBFTSQR(MPI_COMM_SELF, 'C', rx[i] * n[i], rx[i + 1], rx[i + 1], y[i], rx[i] * n[i], R, v, t, work);
      }
    }else{
      if(i == 0){
		    attacBFTSQR(comm,          'C', rx[i] * n[i], rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], R, v, t, work);
      }else{
		    attacBFTSQR(comm,          'C', rx[i] * n[i], rx[i + 1], rx[i + 1], y[i], rx[i] * n[i], R, v, t, work);
      }
    }
		ATTAC_TAC(1)
		
		ATTAC_TIC(2, "BFBCAST R")
    if(isRedundant[i] == 0){
#if TIME_BREAKDOWN
		BCAST_TIMER_TIC
#endif
      attacBFOptBcastR(comm, 'C', rx[i + 1], R, work);
#if TIME_BREAKDOWN
		MPI_Barrier(comm);
		BCAST_TIMER_TAC
#endif
    }
		ATTAC_TAC(2)
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< SVD factor \f$R \approx U \Sigma V^T\f$ */
		ATTAC_TIC(4, "SVD")
		ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'O', rx[i + 1], rx[i + 1], R, rx[i + 1], sig, work, rx[i + 1], NULL, 1, superb);
		ATTAC_TAC(4)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
			fprintf(stderr, "core %d compression, r[%d]  = %d \n r[%d] = %d \n n[%d] = %d\n", i, i, rx[i], i + 1, rx[i + 1], i, n[i]);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

		/**< Tau-Rank of R */
		rankSVD = 0;
		for(int j = 0; j < rx[i + 1]; j++){
			if(sig[j] > tau){
				rankSVD = j + 1;
			}else{
				break;
			}
		}

		/**< Copy U to x[i] */
    int rxn = rx[i] * n[i];
		ATTAC_TIC(3, "Copy and memset")
    if(i == 0){
		  memset(y[0], 0, n[0] * rankSVD * sizeof(double));
      dlacpy_("A", &rx[1], &rankSVD, work, &rx[1], y[0], &n[0]);
    }else{
		  memset(x[i], 0, rx[i] * n[i] * rankSVD * sizeof(double));
      dlacpy_("A", &rx[i + 1], &rankSVD, work, &rx[i + 1], x[i], &rxn);
    }
		ATTAC_TAC(3)

		/**< Scale the column j in V by the j^th singular value */
		for(int j = 0; j < rankSVD; j++){
			for(int k = 0; k < rx[i + 1]; k++){
				R[j + k * rx[i + 1]] *= sig[j];
			}
		}
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif

		ATTAC_TIC(5, "C Maj BFApply Q")
    if(isRedundant[i]){
      if(i == 0){
		    attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, y[i], rx[i] * n[i], work);
        dlacpy_("A", &rxn, &rankSVD, y[i], &rxn, x[i], &rxn);
      }else{
		    attacBFTSQRApplyQ(MPI_COMM_SELF, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], y[i], rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }
    }else{
      if(i == 0){
		    attacBFTSQRApplyQ(comm, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], x[i], rx[i] * n[i], v, t, y[i], rx[i] * n[i], work);
        dlacpy_("A", &rxn, &rankSVD, y[i], &rxn, x[i], &rxn);
      }else{
		    attacBFTSQRApplyQ(comm, 'C', rx[i] * n[i], &rankSVD, rx[i + 1], rx[i + 1], y[i], rx[i] * n[i], v, t, x[i], rx[i] * n[i], work);
      }
    }
		ATTAC_TAC(5)

#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
		/**< Update the following core x[i + 1] */
		ATTAC_TIC(6, "Update following core")
    int nprxp = n[i + 1] * rx[i + 2];
    double done = 1.0;
    double zero = 0.0;
    dgemm_("N", "N", &rankSVD, &nprxp, &rx[i + 1], &done, R, &rx[i + 1], x[i + 1], &rx[i + 1], &zero, y[i + 1], &rankSVD);
		ATTAC_TAC(6)

		rx[i + 1] = rankSVD;
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	}
#if TIME_BREAKDOWN
		COMP_TIMER_TIC
#endif
	ATTAC_TIC(3, "Copy and memset")
  dlacpy_("A", &rx[d - 1], &n[d - 1], y[d - 1], &rx[d - 1], x[d - 1], &rx[d - 1]);
	ATTAC_TAC(3)
  for(int i = 0; i < d + 1; i++){
    ry[i] = rx[i];
  }
#if TIME_BREAKDOWN
		COMP_TIMER_TAC
#endif
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacBFCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, int *ry, double **y, double *work);
 * \brief
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx    input
 * \param x     input
 * \param tau   Truncation threshold
 * \param ortho Orthogonalization state of the tensor at the end of the procedure
 * \param ry    output
 * \param y     output
 * \param work minimum space: \f$r(2r + 1) + + r^2 + 2r + 2(1 + \ceil(Log2(P)) r^2\f$, where \f$P\f$ is the number of processors, and \f$r = \max(rx[i])\f$
 * \remarks
 * \warning
*/
void pattacBFCompressOutplace(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tau, const char ortho, int *ry, double **y, double *work){
	ATTAC_START_TIMER
	int rank = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	if(ortho == 'L'){
		ATTAC_TIC(1, "R Compress")
		/**< Right orthogonalization */
		pattacBFROrthogonalizationOutplace(comm, isRedundant, d, n, rx, x, ry, y, work);
		/**< Compress a right orthogonal TT tensor */
		pattacBFRCompressOutplace(comm, isRedundant, d, n, ry, y, tau, rx, x, work);
		ATTAC_TAC(1)
	}else if(ortho == 'R'){
		ATTAC_TIC(2, "L Compress")
		/**< Left orthogonalization */
		pattacBFLOrthogonalizationOutplace(comm, isRedundant, d, n, rx, x, ry, y, work);
		/**< Compress a left orthogonal TT tensor */
		pattacBFLCompressOutplace(comm, isRedundant, d, n, ry, y, tau, rx, x, work);
		ATTAC_TAC(2)
	}else{
		fprintf(stderr, "%s:Line %d %s::Not known orthogonalization side\n", __FILE__, __LINE__, __func__);
		MPI_Abort(comm, -99);
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn double pattacNormFromHadamard(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* work);
 * \brief  Computes the Frobenius norm a tensor y s.t. x = Hadamard(y,y)
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \remarks
 * \warning
*/
double pattacNormFromHadamard(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double* work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	double norm = 0.0;
	/**< Maximum TT rank */
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
	double* b = work;
	double* a = work + rmax;
	/**< Vector of ones */
	for(int i = 0; i < n[d - 1]; i++)
		a[i] = 1.;

  int one = 1;
  int rxn = rx[d - 1] * n[d - 1];
  double done = 1.0;
  double zero = 0.0;
	/**< x[d-1] ones */
  dgemv_("N", &rx[d - 1], &n[d - 1], &done, x[d - 1], &rx[d - 1], a, &one, &zero, b, &one);
	
  if(isRedundant[d - 1] == 0){
    ierr = MPI_Allreduce(MPI_IN_PLACE, b, rx[d - 1], MPI_DOUBLE, MPI_SUM, comm);
  }
  if(ierr != 0){
    fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
    MPI_Abort(MPI_COMM_WORLD, ierr);
#else
    exit(1);
#endif
  }
	
	for(int i = d - 2; i > -1; i--){
    rxn = rx[i] * n[i];
		double* swipe = a;
		a = b;
		b = swipe;
		memset(b, 0, rx[i] * sizeof(double));
		/** b = \sum_j x[i](j) a */
		for(int j = 0; j < n[i]; j++){
			/**< b += x[i](j) a */
      dgemv_("N", &rx[i], &rx[i + 1], &done, x[i] + j * rx[i], &rxn, a, &one, &done, b, &one);
		}
    if(isRedundant[i] == 0){
		  ierr = MPI_Allreduce(MPI_IN_PLACE, b, rx[i], MPI_DOUBLE, MPI_SUM, comm);
    }
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}
	norm = sqrt(b[0]);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return norm;
}

/** \fn double pattacNormSquaredHadamardOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work)
 * \brief Norm squared of a TT tensor
 * \details Compute norm squared of a TT tensor
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \remarks
 * \warning
*/
double pattacNormSquaredHadamardOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  double *a = work;
  double *b = work + rmax * rmax;

  int rxn = rx[0] * n[0];
  int nrx = n[0] * rx[1];
  double done = 1.0;
  double zero = 0.0;
  ATTAC_TIC(1, "GEMM")
  /**< initiate the first core product */
  dgemm_("T", "N", &rx[1], &rx[1], &rxn, &done, x[0], &rxn, x[0], &rxn, &zero, a, &rx[1]);
  /**< sum up along the mode */
#if TIME_BREAKDOWN
  if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(1)
  ATTAC_TIC(2, "Comm")
  if(isRedundant[0] == 0){
  	ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[1] * rx[1], MPI_DOUBLE, MPI_SUM, comm);
  }
#if TIME_BREAKDOWN
  if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(2)
  if(ierr != 0){
    fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
    MPI_Abort(MPI_COMM_WORLD, ierr);
#else
    exit(1);
#endif
  }

	for(int i = 1; i < d; i++){
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    /**< H(B) = M H(X_{i}) */
    ATTAC_TIC(1, "GEMM")
    dgemm_("N", "N", &rx[i], &nrx, &rx[i], &done, a, &rx[i], x[i], &rx[i], &zero, b, &rx[i]);
    /**< V(X_{i})^T V(B)  */
    dgemm_("T", "N", &rx[i + 1], &rx[i + 1], &rxn, &done, x[i], &rxn, b, &rxn, &zero, a, &rx[i + 1]);
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(1)
    ATTAC_TIC(2, "Comm")
    if(isRedundant[i] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[i + 1] * rx[i + 1], MPI_DOUBLE, MPI_SUM, comm);
    }
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(2)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return a[0];
}

/** \fn double pattacNormSquaredHadamardSymOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work, int *worki)
 * \brief Norm squared of a TT tensor
 * \details Compute norm squared of a TT tensor exploiting the symmetry
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \param worki
 * \remarks
 * \warning
*/
double pattacNormSquaredHadamardSymOpt(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *work, int *worki){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  double *a = work;
  double *b = work + rmax * rmax;

  int rxn = rx[0] * n[0];
  int nrx = n[0] * rx[1];
  double done = 1.0;
  double zero = 0.0;
  ATTAC_TIC(1, "Mult")
  /**< initiate the first core product */
  dsyrk_("L", "T", &rx[1], &rxn, &done, x[0], &rxn, &zero, a, &rx[1]);
#if TIME_BREAKDOWN
  if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(1)
  /**< sum up along the mode */
  ATTAC_TIC(2, "Comm")
  if(isRedundant[0] == 0){
	  ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[1] * rx[1], MPI_DOUBLE, MPI_SUM, comm);
    if(ierr != 0){
      fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
      MPI_Abort(MPI_COMM_WORLD, ierr);
#else
      exit(1);
#endif
    }
  }
#if TIME_BREAKDOWN
  if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(2)

	for(int i = 1; i < d; i++){
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    ATTAC_TIC(3, "FillSym")
    for(int j = 0; j < rx[i]; j++){
      for(int k = 0; k < j; k++){
        a[k + j * rx[i]] = a[j + k * rx[i]];
      }
    }
    ATTAC_TAC(3)
    ATTAC_TIC(6, "SVD")
    ierr = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'N', 'O', rx[i], rx[i], a, rx[i], b, NULL, 1, NULL, 1, b + rx[i]);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    ATTAC_TAC(6)
    ATTAC_TIC(7, "SigV")
    for(int j = 0; j < rx[i]; j++){
      for(int k = 0; k < rx[i]; k++){
        a[k + j * rx[i]] *= sqrt(b[k]);
      }
    }
    ATTAC_TAC(7)
    ATTAC_TIC(8, "QR")
    int lb = rx[i] * rx[i];
    dgeqlf_(&rx[i], &rx[i], a, &rx[i], b, b + rx[i], &lb, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::dgeqlf:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    ATTAC_TAC(8)
    // TRMM can be only used in place. Thus, either we copy and perform TRMM or use GEMM. In the latter case, zero the strict upper triangular part of a
    // Copy and TRMM 
      /*
    ATTAC_TIC(4, "Copy")
    memcpy(b, x[i], rx[i] * n[i] * rx[i + 1] * sizeof(double));
    ATTAC_TAC(4)
    ATTAC_TIC(5, "TRMM")
    cblas_dtrmm (CblasColMajor, CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, rx[i], rx[i + 1] * n[i], 1., a, rx[i], b, rx[i]);
    ATTAC_TAC(5)
    */
    // GEMM Usage
    ATTAC_TIC(1, "Mult")
		setLowerZero('R', rx[i], rx[i], a, rx[i]); // upper part of a is lower part of a^T (row major viewed)
    dgemm_("N", "N", &rx[i], &nrx, &rx[i], &done, a, &rx[i], x[i], &rx[i], &zero, b, &rx[i]);
    ATTAC_TAC(1)
  /**< V(B)^T V(B) */
    ATTAC_TIC(1, "Mult")
    dsyrk_("L", "T", &rx[i + 1], &rxn, &done, b, &rxn, &zero, a, &rx[i + 1]);
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(1)
    /********** End SVDChol  **********/

    ATTAC_TIC(2, "Comm")
    if(isRedundant[i] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[i + 1] * rx[i + 1], MPI_DOUBLE, MPI_SUM, comm);
    }
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(2)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return a[0];
}

/** \fn double pattacInnerProduct(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, double *work)
 * \brief Inner product of two TT tensors
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param ry
 * \param y
 * \param work
 * \remarks
 * \warning
*/
double pattacInnerProduct(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, const int *ry, double *const *y, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
		rmax = (rmax > ry[i]) ? rmax : ry[i];
	}
  double *a = work;
  double *b = work + rmax * rmax;

  int rxn = rx[0] * n[0];
  int nrx = n[0] * rx[1];
  int ryn = ry[0] * n[0];
  int nry = n[0] * ry[1];

  double done = 1.0;
  double zero = 0.0;
  ATTAC_TIC(1, "GEMM")
  /**< initiate the first core product */
  dgemm_("T", "N", &rx[1], &ry[1], &n[0], &done, x[0], &rxn, y[0], &ryn, &zero, a, &rx[1]);
#if TIME_BREAKDOWN
    if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(1)

  ATTAC_TIC(2, "Comm")
  /**< sum up along the mode */
  if(isRedundant[0] == 0){
	  ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[1] * ry[1], MPI_DOUBLE, MPI_SUM, comm);
  }
  ATTAC_TAC(2)
  if(ierr != 0){
    fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
    MPI_Abort(MPI_COMM_WORLD, ierr);
#else
    exit(1);
#endif
  }

	for(int i = 1; i < d; i++){
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    ryn = ry[i] * n[i];
    nry = n[i] * ry[i + 1];
    ATTAC_TIC(1, "GEMM")
    /**< H(B) = M H(Y_{i}) */
    dgemm_("N", "N", &rx[i], &nry, &ry[i], &done, a, &rx[i], y[i], &ry[i], &zero, b, &rx[i]);
    /**< V(X_{i})^T V(B)  */
    dgemm_("T", "N", &rx[i + 1], &ry[i + 1], &rxn, &done, x[i], &rxn, b, &rxn, &zero, a, &rx[i + 1]);
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(1)
    ATTAC_TIC(2, "Comm")
    if(isRedundant[i] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[i + 1] * ry[i + 1], MPI_DOUBLE, MPI_SUM, comm);
    }
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(2)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
	return a[0];
}

/** \fn void pattacGramLR(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GL, double *work)
 * \brief Inner product of two TT tensors
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \param GL Gram of left cores
 * \remarks
 * \warning
*/
void pattacGramLR(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GL, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  double *a = GL;
  double *b = work;

  int rxn = rx[0] * n[0];
  int nrx = n[0] * rx[1];

  double done = 1.0;
  double zero = 0.0;
  ATTAC_TIC(1, "GEMM")
  /**< initiate the first core product */
  dgemm_("T", "N", &rx[1], &rx[1], &n[0], &done, x[0], &rxn, x[0], &rxn, &zero, a, &rx[1]);
#if TIME_BREAKDOWN
    if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(1)

  ATTAC_TIC(2, "Comm")
  /**< sum up along the mode */
  if(isRedundant[0] == 0){
	  ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[1] * rx[1], MPI_DOUBLE, MPI_SUM, comm);
  }
  ATTAC_TAC(2)
  if(ierr != 0){
    fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
    MPI_Abort(MPI_COMM_WORLD, ierr);
#else
    exit(1);
#endif
  }

	for(int i = 1; i < d; i++){
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    ATTAC_TIC(1, "GEMM")
    /**< H(B) = M H(X_{i}) */
    dgemm_("N", "N", &rx[i], &nrx, &rx[i], &done, a, &rx[i], x[i], &rx[i], &zero, b, &rx[i]);
    /**< V(X_{i})^T V(B)  */
    a = a + rx[i] * rx[i];
    dgemm_("T", "N", &rx[i + 1], &rx[i + 1], &rxn, &done, x[i], &rxn, b, &rxn, &zero, a, &rx[i + 1]);
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(1)
    ATTAC_TIC(2, "Comm")
    if(isRedundant[i] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, a, rx[i + 1] * rx[i + 1], MPI_DOUBLE, MPI_SUM, comm);
    }
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(2)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

/** \fn void pattacGramRL(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GR, double *work)
 * \brief Inner product of two TT tensors
 * \details
 * \param comm
 * \param isRedundant
 * \param d
 * \param n
 * \param rx
 * \param x
 * \param work
 * \param GR Gram of right cores
 * \remarks
 * \warning
*/
void pattacGramRL(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, const int *rx, double *const *x, double *GR, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  double *a = GR;
  double *b = work;

  int rxn = rx[d-1] * n[d-1];
  int nrx = n[d-1] * rx[d];
  int rxi = rx[d-1];
  int rxip1 = rx[d];

  double done = 1.0;
  double zero = 0.0;
  ATTAC_TIC(1, "GEMM")
  /**< initiate the lat core product */
  dgemm_("N", "T", &rxi, &rxi, &nrx, &done, x[d-1], &rxi, x[d-1], &rxi, &zero, a, &rxi);
#if TIME_BREAKDOWN
    if(isRedundant[d-1] == 0) MPI_Barrier(comm);
#endif
  ATTAC_TAC(1)

  ATTAC_TIC(2, "Comm")
  /**< sum up along the mode */
  if(isRedundant[d-1] == 0){
	  ierr = MPI_Allreduce(MPI_IN_PLACE, a, rxi * rxi, MPI_DOUBLE, MPI_SUM, comm);
  }
  ATTAC_TAC(2)
  if(ierr != 0){
    fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
    MPI_Abort(MPI_COMM_WORLD, ierr);
#else
    exit(1);
#endif
  }

	for(int i = d-2; i > -1; i--){
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    rxi = rx[i];
    rxip1 = rx[i + 1];

    ATTAC_TIC(1, "GEMM")
    /**< V(B) = V(X_{i}) M */
    dgemm_("N", "N", &rxn, &rxip1, &rxip1, &done, x[i], &rxn, a, &rxip1, &zero, b, &rxn);
    /**< H(B) H(X_{i})^T */
    a = a + rx[i+1] * rx[i+1];
    dgemm_("N", "T", &rxi, &rxi, &nrx, &done, b, &rxi, x[i], &rxi, &zero, a, &rxi);
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(1)
    ATTAC_TIC(2, "Comm")
    if(isRedundant[i] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, a, rxi * rxi, MPI_DOUBLE, MPI_SUM, comm);
    }
#if TIME_BREAKDOWN
    if(isRedundant[i] == 0) MPI_Barrier(comm);
#endif
    ATTAC_TAC(2)
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
	}
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
	ATTAC_STOP_TIMER
}

void pattacGramCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  int rankTT = 0;

  double *GL = NULL, *GR = NULL;
  int lG = 0;
  for(int i = 0; i < d; i++){
    lG += rx[i] * rx[i];
  }
  
  double done = 1.0;
  double zero = 0.0;

  // product of Gram matrices
  double *M = work + 2 * lG;

  // Gram of left cores
  GL = work;
  pattacGramLR(comm, isRedundant, d, n, rx, x, GL, work + 2 * lG);

  // Gram of right cores
  GR = work + lG;
  pattacGramRL(comm, isRedundant, d, n, rx, x, GR, work + 2 * lG);
  
  // offset to point to the Gram of current core
  int Loffset = 0;
  int Roffset = lG - rx[0] * rx[0] - rx[1] * rx[1];

  // thrshold for truncation
  double tau = tol * tol * GL[lG - 1]/((double) d - 1);

  // eigenvalues of Gram of left cores and right factors and singular values of M
  double *Lsig = work + 2 * lG + rmax * rmax;
  double *Rsig = work + 2 * lG + rmax * rmax + rmax;
  double *Msig = work + 2 * lG + rmax * rmax + 2 * rmax;

  // space to store right singular vectors of M
  double *V = work + 2 * lG + rmax * rmax + 3 * rmax;

  // space to store the product of a factor of M and the factor of a Gram matrix
  double *tempL = work + 2 * lG + 2 * rmax * rmax + 3 * rmax;
  double *tempR = tempL;

  // temporary space to store truncated core
  double *tempCore = work + 2 * lG + 3 * rmax * rmax + 3 * rmax;

  // Error in Frobenius norm
  double errNorm = 0;

  int rxn = rx[0] * n[0];
  int nrx = n[1] * rx[2];

  int lwork = -1;
  int maxlwork = 0;
  int one = 1;
  for(int i = 0; i < d - 1; i++){
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GL + Loffset, &rx[i + 1], Lsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GR + Roffset, &rx[i + 1], Rsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "A", &rx[i + 1], &rx[i + 1], M,            &rx[i + 1], Msig, NULL, &one,    V, &rx[i + 1], tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
  }
  double *workSVD = NULL;
  lwork = maxlwork;
  workSVD = (double*) malloc(lwork * sizeof(double));

  for(int i = 0; i < d - 1; i++){
    // factor the left Gram matrix, GL = Ul Sl^2 Ul^T
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GL + Loffset, &rx[i + 1], Lsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // factor the right Gram matrix, GR = Ur Sr^2 Ur^T
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GR + Roffset, &rx[i + 1], Rsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // compute M = Ul^T Ur 
    dgemm_("T", "N", &rx[i + 1], &rx[i + 1], &rx[i + 1], &done, GL + Loffset, &rx[i + 1], GR + Roffset, &rx[i + 1], &zero, M, &rx[i + 1]);
    // compute M = Sl M Sr
    for(int j = 0; j < rx[i + 1]; j++){
      for(int k = 0; k < rx[i + 1]; k++){
        M[k + j * rx[i + 1]] *= (sqrt(Lsig[k]) * sqrt(Rsig[j]));
      }
    }

    // factor M, M = Um Sm Vm^T
		dgesvd_("O", "A", &rx[i + 1], &rx[i + 1], M,            &rx[i + 1], Msig, NULL, &one,    V, &rx[i + 1], workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

    rankTT = rx[i + 1];
    // cumsum of the trailing singular values squared
    errNorm = 0;
    for(int k = rx[i + 1]; k > 0; k--){
      if(errNorm > tau){
        rankTT = k + 1;
        break;
      }
      errNorm += Msig[k - 1] * Msig[k - 1];
    }

    // Lsig^{-1} Um
    for(int j = 0; j < rankTT; j++){
      for(int k = 0; k < rx[i + 1]; k++){
        M[k + j * rx[i + 1]] = M[k + j * rx[i + 1]]/sqrt(Lsig[k]);
      }
    }
    // Msig Vm^T Rsig^{-1}
    for(int j = 0; j < rx[i + 1]; j++){
      for(int k = 0; k < rankTT; k++){
        V[k + j * rx[i + 1]] = V[k + j * rx[i + 1]] * (Msig[k]/sqrt(Rsig[j]));
      }
    }
    // GL{i} Lsig^{-1} Um
    dgemm_("N", "N", &rx[i + 1], &rankTT, &rx[i + 1], &done, GL + Loffset, &rx[i + 1], M, &rx[i + 1], &zero, tempL, &rx[i + 1]);
    // x{i} = x{i} GL{i} Lsig^{-1} Um
    rxn = rx[i] * n[i];
    dgemm_("N", "N", &rxn, &rankTT, &rx[i + 1], &done, x[i], &rxn, tempL, &rx[i + 1], &zero, tempCore, &rxn);
    memcpy(x[i], tempCore, rankTT * rxn * sizeof(double));

    // Msig Vm^T Rsig^{-1} GR{i}^T
    dgemm_("N", "T", &rankTT, &rx[i + 1], &rx[i + 1], &done, V, &rx[i + 1], GR + Roffset, &rx[i + 1], &zero, tempR, &rankTT);
    // x{i + 1} = Vm^T Rsig^{-1} GR{i}x{i + 1} 
    nrx = n[i + 1] * rx[i + 2];
    dgemm_("N", "N", &rankTT, &nrx, &rx[i + 1], &done, tempR, &rankTT, x[i + 1], &rx[i + 1], &zero, tempCore, &rankTT);
    memcpy(x[i + 1], tempCore, rankTT * nrx * sizeof(double));

    // update offset for next iteration
    Loffset += rx[i + 1] * rx[i + 1];
    Roffset = Roffset - rx[i + 2] * rx[i + 2];
    rx[i + 1] = rankTT;
  }

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
  free(workSVD);
	ATTAC_STOP_TIMER
}

void pattacGramSeqRLRCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  int rankTT = 0;

  double *GL = NULL, *GR = NULL;
  int lG = 0;
  for(int i = 0; i < d; i++){
    lG += rx[i] * rx[i];
  }
  
  double done = 1.0;
  double zero = 0.0;

  // product of Gram matrices
  double *M = work + lG + rmax * rmax;

  // Gram of left cores
  GL = work;

  // Gram of right cores
  GR = work + rmax * rmax;
  pattacGramRL(comm, isRedundant, d, n, rx, x, GR, M);
  
  // offset to point to the Gram of current core
  int Roffset = lG - rx[0] * rx[0] - rx[1] * rx[1];

  // thrshold for truncation
  double tau = tol * tol * GL[lG - 1]/((double) d - 1);

  // eigenvalues of Gram of left cores and right factors and singular values of M
  double *Lsig = work + lG + 2 * rmax * rmax;
  double *Rsig = work + lG + 2 * rmax * rmax + rmax;
  double *Msig = work + lG + 2 * rmax * rmax + 2 * rmax;

  // space to store right singular vectors of M
  double *V = work + lG + 2 * rmax * rmax + 3 * rmax;

  // space to store the product of a factor of M and the factor of a Gram matrix
  double *tempL = work + lG + 3 * rmax * rmax + 3 * rmax;
  double *tempR = tempL;

  // temporary space to store truncated core
  double *tempCore = work + lG + 4 * rmax * rmax + 3 * rmax;

  // Error in Frobenius norm
  double errNorm = 0;

  int rxn = rx[0] * n[0];
  int nrx = n[0] * rx[1];

  int lwork = -1;
  int maxlwork = 0;
  int one = 1;
  for(int i = 0; i < d - 1; i++){
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GL, &rx[i + 1], Lsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GR + Roffset, &rx[i + 1], Rsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "A", &rx[i + 1], &rx[i + 1], M,            &rx[i + 1], Msig, NULL, &one,    V, &rx[i + 1], tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
  }
  double *workSVD = NULL;
  lwork = maxlwork;
  workSVD = (double*) malloc(lwork * sizeof(double));

  for(int i = 0; i < d - 1; i++){
    // compute Gram of left cores
    rxn = rx[i] * n[i];
    nrx = n[i] * rx[i + 1];
    dgemm_("T", "N", &rx[i + 1], &rx[i + 1], &rxn, &done, x[i], &rxn, x[i], &rxn, &zero, GL, &rx[i + 1]);
#if TIME_BREAKDOWN
    if(isRedundant[0] == 0) MPI_Barrier(comm);
#endif

    /**< sum up along the mode */
    if(isRedundant[0] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, GL, rx[i + 1] * rx[i + 1], MPI_DOUBLE, MPI_SUM, comm);
    }
    if(ierr != 0){
      fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
      MPI_Abort(MPI_COMM_WORLD, ierr);
#else
      exit(1);
#endif
    }
    // factor the left Gram matrix, GL = Ul Sl^2 Ul^T
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GL, &rx[i + 1], Lsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // factor the right Gram matrix, GR = Ur Sr^2 Ur^T
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GR + Roffset, &rx[i + 1], Rsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // compute M = Ul^T Ur 
    dgemm_("T", "N", &rx[i + 1], &rx[i + 1], &rx[i + 1], &done, GL, &rx[i + 1], GR + Roffset, &rx[i + 1], &zero, M, &rx[i + 1]);
    // compute M = Sl M Sr
    for(int j = 0; j < rx[i + 1]; j++){
      for(int k = 0; k < rx[i + 1]; k++){
        M[k + j * rx[i + 1]] *= (sqrt(Lsig[k]) * sqrt(Rsig[j]));
      }
    }

    // factor M, M = Um Sm Vm^T
		dgesvd_("O", "A", &rx[i + 1], &rx[i + 1], M,            &rx[i + 1], Msig, NULL, &one,    V, &rx[i + 1], workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

    rankTT = rx[i + 1];
    // cumsum of the trailing singular values squared
    errNorm = 0;
    for(int k = rx[i + 1]; k > 0; k--){
      if(errNorm > tau){
        rankTT = k + 1;
        break;
      }
      errNorm += Msig[k - 1] * Msig[k - 1];
    }

    // Lsig^{-1} Um
    for(int j = 0; j < rankTT; j++){
      for(int k = 0; k < rx[i + 1]; k++){
        M[k + j * rx[i + 1]] = M[k + j * rx[i + 1]]/sqrt(Lsig[k]);
      }
    }
    // Msig Vm^T Rsig^{-1}
    for(int j = 0; j < rx[i + 1]; j++){
      for(int k = 0; k < rankTT; k++){
        V[k + j * rx[i + 1]] = V[k + j * rx[i + 1]] * (Msig[k]/sqrt(Rsig[j]));
      }
    }
    // GL{i} Lsig^{-1} Um
    dgemm_("N", "N", &rx[i + 1], &rankTT, &rx[i + 1], &done, GL, &rx[i + 1], M, &rx[i + 1], &zero, tempL, &rx[i + 1]);
    // x{i} = x{i} GL{i} Lsig^{-1} Um
    rxn = rx[i] * n[i];
    dgemm_("N", "N", &rxn, &rankTT, &rx[i + 1], &done, x[i], &rxn, tempL, &rx[i + 1], &zero, tempCore, &rxn);
    memcpy(x[i], tempCore, rankTT * rxn * sizeof(double));

    // Msig Vm^T Rsig^{-1} GR{i}^T
    dgemm_("N", "T", &rankTT, &rx[i + 1], &rx[i + 1], &done, V, &rx[i + 1], GR + Roffset, &rx[i + 1], &zero, tempR, &rankTT);
    // x{i + 1} = Vm^T Rsig^{-1} GR{i}x{i + 1} 
    nrx = n[i + 1] * rx[i + 2];
    dgemm_("N", "N", &rankTT, &nrx, &rx[i + 1], &done, tempR, &rankTT, x[i + 1], &rx[i + 1], &zero, tempCore, &rankTT);
    memcpy(x[i + 1], tempCore, rankTT * nrx * sizeof(double));

    // update offset for next iteration
    Roffset = Roffset - rx[i + 2] * rx[i + 2];
    rx[i + 1] = rankTT;
  }

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
  free(workSVD);
	ATTAC_STOP_TIMER
}

void pattacGramSeqLRLCompress(const MPI_Comm comm, const int* isRedundant, const int d, const int *n, int *rx, double **x, const double tol, double *work){
	ATTAC_START_TIMER
	int rank = 0, ierr = 0;
	MPI_Comm_rank(comm, &rank);
	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s begins\n", __func__);
	}
	int rmax = 0;
	for(int i = 0; i < d; i++){
		rmax = (rmax > rx[i]) ? rmax : rx[i];
	}
  int rankTT = 0;

  double *GL = NULL, *GR = NULL;
  int lG = 0;
  for(int i = 0; i < d; i++){
    lG += rx[i] * rx[i];
  }
  
  double done = 1.0;
  double zero = 0.0;

  // product of Gram matrices
  double *M = work + lG + rmax * rmax;

  // Gram of left cores
  GL = work;
  pattacGramLR(comm, isRedundant, d, n, rx, x, GL, M);

  // Gram of right cores
  GR = work + lG;
  
  // offset to point to the Gram of current core
  int Loffset = lG - 1 - rx[d - 1] * rx[d - 1];

  // thrshold for truncation
  double tau = tol * tol * GL[lG - 1]/((double) d - 1);

  // eigenvalues of Gram of left cores and right factors and singular values of M
  double *Lsig = work + lG + 2 * rmax * rmax;
  double *Rsig = work + lG + 2 * rmax * rmax + rmax;
  double *Msig = work + lG + 2 * rmax * rmax + 2 * rmax;

  // space to store right singular vectors of M
  double *V = work + lG + 2 * rmax * rmax + 3 * rmax;

  // space to store the product of a factor of M and the factor of a Gram matrix
  double *tempL = work + lG + 3 * rmax * rmax + 3 * rmax;
  double *tempR = tempL;

  // temporary space to store truncated core
  double *tempCore = work + lG + 4 * rmax * rmax + 3 * rmax;

  // Error in Frobenius norm
  double errNorm = 0;

  int rxn = rx[d - 1] * n[d - 1];
  int nrx = n[d - 1] * rx[d];
  int rxi = rx[d - 1];

  int lwork = -1;
  int maxlwork = 0;
  int one = 1;
  for(int i = 0; i < d - 1; i++){
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GL + Loffset, &rx[i + 1], Lsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "N", &rx[i + 1], &rx[i + 1], GR          , &rx[i + 1], Rsig, NULL, &one, NULL, &one,       tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
		dgesvd_("O", "A", &rx[i + 1], &rx[i + 1], M,            &rx[i + 1], Msig, NULL, &one,    V, &rx[i + 1], tempL, &lwork, &ierr);
    maxlwork = ((int) tempL[0] > maxlwork) ? (int) tempL[0] : maxlwork;
    lwork = -1;
  }
  double *workSVD = NULL;
  lwork = maxlwork;
  workSVD = (double*) malloc(lwork * sizeof(double));

  for(int i = d - 1; i > 0; i--){
    rxi = rx[i];
    nrx = n[i] * rx[i + 1];
    // compute the Gram matrix of core i
    dgemm_("N", "T", &rxi, &rxi, &nrx, &done, x[i], &rxi, x[i], &rxi, &zero, GR, &rxi);
#if TIME_BREAKDOWN
    if(isRedundant[d-1] == 0) MPI_Barrier(comm);
#endif
    /**< sum up along the mode */
    if(isRedundant[d-1] == 0){
	    ierr = MPI_Allreduce(MPI_IN_PLACE, GR, rxi * rxi, MPI_DOUBLE, MPI_SUM, comm);
    }
    if(ierr != 0){
      fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
      MPI_Abort(MPI_COMM_WORLD, ierr);
#else
      exit(1);
#endif
    }

    // factor the left Gram matrix, GL = Ul Sl^2 Ul^T
		dgesvd_("O", "N", &rx[i], &rx[i], GL + Loffset, &rx[i], Lsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // factor the right Gram matrix, GR = Ur Sr^2 Ur^T
		dgesvd_("O", "N", &rx[i], &rx[i], GR, &rx[i], Rsig, NULL, &one, NULL, &one, workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}
    // compute M = Ul^T Ur 
    dgemm_("T", "N", &rx[i], &rx[i], &rx[i], &done, GL + Loffset, &rx[i], GR, &rx[i], &zero, M, &rx[i]);
    // compute M = Sl M Sr
    for(int j = 0; j < rx[i]; j++){
      for(int k = 0; k < rx[i]; k++){
        M[k + j * rx[i]] *= (sqrt(Lsig[k]) * sqrt(Rsig[j]));
      }
    }

    // factor M, M = Um Sm Vm^T
		dgesvd_("O", "A", &rx[i], &rx[i], M,            &rx[i], Msig, NULL, &one,    V, &rx[i], workSVD, &lwork, &ierr);
		if(ierr != 0){
			fprintf(stderr, "%s:Line %d %s::LAPACKE_dgesvd:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
#ifdef ATTAC_MPI
			MPI_Abort(MPI_COMM_WORLD, ierr);
#else
			exit(1);
#endif
		}

    rankTT = rx[i];
    // cumsum of the trailing singular values squared
    errNorm = 0;
    for(int k = rx[i]; k > 0; k--){
      if(errNorm > tau){
        rankTT = k + 1;
        break;
      }
      errNorm += Msig[k - 1] * Msig[k - 1];
    }

    // Lsig^{-1} Um Msig
    for(int j = 0; j < rankTT; j++){
      for(int k = 0; k < rx[i]; k++){
        M[k + j * rx[i]] = M[k + j * rx[i]] * (Msig[j]/sqrt(Lsig[k]));
      }
    }
    // Vm^T Rsig^{-1}
    for(int j = 0; j < rx[i]; j++){
      for(int k = 0; k < rankTT; k++){
        V[k + j * rx[i]] = V[k + j * rx[i]] * (1./sqrt(Rsig[j]));
      }
    }
    // GL{i} Lsig^{-1} Um Msig
    dgemm_("N", "N", &rx[i], &rankTT, &rx[i], &done, GL + Loffset, &rx[i], M, &rx[i], &zero, tempL, &rx[i]);
    // x{i - 1} = x{i - 1} GL{i} Lsig^{-1} Um Msig
    rxn = rx[i - 1] * n[i - 1];
    dgemm_("N", "N", &rxn, &rankTT, &rx[i], &done, x[i - 1], &rxn, tempL, &rx[i], &zero, tempCore, &rxn);
    memcpy(x[i - 1], tempCore, rankTT * rxn * sizeof(double));

    // Vm^T Rsig^{-1} GR{i}^T
    dgemm_("N", "T", &rankTT, &rx[i], &rx[i], &done, V, &rx[i], GR, &rx[i], &zero, tempR, &rankTT);
    // x{i + 1} = Vm^T Rsig^{-1} GR{i}x{i + 1} 
    nrx = n[i] * rx[i + 1];
    dgemm_("N", "N", &rankTT, &nrx, &rx[i], &done, tempR, &rankTT, x[i], &rx[i], &zero, tempCore, &rankTT);
    memcpy(x[i], tempCore, rankTT * nrx * sizeof(double));

    // update offset for next iteration
    Loffset = Loffset - rx[i - 1] * rx[i - 1];
    rx[i] = rankTT;
  }

	if(ATTAC_VERBOSITY > 10 && !rank){
		printf("%s ends\n", __func__);
	}
  free(workSVD);
	ATTAC_STOP_TIMER
}
