/** \file pattac_Comparison.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 14/06/2020
 * \details
*/
#include <string.h>
#include <math.h>
#include <mkl.h>
#include "timer.h"
#include "utilities.h"
#include "pattac.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);
	/** Timer set up*/
	char *filename;
  filename = (char*) malloc(100 * sizeof(char));
	attacInitTimer();
	ATTAC_START_TIMER

  int ierr = 0;
	int size = 0, rank = 0;
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

  if(!rank){
    printf("**************************\n");
    printf("* Number of processors: %d\n", size);
    printf("**************************\n");
  }

	/**< Binary Tree Depth */

	int d = 10;
	int *n = NULL;
	int *r = NULL;
	int *isRedundant = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	isRedundant = (int*) malloc(d * sizeof(int));
	if(isRedundant == NULL){
		fprintf(stderr, "isRedundant was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = 100;
	isRedundant[0] = 0;
	for(int i = 1; i < d; ++i){
		r[i] =  5 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = n[0] + 10 * i;// + 100 * i;
    isRedundant[i] = 0;
	}
	isRedundant[d-1] = 0;
  r[d/2] = 50;

	if(!rank){
    printf("**************************\n");
    printf("* Tensor Order: %d\n", d);
    printf("* Tensor Mode Sizes: \n");
    for(int i = 0; i < d - 1; i++){
      printf("n[%d] = %d, ", i, n[i]);
    }
		printf("n[%d] = %d\n", d - 1, n[d - 1]);
    printf("* Modes Redundancy:  \n");
    for(int i = 0; i < d - 1; i++){
      printf("%d, ", isRedundant[i]);
    }
    printf("%d\n", isRedundant[d - 1]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);

	int nmax = 0;
	int rmax = 0;
	int Srr = 0;
	for(int i = 0; i < d; i++){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}
	for(int i = 1; i < d; i++){
		Srr += 4 * r[i] * r[i];
	}
	Srr += 4 * rmax * rmax;

	for(int i = 0; i < d; i++){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	srand(rank);
	
	for(int i = 0; i < d; i++){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
    MPI_Barrier(MPI_COMM_WORLD);
    if(isRedundant[i] == 1){
      ierr = 0;
      ierr = MPI_Bcast(x[i], r[i] * n[i] * r[i + 1], MPI_DOUBLE, 0, MPI_COMM_WORLD);
		  if(ierr != 0){
		  	fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
		  	MPI_Abort(MPI_COMM_WORLD, ierr);
		  }
    }
	}
	for(int i = 0; i < d; i++){
		double normxi = 0;
	  normxi = LAPACKE_dlange(LAPACK_COL_MAJOR, 'F', r[i] * n[i], r[i + 1], x[i], r[i] * n[i]);
    if(isRedundant[i] == 0){
	  	normxi *= normxi;
	    MPI_Allreduce(MPI_IN_PLACE, &normxi, 1, MPI_DOUBLE, MPI_SUM, comm);
	  	normxi = sqrt(normxi);
    }
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}

	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (4 * n[i] * r[i] * r[i + 1] > maxnrr) ? 4 * n[i] * r[i] * r[i + 1] : maxnrr;
  }

  int CLog2P = (int) ceil(log2((double)size));
	int lworkd =   Srr * (CLog2P + 1)                     // lv
               + Srr * (CLog2P + 1)                     // lt
               + (2 * (2 * rmax) + 1) * (2 * rmax)      // work
               + (2 * rmax) * (2 * rmax)                // R
               + 2 * (2 * rmax)                         // Singular values
               + maxnrr;                                // work for Orthogonalization/compression/NormHadamardOpt
	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *indices = NULL, *rankIndices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rankIndices = (int*) malloc(d * sizeof(double));
	if(rankIndices == NULL){
		fprintf(stderr, "rankIndices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	for( int i = 0; i < d; i++){
		indices[i] = i % n[i];
		rankIndices[i] = i % size;
	}
	double val = 0; 
	double val_L, val_R, val_LI, val_RI, val_BFL, val_BFR, val_BFLI, val_BFRI, val_BFLO, val_BFRO;
	/**
	 * Runtime: Left to Right compression 
	 */

	/**< Normalizing the tensor */
	double norm = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, r, x, workd);
	pattacScale(1./sqrt(norm), 0, d, n, r, x);

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; i++){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; i++){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	double **y = NULL;
	y = (double**) malloc(d * sizeof(double*));
	if(y == NULL){
		fprintf(stderr, "y was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *ry = NULL;
	ry = (int*) malloc((d + 1) * sizeof(int));
	if(ry == NULL){
		fprintf(stderr, "ry was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	ry[0] = 1;
	ry[d] = 1;
	for(int i = 1; i < d; i++){
		ry[i] = 2 * r[i];
	}
	for(int i = 0; i < d; i++){
		y[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(y[i] == NULL){
			fprintf(stderr, "y[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	double alpha = 2.0;
	double beta = -1.0;

	double timing_L, timing_R, timing_LI, timing_RI, timing_BFL, timing_BFR, timing_BFLI, timing_BFRI, timing_BFLO, timing_BFRO;
  double tol = 1e-5;
	double threshold = tol/sqrt((double)(d - 1));

	val = pattacValue(comm, indices, rankIndices, d, n, r, x, workd);
	attacResetTimer();
	MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(1, "LRL")
	MPI_Barrier(comm);
	timing_R = MPI_Wtime();
  pattacCompress(comm, isRedundant, d, n, rxpx, xpx, threshold, 'R', workd);
	timing_R = (MPI_Wtime() - timing_R);
	MPI_Barrier(comm);
  ATTAC_TAC(1)

	if(!rank) printf("Ranks of LRL compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_R = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacCompress_R", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  double normx = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, r, x, workd);
  double normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
  if(!rank){
    printf("pattacCompressR ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Left to right Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(2, "RLR")
	MPI_Barrier(comm);
	timing_L = MPI_Wtime();
  pattacCompress(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', workd);
	timing_L = (MPI_Wtime() - timing_L);
	MPI_Barrier(comm);
  ATTAC_TAC(2)

	if(!rank) printf("Ranks of RLR compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_L = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacCompress_L", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacCompressL ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(3, "LRLI")
	MPI_Barrier(comm);
	timing_RI = MPI_Wtime();
  pattacCompressImplicitQs(comm, isRedundant, d, n, rxpx, xpx, threshold, 'R', workd);
	timing_RI = (MPI_Wtime() - timing_RI);
	MPI_Barrier(comm);
  ATTAC_TAC(3)
	
  if(!rank) printf("Ranks of LRLI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_RI = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacCompressImplicit_R", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacCompressImplicitR ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Left to Right Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	
  ATTAC_TIC(4, "RLRI")
	MPI_Barrier(comm);
	timing_LI = MPI_Wtime();
  pattacCompressImplicitQs(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', workd);
	timing_LI = (MPI_Wtime() - timing_LI);
	MPI_Barrier(comm);
  ATTAC_TAC(4)

	if(!rank) printf("Ranks of RLRI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_LI = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacCompressImplicit_L", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacCompressImplicitL ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(5, "BFLRL")
	MPI_Barrier(comm);
	timing_BFR = MPI_Wtime();
  pattacBFCompress(comm, isRedundant, d, n, rxpx, xpx, threshold, 'R', workd);
	timing_BFR = (MPI_Wtime() - timing_BFR);
	MPI_Barrier(comm);
  ATTAC_TAC(5)

	if(!rank) printf("Ranks of BFLRL compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

	val_BFR = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompress_R", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacBFCompressImplicitR ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Left to right Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(6, "BFRLR")
	MPI_Barrier(comm);
	timing_BFL = MPI_Wtime();
  pattacBFCompress(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', workd);
	timing_BFL = (MPI_Wtime() - timing_BFL);
	MPI_Barrier(comm);
  ATTAC_TAC(6)

	if(!rank) printf("Ranks of BFRLR compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_BFL = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompress_L", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacBFCompressL ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
  
  ATTAC_TIC(7, "BFLRLI")
	MPI_Barrier(comm);
	timing_BFRI = MPI_Wtime();
  pattacBFCompressImplicitQs(comm, isRedundant, d, n, rxpx, xpx, threshold, 'R', workd);
	timing_BFRI = (MPI_Wtime() - timing_BFRI);
	MPI_Barrier(comm);
  ATTAC_TAC(7)

	if(!rank) printf("Ranks of BFLRLI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_BFRI = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompressImplicit_R", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacBFCompressImplicitR ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Left to right Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
  
  ATTAC_TIC(8, "BFRLRI")
	MPI_Barrier(comm);
	timing_BFLI = MPI_Wtime();
  pattacBFCompressImplicitQs(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', workd);
	timing_BFLI = (MPI_Wtime() - timing_BFLI);
	MPI_Barrier(comm);
  ATTAC_TAC(8)

	if(!rank) printf("Ranks of BFRLRI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_BFLI = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompressImplicit_L", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, rxpx, xpx, ry, y);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("pattacBFCompressImplicitL ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(9, "BFLRLO")
	MPI_Barrier(comm);
	timing_BFRO = MPI_Wtime();
  pattacBFCompressOutplace(comm, isRedundant, d, n, rxpx, xpx, threshold, 'R', ry, y, workd);
	timing_BFRO = (MPI_Wtime() - timing_BFRO);
	MPI_Barrier(comm);
  ATTAC_TAC(9)

	if(!rank) printf("Ranks of BFLRLO compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_BFRO = pattacValue(comm, indices, rankIndices, d, n, ry, y, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompressOutplace_R", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, ry, y, rxpx, xpx);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, rxpx, xpx, workd);
  if(!rank){
    printf("pattacBFCompressOutplaceR ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Left to right Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);

  ATTAC_TIC(10, "BFRLRO")
	MPI_Barrier(comm);
	timing_BFLO = MPI_Wtime();
  pattacBFCompressOutplace(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', ry, y, workd);
	timing_BFLO = (MPI_Wtime() - timing_BFLO);
	MPI_Barrier(comm);
  ATTAC_TAC(10)

	if(!rank) printf("Ranks of BFRLRO compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val_BFLO = pattacValue(comm, indices, rankIndices, d, n, ry, y, workd);
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompressOutplace_L", size);
	printTSQRTimer(filename, comm);
  for(int i = 0; i < size; i++){
	  if(rank == i) attacPrintTimer(filename);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  // test norm of xpx - x
	pattacFormalAXPBY(d, n, 1., r, x, -1, ry, y, rxpx, xpx);
  normDiff = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, rxpx, xpx, workd);
  if(!rank){
    printf("pattacBFCompressOutplaceL ||x - round(2x-x)||_F^2/||x||_F^2 = %e/%e = %e\n", normDiff, normx, normDiff/normx); 
  }
  if(fabs(normDiff) > tol * fabs(normx)){
    ierr = 1;
  }
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);


	if(!rank){
		printf("/*************************/\n");
		printf("*     Time comparison     *\n");
		printf("/*************************/\n");
		printf("pattacCompress LRL:     %f\n", timing_R);
		printf("pattacCompress RLR:     %f\n", timing_L);
		printf("pattacCompress LRLI:    %f\n", timing_RI);
		printf("pattacCompress RLRI:    %f\n", timing_LI);
		printf("pattacCompress BFLRL:   %f\n", timing_BFR);
		printf("pattacCompress BFRLR:   %f\n", timing_BFL);
		printf("pattacCompress BFLRLI:  %f\n", timing_BFRI);
		printf("pattacCompress BFRLRI:  %f\n", timing_BFLI);
		printf("pattacCompress BFLRLO:  %f\n", timing_BFRO);
		printf("pattacCompress BFRLRO:  %f\n", timing_BFLO);
		printf("/*************************/\n");
		printf("/*************************/\n");
		printf("/*************************/\n");
		printf("*     Time comparison     *\n");
		printf("/*************************/\n");
		printf("val_LRL:     %e\n", val_R);
		printf("val_RLR:     %e\n", val_L);
		printf("val_LRLI:    %e\n", val_RI);
		printf("val_RLRI:    %e\n", val_LI);
		printf("val_BFLRL:   %e\n", val_BFR);
		printf("val_BFRLR:   %e\n", val_BFL);
		printf("val_BFLRLI:  %e\n", val_BFRI);
		printf("val_BFRLRI:  %e\n", val_BFLI);
		printf("val_BFLRLO:  %e\n", val_BFRO);
		printf("val_BFRLRO:  %e\n", val_BFLO);
		printf("/*************************/\n");
		printf("/*************************/\n");
	}
	for(int i = 0; i < d; i++){
		free(x[i]);
		free(xpx[i]);
		free(y[i]);
	}
	free(y);
	free(xpx);
	free(rxpx);
	free(x);
	free(workd);
	free(n);
	free(r);
	free(isRedundant);
  ATTAC_STOP_TIMER
	free(filename);
	attacFinalizeTimer();
	MPI_Finalize();
  return ierr;
}

