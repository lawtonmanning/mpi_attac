/** \file pattacGram.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 27/03/2021
 * \details
*/
#include <string.h>
#include <math.h>
#include "timer.h"
#include "utilities.h"
#include "pattac.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);
	/** Timer set up*/
	char *filename;
  filename = (char*) malloc(100 * sizeof(char));
	attacInitTimer();
	ATTAC_START_TIMER

  int ierr = 0;
	int size = 0, rank = 0;
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

  if(!rank){
    printf("**************************\n");
    printf("* Number of processors: %d\n", size);
    printf("**************************\n");
  }

	int d = 10;
	int *n = NULL;
	int *r = NULL;
	int *isRedundant = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	isRedundant = (int*) malloc(d * sizeof(int));
	if(isRedundant == NULL){
		fprintf(stderr, "isRedundant was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = 1000;
	isRedundant[0] = 0;
	for(int i = 1; i < d; ++i){
		r[i] =  2 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = n[0] + 10 * i;
    isRedundant[i] = 0;
	}
  r[d/2] = 50;

	if(!rank){
    printf("**************************\n");
    printf("* Tensor Order: %d\n", d);
    printf("* Tensor Mode Sizes: \n");
    for(int i = 0; i < d - 1; ++i){
      printf("n[%d] = %d, ", i, n[i]);
    }
		printf("n[%d] = %d\n", d - 1, n[d - 1]);
    printf("* Modes Redundancy:  \n");
    for(int i = 0; i < d - 1; ++i){
      printf("%d, ", isRedundant[i]);
    }
    printf("%d\n", isRedundant[d - 1]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	srand(rank);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
    if(isRedundant[i] == 1){
      ierr = 0;
      ierr = MPI_Bcast(x[i], r[i] * n[i] * r[i + 1], MPI_DOUBLE, 0, MPI_COMM_WORLD);
		  if(ierr != 0){
		  	fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
		  	MPI_Abort(MPI_COMM_WORLD, ierr);
		  }
    }
	}
	for(int i = 0; i < d; ++i){
		double normxi = 0;
    int rn = r[i] * n[i];
	  normxi = dlange_("F", &rn, &r[i + 1], x[i], &rn, NULL);
    if(isRedundant[i] == 0){
		  normxi *= normxi;
	    MPI_Allreduce(MPI_IN_PLACE, &normxi, 1, MPI_DOUBLE, MPI_SUM, comm);
		  normxi = sqrt(normxi);
    }
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}


	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (n[i] * r[i] * r[i + 1] > maxnrr) ? n[i] * r[i] * r[i + 1] : maxnrr;
  }

	int lworkd =   rmax * rmax + maxnrr; // work space for Norm/GramLR/GramRL
	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *indices = NULL, *rankIndices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rankIndices = (int*) malloc(d * sizeof(double));
	if(rankIndices == NULL){
		fprintf(stderr, "rankIndices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	for( int i = 0; i < d; ++i){
		indices[i] = i % n[i];
		rankIndices[i] = i % size;
	}

	double begin = 0, timingGL = 0, timingGR = 0;
  int nrun = 10;

  double normInnerProd = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, r, x, workd);
  if(!rank){
    printf("pattacNormSquaredHadamardOpt = %f\n", sqrt(normInnerProd));
  }
  int lGL = 0;
  for(int i = 1; i < d + 1; i++){
    lGL += r[i] * r[i];
  }
  double *GL = NULL;
  GL = (double*) malloc(lGL * sizeof(double));
	if(GL == NULL){
		fprintf(stderr, "GL was not well allocated\n");
		MPI_Abort(comm, 100);
	}
  for(int i = 0; i < nrun; i++){
    MPI_Barrier(comm);
    begin = MPI_Wtime();
    pattacGramLR(comm, isRedundant, d, n, r, x, GL, workd);
    timingGL += MPI_Wtime() - begin;
  }

  double *GR = NULL;
  GR = (double*) malloc(lGL * sizeof(double));
	if(GR == NULL){
		fprintf(stderr, "GR was not well allocated\n");
		MPI_Abort(comm, 100);
	}
  for(int i = 0; i < nrun; i++){
    MPI_Barrier(comm);
    begin = MPI_Wtime();
    pattacGramRL(comm, isRedundant, d, n, r, x, GR, workd);
    timingGR += MPI_Wtime() - begin;
  }
  if(!rank){
    printf("Norm of x squared GL = %.16f\n", GR[lGL - 1]);
    printf("Norm of x squared GR = %.16f\n", GL[lGL - 1]);
    printf("Norm of x squared IP = %.16f\n", normInnerProd);
  }
  if(fabs(GR[lGL - 1] - GL[lGL - 1]) > 1e-8 * fabs(GL[lGL - 1])){
    ierr = 1;
    if(!rank){
      fprintf(stderr, "Squared norms given by GramLR and GramRL do not match\n");
    }
  }else{
    ierr = 0;
  }
  
	if(!rank){
		printf("/*************************/\n");
		printf("*     Time comparison     *\n");
		printf("/*************************/\n");
		printf("pattacGramLR: %f\n", timingGL);
		printf("pattacGramRL: %f\n", timingGR);
		printf("/*************************/\n");
		printf("/*************************/\n");
	}
	for(int i = 0; i < d; ++i){
		free(x[i]);
	}
	free(GL);
	free(GR);
	free(x);
	free(workd);
	free(n);
	free(r);
  ATTAC_STOP_TIMER
	attacFinalizeTimer();
	free(filename);
	MPI_Finalize();
  return ierr;
}
