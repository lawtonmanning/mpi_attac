/** \file attacBFTSQR.c
 * \author Hussam Al Daas
 * \brief TSQR test
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <mkl.h>
#include <math.h>
#include "timer.h"
#include "utilities.h"
#include "putilities.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);
	attacInitTimer();
	ATTAC_START_TIMER

	int ierr = 0, size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Status status;

	char layout = 'R';
	int LapackLayout = (layout == 'C') ? LAPACK_COL_MAJOR : LAPACK_ROW_MAJOR;

	int m = 10, n = 6;
	int nb = n;
	int l = n;
	int ldloca = (layout == 'C') ? m : n;

	double *a = NULL, *loca = NULL, *c = NULL, *copy_loca = NULL, *copy_c = NULL;

	loca = (double*) malloc(m * n * sizeof(double));
	copy_loca = (double*) malloc(m * n * sizeof(double));
	c = (double*) malloc(m * n * sizeof(double));
	a = (double*) malloc(size * m * n * sizeof(double));
  copy_c = (double*) malloc(n * n * sizeof(double));
	if(a == NULL || copy_loca == NULL || c == NULL || a == NULL || copy_c == NULL){
		fprintf(stderr, "Memory allocation did not take place as expected\n");
		MPI_Abort(MPI_COMM_WORLD, -999);
	}
	
	// Send local matrices
	srand(1);
	for(int i = 0; i < size * m; ++i){
		for(int j = 0; j < n; ++j){
			if(layout == 'C'){
				a[i + j * size * m] = ((double)rand())/RAND_MAX;
			}else{
				a[i * n + j] = ((double)rand())/RAND_MAX;
			}
		}
	}
	// Send Global A
	if(!rank){
		for(int i = 1; i < size; ++i){
			ierr = MPI_Send(a, m * size * n, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
		}
	}else{
		ierr = MPI_Recv(a, m * n * size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
	}

	double* tempa = (layout == 'C') ? a + m * rank : a + m * n * rank;
	int lda = (layout == 'C') ? m * size : n;
	ierr = LAPACKE_dlacpy(LapackLayout, 'A', m, n, tempa, lda, loca, ldloca);
	ierr = LAPACKE_dlacpy(LapackLayout, 'A', m, n, tempa, lda, copy_loca, ldloca);

	// Compute QR of distributed A (TSQR)
	double *work = NULL;
  int CLog2P = (int) ceil(log2((double)size));
  int FLog2P = (int) floor(log2((double)size));

	int lv = (CLog2P + 1) * n * n;  // v
	int lt = (CLog2P + 1) * n * nb; // t
	int lr = n * n;                 // R
	int lrtsqr = n * n;             // RTSQR
	int offset = 0;

	int lwork = (n * (2 * n + 1))  // work
						+ lv                 // v
					  + lt                 // t
						+ lr                 // R
						+ lrtsqr             // RTSQR
						;

	work = (double*) malloc(lwork * sizeof(double));
	if(work == NULL){
		fprintf(stderr, "Memory allocation for work place did not take place as expected\n");
		MPI_Abort(MPI_COMM_WORLD, -999);
	}
	offset += (n * (2 * n + 1));

	double* R;
	R = work + offset;
	offset += lr;

	double* RTSQR = work + offset;
	offset += lrtsqr;

	double *v = work + offset;
	offset += lv;

	double *t = work + offset;
	offset += lt;

	memset(v, 0, lv * sizeof(double));
	memset(t, 0, lt * sizeof(double));
  double *TestR = NULL, *copyR = NULL;
  TestR = (double*) malloc(n * n * sizeof(double));
  copyR = (double*) malloc(n * n * sizeof(double));

	attacBFTSQR(MPI_COMM_WORLD, layout, m, n, nb, loca, ldloca, RTSQR, v, t, work);

	if(!rank){
		setLowerZero(layout, n, n, RTSQR, n);
	}
	ierr = MPI_Bcast(RTSQR, n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	// Compute Q factor TSQR
	memset(c, 0, m * n * sizeof(double));
	
	MPI_Barrier(MPI_COMM_WORLD);

	for(int i = 0; i < n; ++i){
		c[i + i * ldloca] = 1.;
	}

  attacBFTSQRApplyQ(MPI_COMM_WORLD, layout, m, &n, n, nb, loca, ldloca, v, t, c, ldloca, work);
	////////////////////////

	// Compute Q \times R - A

	CBLAS_LAYOUT cblaslayout = (layout == 'C') ? CblasColMajor : CblasRowMajor;
	cblas_dgemm (cblaslayout, CblasNoTrans, CblasNoTrans, m, n, n, 1., c, ldloca, RTSQR, n, -1., copy_loca, ldloca);

	double maxa = -1.;
	double maxdif = -1.;
	double dif = 0;
	for(int j = 0; j < n * m * size; ++j){
		maxa = (maxa > fabs(a[j])) ? maxa : fabs(a[j]);
	}

	for(int j = 0; j < n; ++j){
		for(int i = 0; i < m; ++i){
			dif = (layout == 'C') ? fabs(copy_loca[i + j * m]) : fabs(copy_loca[i * n + j]);
			maxdif = (maxdif > dif) ? maxdif : dif;
		}
	}
	for(int i = 0; i < size; ++i){
	  if(rank == i && maxdif > 1e-14){
		  printf("At proc %d, Max(|loc(Q) * R - loc(A)|) = %e\n", i, maxdif);
    }
    MPI_Barrier(MPI_COMM_WORLD);
	}
	MPI_Allreduce(MPI_IN_PLACE, &maxdif, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("\n**************************************************\n");
	}
	if(!rank){
		printf("Max(|Q * R - A|) = %e\n", maxdif);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	for(int i = 0; i < size; ++i){
		if(maxdif > 1e-12 * maxa && rank == i){
			printf("\n**************************************************\n");
			printf("\t Matrix rank %d\n", i);
			printf("max_{i,j} || A || = %.14f\n", maxa);
			printf("max_{i,j} || Q R - A || = %.14f\n", maxdif);
			printf("\n**************************************************\n");
			printf("a\n");
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

	if(!rank){
		printf("\n**************************************************\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);

	// Compute Q^T Q - I
	cblas_dgemm (cblaslayout, CblasTrans, CblasNoTrans, n, n, m, 1., c, ldloca, c, ldloca, 0.0, copy_loca, n);
	MPI_Allreduce(MPI_IN_PLACE, copy_loca, n * n, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	for(int k = 0; k < n; ++k){
		copy_loca[k + n * k] -= 1.;
	}
	double maxQtQMinusI = 0;
	for(int k = 0; k < n * n; ++k){
		maxQtQMinusI = (fabs(copy_loca[k]) > maxQtQMinusI) ? fabs(copy_loca[k]) : maxQtQMinusI;
	}
	MPI_Allreduce(MPI_IN_PLACE, &maxQtQMinusI, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("Max(| Q^T Q - I | = %e\n", maxQtQMinusI);
	}
	if(!rank){
		printf("\n**************************************************\n");
	}

  /////////////////////////////////////////
  ///     compute Q_GEQRF - Q_TSQR      ///
  ///    compute Q_GEQRF c - Q_TSQR c   ///
  /////////////////////////////////////////
  // GEQRF ORGQR
  if(layout == 'C'){
    ierr = LAPACKE_dgeqrf(LAPACK_COL_MAJOR, m * size, n, a, m * size, work);
	  if(ierr != 0){
	  	fprintf(stderr, "%s:Line %d %s::LAPACKE_dgeqrf:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
	  	MPI_Abort(MPI_COMM_WORLD, ierr);
	  }
    for(int i = 0; i < n; i++){
      work[n + i] = a[i + m * size * i] * RTSQR[i + n * i];
    }
    ierr = LAPACKE_dorgqr(LAPACK_COL_MAJOR, m * size, n, n, a, m * size, work);
	  if(ierr != 0){
	  	fprintf(stderr, "%s:Line %d %s::LAPACKE_dorgqr:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
	  	MPI_Abort(MPI_COMM_WORLD, ierr);
	  }
    for(int j = 0; j < n; j++){
      if(work[n + j] < 0){
        for(int i = 0; i < m * size; i++){
          a[i + m * size * j] *= -1.;
        }
      }
    }
  }else{
    ierr = LAPACKE_dgeqrf(LAPACK_ROW_MAJOR, m * size, n, a, n, work);
	  if(ierr != 0){
	  	fprintf(stderr, "%s:Line %d %s::LAPACKE_dgelqf:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
	  	MPI_Abort(MPI_COMM_WORLD, ierr);
	  }
    for(int i = 0; i < n; i++){
      work[n + i] = a[i + n * i] * RTSQR[i + n * i];
    }
	  if(!rank){
	    ierr = LAPACKE_dlacpy(LapackLayout, 'A', n, n, a, n, copy_c, n);
		  setLowerZero(layout, n, n, copy_c, n);
      //printMatrix(layout, n, n, copy_c, n);
	  }
    ierr = LAPACKE_dorgqr(LAPACK_ROW_MAJOR, m * size, n, n, a, n, work);
	  if(ierr != 0){
	  	fprintf(stderr, "%s:Line %d %s::LAPACKE_dorglq:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
	  	MPI_Abort(MPI_COMM_WORLD, ierr);
	  }
    // make signs the same
    for(int j = 0; j < n; j++){
      if(work[n + j] < 0){
        for(int i = 0; i < m * size; i++){
          a[i * n + j] *= -1.;
        }
      }
    }
  }
  ///////////////////////////////////////
  ///////////////////////////////////////
  ///////   c contains the Q_TSQR  //////
  ///////////////////////////////////////
  // compute Q_TSQR - Q_GEQRF       /////
  ///////////////////////////////////////
  maxdif = 0;
  for(int i = 0; i < m; i++){
    for(int j = 0; j < n; j++){
			dif = (layout == 'C') ? fabs(c[i + j * m] - a[m * rank + i + j * m * size]) : fabs(c[i * n + j] - a[m * n * rank + i * n + j]);
			maxdif = (maxdif > dif) ? maxdif : dif;
    }
  }
  int ldc = (layout == 'C') ? m : n;
  for(int i = 0; i < size; i++){
    if(rank == i){
      if(maxdif > 1e-14){
        printf("p %d: local || Q_TSQR - Q_GEQRF || = %e \n", i, maxdif);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
	MPI_Allreduce(MPI_IN_PLACE, &maxdif, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("Max(|| Q_TSQR - Q_GEQRF ||) = %e\n", maxdif);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	if(!rank){
		printf("\n**************************************************\n");
	}
  
  //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////
  l = n-2;
  memset(c, 0, m * l * sizeof(double));
  // put random matrix in c
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < l; ++j){
			if(layout == 'C'){
				c[i + j * m] = ((double)rand())/RAND_MAX;
			}else{
				c[i * l + j] = ((double)rand())/RAND_MAX;
			}
		}
	}

  ldc = (layout == 'C') ? m : l;
  memset(copy_c, 0, n * n * sizeof(double));

  // copy the nonzero block of c into copy_c
  // We will assess Q_GEQ copy_c = Q_TSQR c
	for(int i = 0; i < n; ++i){
		for(int j = 0; j < l; ++j){
			if(layout == 'C'){
				copy_c[i + j * n] = c[i + j * m];
			}else{
				copy_c[i * l + j] = c[i * l + j];
			}
		}
	}
  MPI_Bcast(copy_c, n * l, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  // On procs not in the butterfly set 0
  // to assess general applicability
  if(CLog2P != FLog2P){
    if(rank >= ((int)pow(2., (double)FLog2P))){
      l = n;
      if(layout == 'R') ldc = n;
      memset(c, 0, n * m * sizeof(double));
    }
  }
  
  // c = Q_TSQR c
  attacBFTSQRApplyQ(MPI_COMM_WORLD, layout, m, &l, n, nb, loca, ldloca, v, t, c, ldc, work);
  if(layout == 'R'){
    ldc = l;
  }

  // c - Q_GEQ c_copy
  if(layout == 'C'){
	  cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, l, n, 1., a + m * rank, m * size, copy_c, n, -1., c, m);
  }else{
	  cblas_dgemm (CblasRowMajor, CblasNoTrans, CblasNoTrans, m, l, n, 1., a + m * n * rank, n, copy_c, l, -1., c, l);
  }
  
  maxdif = 0;
	for(int j = 0; j < l; ++j){
		for(int i = 0; i < m; ++i){
			dif = (layout == 'C') ? fabs(c[i + j * m]) : fabs(c[i * l + j]);
			maxdif = (maxdif > dif) ? maxdif : dif;
		}
	}
  for(int i = 0; i < size; i++){
    if(rank == i && maxdif > 1e-14){
      printf("p %d: local || Q c - TSQR_Q c || = %e \n", i, maxdif);
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
	MPI_Allreduce(MPI_IN_PLACE, &maxdif, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("Max(||Q c - TSQR_Q c||) = %e\n", maxdif);
	}
	if(!rank){
		printf("\n**************************************************\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);
	free(c);
	free(a);
	free(loca);
	free(copy_loca);
	free(copy_c);
	free(work);
  ATTAC_STOP_TIMER
	attacPrintTimer("Timing.txt");
	attacFinalizeTimer();
	MPI_Finalize();
  return 0;
}
