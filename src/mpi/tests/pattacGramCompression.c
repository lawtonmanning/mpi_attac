/** \file pattacGramCompression.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 27/03/2021
 * \details
*/
#include <string.h>
#include <math.h>
#include "timer.h"
#include "utilities.h"
#include "pattac.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);
	/** Timer set up*/
	char *filename;
  filename = (char*) malloc(100 * sizeof(char));
	attacInitTimer();
	ATTAC_START_TIMER

  int ierr = 0;
	int size = 0, rank = 0;
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

  if(!rank){
    printf("**************************\n");
    printf("* Number of processors: %d\n", size);
    printf("**************************\n");
  }

	int d = 10;
	int *n = NULL;
	int *r = NULL;
	int *isRedundant = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	isRedundant = (int*) malloc(d * sizeof(int));
	if(isRedundant == NULL){
		fprintf(stderr, "isRedundant was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = 1000;
	isRedundant[0] = 0;
	for(int i = 1; i < d; ++i){
		r[i] =  5 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = n[0] + 10 * i;
    isRedundant[i] = 0;
	}
  r[d/2] = 50;

	if(!rank){
    printf("**************************\n");
    printf("* Tensor Order: %d\n", d);
    printf("* Tensor Mode Sizes: \n");
    for(int i = 0; i < d - 1; ++i){
      printf("n[%d] = %d, ", i, n[i]);
    }
		printf("n[%d] = %d\n", d - 1, n[d - 1]);
    printf("* Modes Redundancy:  \n");
    for(int i = 0; i < d - 1; ++i){
      printf("%d, ", isRedundant[i]);
    }
    printf("%d\n", isRedundant[d - 1]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	srand(rank);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
    if(isRedundant[i] == 1){
      ierr = 0;
      ierr = MPI_Bcast(x[i], r[i] * n[i] * r[i + 1], MPI_DOUBLE, 0, MPI_COMM_WORLD);
		  if(ierr != 0){
		  	fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
		  	MPI_Abort(MPI_COMM_WORLD, ierr);
		  }
    }
	}
	for(int i = 0; i < d; ++i){
		double normxi = 0;
    int rn = r[i] * n[i];
	  normxi = dlange_("F", &rn, &r[i + 1], x[i], &rn, NULL);
    if(isRedundant[i] == 0){
		  normxi *= normxi;
	    MPI_Allreduce(MPI_IN_PLACE, &normxi, 1, MPI_DOUBLE, MPI_SUM, comm);
		  normxi = sqrt(normxi);
    }
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}

	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (4 * n[i] * r[i] * r[i + 1] > maxnrr) ? 4 * n[i] * r[i] * r[i + 1] : maxnrr;
  }

  int lG = 0;
  for(int i = 0; i < d; i++){
    lG += 2 * r[i] * 2 * r[i];
  }

	int lworkd =   2 * lG                                       // GL, GR
               + maxnrr                                       // work for Gram and Gram Compression
               + 4 * 2 * rmax + 3 * (2 * rmax) * (2 * rmax);  // work for Gram Compression

	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *indices = NULL, *rankIndices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rankIndices = (int*) malloc(d * sizeof(double));
	if(rankIndices == NULL){
		fprintf(stderr, "rankIndices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	for( int i = 0; i < d; ++i){
		indices[i] =  i % n[i];
		rankIndices[i] = i % size;
	}
	double val = 0; 
	/**
	 * Runtime comparison
	 */

  double normx = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, r, x, workd);

  if(!rank){
    printf("pattacNormSquaredHadamardOpt = %f\n", sqrt(fabs(normx)));
  }

	pattacScale(1./sqrt(fabs(normx)), 0, d, n, r, x);

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}

  // Allocate space for xpx and rxpx
	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; ++i){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; ++i){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}
  // tensor y to for x - rounded(xpx)
	double **y = NULL;
	y = (double**) malloc(d * sizeof(double*));
	if(y == NULL){
		fprintf(stderr, "y was not well allocated\n");
		MPI_Abort(comm, 100);
	}

  // Allocate space for y and ry
	int *ry = NULL;
	ry = (int*) malloc((d + 1) * sizeof(int));
	if(ry == NULL){
		fprintf(stderr, "ry was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	ry[0] = 1;
	ry[d] = 1;
	for(int i = 1; i < d; ++i){
		ry[i] = 2 * r[i];
	}
	for(int i = 0; i < d; ++i){
		y[i] = (double*) malloc(n[i] * ry[i] * ry[i + 1] * sizeof(double));
		if(y[i] == NULL){
			fprintf(stderr, "y[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	double alpha = 2.0;
	double beta = -1.0;
  double normErr = 0;

  // Gram Simultaneous
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank){
    printf("**************************\n");
    printf("* ranks before truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);
  double timingGComp = MPI_Wtime();
  pattacGramCompress(comm, isRedundant, d, n, rxpx, xpx, 1e-6, workd);
  MPI_Barrier(comm);
  timingGComp = MPI_Wtime() - timingGComp;
	if(!rank){
    printf("**************************\n");
    printf("* ranks after truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }

  // y = x - xpx
	pattacFormalAXPBY(d, n, 1., r, x, -1., rxpx, xpx, ry, y);

  normErr = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("|| x - xpx||^2_F/||x||^2_F = %.6e\n", fabs(normErr));
  }
  if(fabs(normErr) > 1e-12 * fabs(normx)){
    if(!rank){
      fprintf(stderr, "Relative error norm produced by pattacGramCompress is larger than expected: %e\n", fabs(normErr)/fabs(normx));
    }
    ierr = 1;
  }
  /********************/
  // Gram LRL
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank){
    printf("**************************\n");
    printf("* ranks before truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);
  double timingGLRLComp = MPI_Wtime();
  pattacGramSeqLRLCompress(comm, isRedundant, d, n, rxpx, xpx, 1e-6, workd);
  MPI_Barrier(comm);
  timingGLRLComp = MPI_Wtime() - timingGLRLComp;
	if(!rank){
    printf("**************************\n");
    printf("* ranks after truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }

  // y = x - xpx
	pattacFormalAXPBY(d, n, 1., r, x, -1., rxpx, xpx, ry, y);

  normErr = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("|| x - xpx||^2_F/||x||^2_F = %.6e\n", fabs(normErr));
  }
  if(fabs(normErr) > 1e-12 * fabs(normx)){
    if(!rank){
      fprintf(stderr, "Relative error norm produced by pattacGramCompress is larger than expected: %e\n", fabs(normErr)/fabs(normx));
    }
    ierr = 1;
  }
  /********************/
  // Gram RLR
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank){
    printf("**************************\n");
    printf("* ranks before truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);
  double timingGRLRComp = MPI_Wtime();
  pattacGramSeqRLRCompress(comm, isRedundant, d, n, rxpx, xpx, 1e-6, workd);
  MPI_Barrier(comm);
  timingGRLRComp = MPI_Wtime() - timingGRLRComp;
	if(!rank){
    printf("**************************\n");
    printf("* ranks after truncation:  \n");
    for(int i = 0; i < d; ++i){
      printf("%d, ", rxpx[i]);
    }
    printf("%d\n", rxpx[d]);
    printf("**************************\n");
  }

  // y = x - xpx
	pattacFormalAXPBY(d, n, 1., r, x, -1., rxpx, xpx, ry, y);

  normErr = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, ry, y, workd);
  if(!rank){
    printf("|| x - xpx||^2_F/||x||^2_F = %.6e\n", fabs(normErr));
  }
  if(fabs(normErr) > 1e-12 * fabs(normx)){
    if(!rank){
      fprintf(stderr, "Relative error norm produced by pattacGramCompress is larger than expected: %e\n", fabs(normErr)/fabs(normx));
    }
    ierr = 1;
  }

	/*****************/
	if(!rank){
		printf("/*************************/\n");
		printf("*         Timing          *\n");
		printf("/*************************/\n");
		printf("pattacGramCompress:    %f\n", timingGComp);
		printf("pattacGramLRLCompress: %f\n", timingGLRLComp);
		printf("pattacGramRLRCompress: %f\n", timingGRLRComp);
		printf("/*************************/\n");
		printf("/*************************/\n");
	}
	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(y[i]);
		free(xpx[i]);
	}
	free(xpx);
	free(rxpx);
	free(y);
	free(ry);
	free(x);
	free(workd);
	free(n);
	free(r);
  ATTAC_STOP_TIMER
	attacFinalizeTimer();
	free(filename);
	MPI_Finalize();
  return ierr;
}
