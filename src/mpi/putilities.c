/** \copyright
 * Copyright 2021 Hussam Al Daas, Grey Ballard, and Peter Benner
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/** \file putilities.c
 * \author Hussam Al Daas
 * \brief Utility functions - parallel
 * \date 16/07/2019
 * \details 
*/
#include <math.h>
#include <unistd.h>
#include "timer.h"
#include "utilities.h"
#include "putilities.h"

/** \fn void attacTSQRUp(const MPI_Comm comm, const char layout, const int n, const int nb, double* r, double* v, double* t, double* work);
 * \brief Compute R/L factor and implicit representation of Q by using TSQR/SFLQ algorithm with binary tree starting from local R matrices
 * \details
 * \param comm
 * \param layout If 'R' computes short fat LQ. If 'C' computes TSQR
 * \param n number of columns in A (TSQR)
 * \param nb block size used by block QR/LQ
 * \param r (i/o) On input: Contain the local R/L matrix to be factored. On output: The factor R/L on root
 * \param v Contain the Householder transformation of subsequent QR factorization in the tree. Minimal size: \f$(ceil(log2(P)) + 1) * n * n\f$
 * \param t Contain reflectors for all QR/LQ factorization performed in the routine. Minimal size: \f$(ceil(log2(P)) + 1) * nb * n\f$
 * \param work Work place of size n * (n + nb) at least
 * \remarks
 * \warning
*/
void attacTSQRUp(const MPI_Comm comm, const char layout, const int n, const int nb, double* r, double* v, double* t, double* work){
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
	
	MPI_Status status;

	int nTr = (n * (n + 1))/2;

	int toffset = 0, voffset = 0;
  int CLog2P = (int) ceil(log2((double)size));

	/**< Either we receive or we send then those can be the same */
	double *recvRq = work;
	double* RpSend = work;
	double* workDtp = work + n * n;

	double *tau = t;

#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
  int TwoPowerI = 0;
	for(int i = 1; i < CLog2P + 1; i++){
    TwoPowerI = (int) pow(2., (double)i);
		if(rank % TwoPowerI == 0 && rank + (TwoPowerI/2) < size){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
			int source = rank + (TwoPowerI/2);
			ATTAC_TIC(2, "Comm Recv")
			ierr = MPI_Recv(recvRq, nTr, MPI_DOUBLE, source, 0, comm, &status);
			ATTAC_TAC(2)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Recv ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, i);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			ATTAC_TIC(3, "Comp")
			double* Rq = v + voffset;
			memset(Rq, 0, n * n * sizeof(double));
			voffset += n * n;
			unpackR(layout, n, recvRq, Rq, n);
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
	TSQR_COMP_TIC
#endif
			tau = t + toffset;
			toffset += nb * n;
			int ldtau = nb;
      if(layout == 'C'){
			  dtpqrt_(&n, &n, &n, &nb, r, &n, Rq, &n, tau, &ldtau, workDtp, &ierr);
      }else{
			  dtplqt_(&n, &n, &n, &nb, r, &n, Rq, &n, tau, &ldtau, workDtp, &ierr);
      }
			ATTAC_TAC(3)
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
		}else if(rank % TwoPowerI == (TwoPowerI/2)){
			int dest = rank - (TwoPowerI/2);
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
			packR(layout, n, r, n, RpSend);
			ATTAC_TIC(1, "Comm Send")
			ierr = MPI_Send(RpSend, nTr, MPI_DOUBLE, dest, 0, comm);
			ATTAC_TAC(1)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, i);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
		}
	}

	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacBFTSQRUp(const MPI_Comm comm, const char layout, const int n, const int nb, double* r, double* v, double* t, double* work);
 * \brief Compute R/L factor and implicit representation of Q by using TSQR/SFLQ algorithm with butterfly tree starting from local R/L matrices
 * \details
 * \param comm
 * \param layout If 'R' computes short fat LQ. If 'C' computes TSQR
 * \param n number of columns in A (TSQR)
 * \param nb block size used by block QR/LQ
 * \param r (i/o) On input: Contain the local R/L matrix to be factored. On output: The factor R/L on root
 * \param v Contain the Householder transformation of subsequent QR factorization in the tree. Minimal size: \f$(ceil(log2(P)) + 1) * n * n\f$
 * \param t Contain reflectors for all QR/LQ factorization performed in the routine. Minimal size: \f$(ceil(log2(P)) + 1) * nb * n\f$
 * \param work Work place of size \f$n * (n + nb)\f$ at least
 * \remarks
 * \warning
*/
void attacBFTSQRUp(const MPI_Comm comm, const char layout, const int n, const int nb, double* r, double* v, double* t, double* work){
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
	
	MPI_Status statSend;
	MPI_Status statRecv;
  MPI_Request reqSend;
  MPI_Request reqRecv;
	int nTr = (n * (n + 1))/2;

  int CLog2P = (int) ceil(log2((double)size));
  int FLog2P = (int) floor(log2((double)size));

	int toffset = 0, voffset = 0;

	/**< Either we receive or we send then those can be the same */
	double *recvRj = work;
	double *sendRr = work + nTr;
  double *workDtp = work + 2 * nTr;

	double *tau = t;

	double *Rj = v;
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	for(int k = CLog2P; k > 0; k--){
    //Determine the neighbor of π(r) for the kth level of the butterfly
    int TwoPowerI = (int)pow(2.0, (double)k);
    int j = TwoPowerI * (rank/TwoPowerI) + ((rank + (TwoPowerI/2)) % TwoPowerI);
    // pack the matrix to be sent
    ATTAC_TIC(1, "Comp")
	  packR(layout, n, r, n, sendRr);
    ATTAC_TAC(1)
    if(rank < j && j < size){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
			ierr = MPI_Irecv(recvRj, nTr, MPI_DOUBLE, j, 0, comm, &reqRecv);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Irecv ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, k);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			ierr = MPI_Isend(sendRr, nTr, MPI_DOUBLE, j, 0, comm, &reqSend);
      ATTAC_TAC(2)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Isend ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, k);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
      // Set where to put the householer vector
      ATTAC_TIC(1, "Comp")
      Rj = v + voffset;
      tau = t + toffset;
      voffset += n * n;
      toffset += n * nb;
      // Zero lower part of householer vector
      memset(Rj, 0, n * n * sizeof(double));
      ATTAC_TAC(1)
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
      // Wait for the whole receipt of recvRj. Then you can perform a TPQRT
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
      MPI_Wait(&reqRecv, &statRecv);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
      // Unpack in Rj
      ATTAC_TIC(1, "Comp")
      unpackR(layout, n, recvRj, Rj, n);
      if(layout == 'C'){
			  dtpqrt_(&n, &n, &n, &nb, r, &n, Rj, &n, tau, &nb, workDtp, &ierr);
      }else{
			  dtplqt_(&n, &n, &n, &nb, r, &n, Rj, &n, tau, &nb, workDtp, &ierr);
      }
      ATTAC_TAC(1)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::TP(QR/LQ)T ierr = %d at level %d, rank = %d \n", __FILE__, __LINE__, __func__, ierr, k, rank);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
      // Wait for sendRr to be fully sent
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
      MPI_Wait(&reqSend, &statSend);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
    }else if(j < size){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
      // Set where to put the householer vector
      Rj = v + voffset;
      tau = t + toffset;
      voffset += n * n;
      toffset += n * nb;
      ATTAC_TIC(2, "Comm")
			ierr = MPI_Irecv(recvRj, nTr, MPI_DOUBLE, j, 0, comm, &reqRecv);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Irecv ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, k);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			ierr = MPI_Isend(sendRr, nTr, MPI_DOUBLE, j, 0, comm, &reqSend);
      ATTAC_TAC(2)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Isend ierr = %d at level %d \n", __FILE__, __LINE__, __func__, ierr, k);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
      ATTAC_TIC(1, "Comp")
      // Swap Rr for Rj
      memcpy(Rj, r, n * n * sizeof(double));
      // Zero lower part of householer vector
      memset(r, 0, n * n * sizeof(double));
      ATTAC_TAC(1)
      // Wait for the whole receipt of recvRj. Then you can perform a TPQRT
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
      MPI_Wait(&reqRecv, &statRecv);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
	TSQR_COMP_TIC
#endif
      ATTAC_TIC(1, "Comp")
      // Unpack in Rr (originally)
      unpackR(layout, n, recvRj, r, n);
      if(layout == 'C'){
			  dtpqrt_(&n, &n, &n, &nb, r, &n, Rj, &n, tau, &nb, workDtp, &ierr);
      }else{
			  dtplqt_(&n, &n, &n, &nb, r, &n, Rj, &n, tau, &nb, workDtp, &ierr);
      }
      ATTAC_TAC(1)
      // Wait for sendRr to be fully sent
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
      MPI_Wait(&reqSend, &statSend);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
    }
    if(rank >= ((int)pow(2, FLog2P))){
      break;
    }
  }
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacTSQR(const MPI_Comm comm, const char layout, const int m, const int n, const int nb, double* a, const int lda, double* r, double* v, double* t, double* work);
 * \brief Compute R factor and implicit representation of Q by using TSQR algorithm with binary tree
 * \details
 * \param comm
 * \param layout
 * \param m
 * \param n
 * \param nb
 * \param a (i/o) On input: Contain the matrix to be factored. On output: Contain the Householder for the QR factorization of the local matrix
 * \param lda
 * \param r Contain the factor R
 * \param v Contain the Householder transformation of subsequent QR factorization in the treer. Minimal size: \f$(ceil(log2(P)) + 1) * n * n\f$
 * \param t Contain reflectors for all QR factorization performed in the routine. Minimal size: \f$(ceil(log2(P)) + 1) * nb * n\f$
 * \param work Work place of size \f$2 * n * n\f$ at least
 * \remarks
 * \warning
*/
void attacTSQR(const MPI_Comm comm, const char layout, const int m, const int n, const int nb, double* a, const int lda, double* r, double* v, double* t, double* work){
#if TIME_BREAKDOWN
	MPI_Barrier(comm);
#endif
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
  if(m < n){
    fprintf(stderr, "TSQR does not allow number of rwos (%d) smaller than number of columns (%d)\n", m, n);
    MPI_Abort(MPI_COMM_WORLD, -99);
  }

	int toffset = 0;
	double *tau = t;
	toffset += n * nb;
  if(layout == 'R'){
    ATTAC_TIC(1, "GELQT")
	  dgelqt_(&n, &m, &nb, a, &lda, tau, &nb, work, &ierr);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(2, "GEQRT")
	  dgeqrt_(&m, &n, &nb, a, &m, tau, &nb, work, &ierr);
    ATTAC_TAC(2)
  }
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::GEQ:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
		MPI_Abort(MPI_COMM_WORLD, -99);
	}

  ATTAC_TIC(3, "Extract R/L and zero")
  if(layout == 'C'){
    dlacpy_("U", &n, &n, a, &lda, r, &n);
  }else{
    dlacpy_("L", &n, &n, a, &lda, r, &n);
  }
  setLowerZero(layout, n, n, r, n);
  ATTAC_TAC(3)

	tau = t + toffset;
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	if(size > 1) attacTSQRUp(comm, layout, n, nb, r, v, tau, work);
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}

#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	MPI_Barrier(comm);
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacBFTSQR(const MPI_Comm comm, const char layout, const int m, const int n, const int nb, double* a, const int lda, double* r, double* v, double* t, double* work);
 * \brief Compute R factor and implicit representation of Q by using TSQR algorithm with binary tree
 * \details
 * \param comm
 * \param layout
 * \param m
 * \param n
 * \param nb
 * \param a (i/o) On input: Contain the matrix to be factored. On output: Contain the Householder for the QR factorization of the local matrix
 * \param lda
 * \param r Contain the factor R
 * \param v Contain the Householder transformation of subsequent QR factorization in the treer. Minimal size: \f$(ceil(log2(P)) + 1) * n * n\f$
 * \param t Contain reflectors for all QR factorization performed in the routine. Minimal size: \f$(ceil(log2(P)) + 1) * nb * n\f$
 * \param work Work place of size \f$2 * n * n\f$ at least
 * \remarks
 * \warning
*/
void attacBFTSQR(const MPI_Comm comm, const char layout, const int m, const int n, const int nb, double* a, const int lda, double* r, double* v, double* t, double* work){
#if TIME_BREAKDOWN
	MPI_Barrier(comm);
#endif
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
  if(m < n){
    fprintf(stderr, "TSQR does not allow number of rwos (%d) smaller than number of columns (%d)\n", m, n);
    MPI_Abort(MPI_COMM_WORLD, -99);
  }

	int toffset = 0;
	double *tau = t;
	toffset += n * nb;
  if(layout == 'R'){
    ATTAC_TIC(1, "GELQT")
	  dgelqt_(&n, &m, &nb, a, &lda, tau, &nb, work, &ierr);
    ATTAC_TAC(1)
  }else{
    ATTAC_TIC(2, "GEQRT")
	  dgeqrt_(&m, &n, &nb, a, &m, tau, &nb, work, &ierr);
    ATTAC_TAC(2)
  }

  ATTAC_TIC(3, "Extract R/L and zero")
  if(layout == 'C'){
    dlacpy_("U", &n, &n, a, &lda, r, &n);
  }else{
    dlacpy_("L", &n, &n, a, &lda, r, &n);
  }
  setLowerZero(layout, n, n, r, n);
  ATTAC_TAC(3)

	tau = t + toffset;
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	if(size > 1) attacBFTSQRUp(comm, layout, n, nb, r, v, tau, work);

	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	MPI_Barrier(comm);
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacTSQRUpApplyQ(const MPI_Comm comm, const char layout, int n, const int k, const int nb, const double* v, const double* t, double* c, int ldc, double* work);
 * \brief
 * \details
 * \param comm
 * \param layout
 * \param n
 * \param k
 * \param nb
 * \param v
 * \param t
 * \param c
 * \param ldc
 * \param work
 * \remarks
 * \warning
*/
void attacTSQRUpApplyQ(const MPI_Comm comm, const char layout, int *pn, const int k, const int nb, const double* v, const double* t, double* c, int ldc, double* work){
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	int n = pn[0];
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
	
	MPI_Status status;
	
  int CLog2P = (int) ceil(log2((double)size));
	
	int voffset = 0, toffset = 0;

	const double* yl = v + voffset;
	const double* tl = t + toffset;

	double* Bq = work;
	double* workDtp = work + k * k;
	int TwoPowerI = 0;
	for(int i = 1; i < CLog2P + 1; i++){
    TwoPowerI = (int) pow(2., (double)i);
		if(rank % (TwoPowerI) == 0 && rank + (TwoPowerI/2) < size){
			yl = v + voffset;
			voffset += k * k;
			tl = t + toffset;
			toffset += nb * k;
		}
	}

#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	for(int i = CLog2P; i > 0; i--){
    TwoPowerI = (int) pow(2., (double)i);
		if(rank % TwoPowerI == 0 && rank + (TwoPowerI/2) < size){
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
			ATTAC_TIC(3, "Comp")
			int dest = rank + (TwoPowerI/2);
			voffset -= k * k;
			yl = v + voffset;
			toffset -= nb * k;
			tl = t + toffset;
			memset(Bq, 0, k * n * sizeof(double));
			int ldtl = nb;
			int ldb = (layout == 'C') ? k : n;
      if(layout == 'C'){
			  dtpmqrt_("L", "N", &k, &n, &k, &k, &nb, yl, &k, tl, &ldtl, c, &ldc, Bq, &ldb, workDtp, &ierr);
      }else{
			  dtpmlqt_("R", "N", &n, &k, &k, &k, &nb, yl, &k, tl, &ldtl, c, &n, Bq, &ldb, workDtp, &ierr);
      }
			ATTAC_TAC(3)
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_COMM_TIC
#endif
			ATTAC_TIC(1, "Comm Send")
			ierr = MPI_Send(Bq, k * n, MPI_DOUBLE, dest, 0, comm);
			ATTAC_TAC(1)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
		}else if(rank % TwoPowerI == (TwoPowerI/2)){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
			int source = rank - (TwoPowerI/2);
			/**< At most receive k * k double. Get the count (= n * k) from the status */
			ATTAC_TIC(2, "Comm Recv")
			ierr = MPI_Recv(Bq, k * k, MPI_DOUBLE, source, 0, comm, &status);
			ATTAC_TAC(2)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Recv:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			ATTAC_TIC(3, "Comp")
			ierr = MPI_Get_count(&status, MPI_DOUBLE, &n);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Get_count:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			n = n/k;
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
	TSQR_COMP_TIC
#endif
			int ldb = (layout == 'C') ? k : n;
      if(layout == 'R'){
        ldc = n;
        dlacpy_("A", &n, &k, Bq, &ldb, c, &n);
      }else{
        dlacpy_("A", &k, &n, Bq, &ldb, c, &ldc);
      }
			ATTAC_TAC(3)
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
		}
	}
	pn[0] = n;
	/**< To return back the value of n which not all processors might know previously */
	work[0] = (double) n;
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacBFOptBcastR(const MPI_Comm comm, const char layout, int n, double* R, double* work);
 * \brief
 * \details
 * \param comm
 * \param layout
 * \param n
 * \param R
 * \param work
 * \remarks
 * \warning
*/
void attacBFOptBcastR(const MPI_Comm comm, const char layout, int n, double* R, double* work){
	ATTAC_START_TIMER
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
  MPI_Status status;
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
  int CLog2P = (int) ceil(log2((double)size));
  int FLog2P = (int) floor(log2((double)size));
  if(CLog2P != FLog2P){
    if(rank < size - ((int)pow(2., (double)FLog2P))){
      packR(layout, n, R, n, work);
		  ierr = MPI_Send(work, (n * (n + 1))/2, MPI_DOUBLE, rank + ((int)pow(2., (double)FLog2P)), 0, comm);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
    }
    if(rank >= ((int)pow(2., (double)FLog2P))){
		  ierr = MPI_Recv(work, (n * (n + 1))/2, MPI_DOUBLE, rank - ((int)pow(2., (double)FLog2P)), 0, comm, &status);
      unpackR(layout, n, work, R, n);
      setLowerZero(layout, n, n, R, n);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
    }
  }
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void attacBFOptBcastR(const MPI_Comm comm, int Rank);
 * \brief
 * \details
 * \param comm
 * \param Rank
 * \remarks
 * \warning
*/
void attacBFOptBcastRank(const MPI_Comm comm, int Rank){
	ATTAC_START_TIMER
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
  MPI_Status status;
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	
  int CLog2P = (int) ceil(log2((double)size));
  int FLog2P = (int) floor(log2((double)size));
  if(CLog2P != FLog2P){
    if(rank < size - ((int)pow(2., (double)FLog2P))){
		  ierr = MPI_Send(&Rank, 1, MPI_INT, rank + ((int)pow(2., (double)FLog2P)), 0, comm);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
    }
    if(rank >= ((int)pow(2., (double)FLog2P))){
		  ierr = MPI_Recv(&Rank, 1, MPI_INT, rank - ((int)pow(2., (double)FLog2P)), 0, comm, &status);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
    }
  }
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
	ATTAC_STOP_TIMER
}

/** \fn void attacBFTSQRUpApplyQ(const MPI_Comm comm, const char layout, int n, const int k, const int nb, const double* v, const double* t, double* c, int ldc, double* work);
 * \brief
 * \details
 * \param comm
 * \param layout
 * \param n
 * \param k
 * \param nb
 * \param v
 * \param t
 * \param c
 * \param ldc
 * \param work
 * \remarks
 * \warning
*/
void attacBFTSQRUpApplyQ(const MPI_Comm comm, const char layout, int *pn, const int k, const int nb, const double* v, const double* t, double* c, int ldc, double* work){
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	int n = pn[0];
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}	

  int CLog2P = (int) ceil(log2((double)size));
  int FLog2P = (int) floor(log2((double)size));

	double* Bq = work;
  double *workDtp = work + k * k;

	int ldb = (layout == 'C') ? k : n;

	int voffset = 0, toffset = 0;

	for(int i = CLog2P; i > 0; i--){
    //Determine the neighbor of π(r) for the kth level of the butterfly
    int TwoPowerI = (int)pow(2.0, (double)i);
    int j = TwoPowerI * (rank/TwoPowerI) + ((rank + (TwoPowerI/2)) % TwoPowerI);
    // pack the matrix to be sent
    if(rank < j && j < size){
      voffset += k * k;
      toffset += k * nb;
    }else if(j < size){
      // Set where to put the householer vector
      voffset += k * k;
      toffset += k * nb;
    }
    if(rank >= ((int)pow(2, FLog2P))){
      break;
    }
  }

	const double* yl = v + voffset;
	const double* tl = t + toffset;
  int maxDepth = 0;
  if(CLog2P == FLog2P){
    maxDepth = CLog2P + 1;
  }else{
    maxDepth = CLog2P;
  }
  MPI_Status status;
	for(int i = 1; i < CLog2P + 1; i++){
    int TwoPowerI = (int)pow(2.0, (double)i);
    int j = TwoPowerI * (rank/TwoPowerI) + ((rank + (TwoPowerI/2)) % TwoPowerI);
    if(rank >= (int)pow(2., (double)FLog2P)){
      break;
    }
    if(rank < j && j < size){
      voffset = voffset - k * k;
      toffset = toffset - k * nb;
      yl = v + voffset;
      tl = t + toffset;

      ATTAC_TIC(1, "Comp")
      memset(Bq, 0, k * n * sizeof(double));
      if(layout == 'C'){
			  dtpmqrt_("L", "N", &k, &n, &k, &k, &nb, yl, &k, tl, &nb, c, &ldc, Bq, &ldb, workDtp, &ierr);
      }else{
			  dtpmlqt_("R", "N", &n, &k, &k, &k, &nb, yl, &k, tl, &nb, c, &n, Bq, &ldb, workDtp, &ierr);
      }
      ATTAC_TAC(1)
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::dtpmqrt_:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, -99);
			}
    }else if(j < size){
      voffset = voffset - k * k;
      toffset = toffset - k * nb;
      yl = v + voffset;
      tl = t + toffset;
      ATTAC_TIC(1, "Comp")
      memset(Bq, 0, k * n * sizeof(double));
      if(layout == 'C'){
			  dtpmqrt_("L", "N", &k, &n, &k, &k, &nb, yl, &k, tl, &nb, c, &ldc, Bq, &ldb, workDtp, &ierr);
      }else{
			  dtpmlqt_("R", "N", &n, &k, &k, &k, &nb, yl, &k, tl, &nb, c, &n, Bq, &ldb, workDtp, &ierr);
      }
      if(layout == 'R'){
        dlacpy_("A", &n, &k, Bq, &n, c, &n);
      }else{
        dlacpy_("A", &k, &n, Bq, &k, c, &ldc);
      }
      ATTAC_TAC(1)
    }
	}
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
    //TODO: make this asynchronous and inside the upper loop
  if(CLog2P != FLog2P){
    if(rank < size - ((int)pow(2., (double)FLog2P))){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
		  ierr = MPI_Send(Bq, k * n, MPI_DOUBLE, rank + ((int)pow(2., (double)FLog2P)), 0, comm);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
    }
    if(rank >= ((int)pow(2., (double)FLog2P))){
#if TIME_BREAKDOWN
	TSQR_COMM_TIC
#endif
      ATTAC_TIC(2, "Comm")
		  ierr = MPI_Recv(Bq, k * k, MPI_DOUBLE, rank - ((int)pow(2., (double)FLog2P)), 0, comm, &status);
      ATTAC_TAC(2)
#if TIME_BREAKDOWN
	TSQR_COMM_TAC
#endif
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Send:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
      ATTAC_TIC(1, "Comp")
			ierr = MPI_Get_count(&status, MPI_DOUBLE, &n);
			if(ierr != 0){
				fprintf(stderr, "%s:Line %d %s::MPI_Get_count:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
				MPI_Abort(MPI_COMM_WORLD, ierr);
			}
			n = n/k;
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
      if(layout == 'R'){
        dlacpy_("A", &n, &k, Bq, &n, c, &n);
      }else{
        dlacpy_("A", &k, &n, Bq, &ldb, c, &ldc);
      }
      ATTAC_TAC(1)
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
    }
  }

	pn[0] = n;
	/**< To return back the value of n which not all processors might know previously */
	work[0] = (double) n;
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_IDLE_TIC
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacTSQRApplyQ(const MPI_Comm comm, const char layout, const int m, int *n, const int k, const int nb, double* a, const int lda, const double* v, const double* t, double* c, int ldc, double* work);
 * \brief
 * \details
 * \param comm
 * \param layout
 * \param m
 * \param n
 * \param k
 * \param nb
 * \param a
 * \param lda
 * \param v
 * \param t
 * \param c
 * \param ldc
 * \param work
 * \remarks
 * \warning
*/
void attacTSQRApplyQ(const MPI_Comm comm, const char layout, const int m, int *pn, const int k, const int nb, double* a, const int lda, const double* v, const double* t, double* c, int ldc, double* work){
#if TIME_BREAKDOWN
	MPI_Barrier(comm);
#endif
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	int n = pn[0];
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}

#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	if(size > 1) attacTSQRUpApplyQ(comm, layout, pn, k, nb, v, t + k * nb, c, ldc, work);
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
  n = pn[0];
	if(layout == 'R'){
    ATTAC_TIC(2, "GEMLQ")
		dgemlqt_("R", "N", &n, &m, &k, &nb, a, &lda, t, &nb, c, &n, work, &ierr);
    ATTAC_TAC(2)
	}else{
    ATTAC_TIC(3, "GEMQR")
		dgemqrt_("L", "N", &m, &n, &k, &nb, a, &lda, t, &nb, c, &ldc, work, &ierr);
    ATTAC_TAC(3)
	}
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::GEMQR/LQ:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
		printf("layout = %c, m = %d, n = %d, k = %d, lda = %d\n", layout, m, n, k, lda);
		MPI_Abort(MPI_COMM_WORLD, ierr);
	}

	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_IDLE_TIC
	MPI_Barrier(comm);
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void attacBFTSQRApplyQ(const MPI_Comm comm, const char layout, const int m, int *n, const int k, const int nb, double* a, const int lda, const double* v, const double* t, double* c, int ldc, double* work);
 * \brief
 * \details
 * \param comm
 * \param layout
 * \param m
 * \param n
 * \param k
 * \param nb
 * \param a
 * \param lda
 * \param v
 * \param t
 * \param c
 * \param ldc
 * \param work
 * \remarks
 * \warning
*/
void attacBFTSQRApplyQ(const MPI_Comm comm, const char layout, const int m, int *pn, const int k, const int nb, double* a, const int lda, const double* v, const double* t, double* c, int ldc, double* work){
#if TIME_BREAKDOWN
	MPI_Barrier(comm);
#endif
	ATTAC_START_TIMER
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
	int ierr = 0, rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
	int n = pn[0];
	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s begins\n", __func__);
	}
	
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
#endif
	if(size > 1) attacBFTSQRUpApplyQ(comm, layout, pn, k, nb, v, t + k * nb, c, ldc, work);
	n = pn[0];
#if TIME_BREAKDOWN
	TSQR_COMP_TIC
#endif
	if(layout == 'C'){
    ATTAC_TIC(1, "GEMQR")
		dgemqrt_("L", "N", &m, &n, &k, &nb, a, &lda, t, &nb, c, &ldc, work, &ierr);
    ATTAC_TAC(1)
	}else{
    ATTAC_TIC(2, "GEMLQ")
		dgemlqt_("R", "N", &n, &m, &k, &nb, a, &lda, t, &nb, c, &n, work, &ierr);
    ATTAC_TAC(2)
	}
	if(ierr != 0){
		fprintf(stderr, "%s:Line %d %s::GEMQR/LQ:: ierr = %d \n", __FILE__, __LINE__, __func__, ierr);
		printf("layout = %c, m = %d, n = %d, k = %d, lda = %d\n", layout, m, n, k, lda);
		MPI_Abort(MPI_COMM_WORLD, ierr);
	}

	if(ATTAC_VERBOSITY > 1000 && !rank){
		printf("%s ends\n", __func__);
	}	
#if TIME_BREAKDOWN
	TSQR_COMP_TAC
	TSQR_IDLE_TIC
	MPI_Barrier(comm);
	TSQR_IDLE_TAC
#endif
	ATTAC_STOP_TIMER
}

/** \fn void laplace1d(const MPI_Comm comm, const int m, const double *x, double *Ax)
 * \brief 1d laplace operator
 * \details
 * \param comm communicator
 * \param m length of local vector
 * \param x input vector
 * \param Ax output A times x
 * \remarks
 * \warning works only for number of procs > 1
*/
void laplace1d(const MPI_Comm comm, const int m, const double *x, double *Ax){
	int rank = 0, size = 0;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);
  MPI_Request requests;
  MPI_Request requests1;
  MPI_Request requestr;
  MPI_Request requestr1;
  MPI_Status statuss;
  MPI_Status statuss1;
  MPI_Status statusr;
  MPI_Status statusr1;
	double x0m1 = 0;
	double xmp1 = 0;
	if(rank == 0){
		MPI_Isend(x + m - 1, 1, MPI_DOUBLE, 1, 0, comm, &requests);
		MPI_Irecv(&xmp1, 1, MPI_DOUBLE, 1, 0, comm, &requestr);
	}else if(rank == size - 1){
		MPI_Isend(x, 1, MPI_DOUBLE, size - 2, 0, comm, &requests);
		MPI_Irecv(&x0m1, 1, MPI_DOUBLE, size - 2, 0, comm, &requestr);
	}else{
		MPI_Isend(x + m - 1, 1, MPI_DOUBLE, rank + 1, 0, comm, &requests);
		MPI_Isend(x, 1, MPI_DOUBLE, rank - 1, 0, comm, &requests1);
		MPI_Irecv(&xmp1, 1, MPI_DOUBLE, rank + 1, 0, comm, &requestr);
		MPI_Irecv(&x0m1, 1, MPI_DOUBLE, rank - 1, 0, comm, &requestr1);
	}
	for(int j = 0; j < m; j++){
		Ax[j] = 2 * x[j] - x[j - 1] - x[j + 1];
	}
	MPI_Wait(&requests, &statuss);
	MPI_Wait(&requestr, &statusr);
	if(rank > 0 && rank < size - 1){
		MPI_Wait(&requests1, &statuss1);
		MPI_Wait(&requestr1, &statusr1);
	}
	if(rank != 0){
		Ax[m - 1] = 2 * x[m - 1] - xmp1;
	}
	if(rank != size - 1){
		Ax[0] = 2 * x[0] - x0m1;
	}
}
