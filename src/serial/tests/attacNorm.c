/** \file attacNorm.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <math.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 10;
	int *n = NULL;
	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(1);
	}

	int *r = NULL;
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(1);
	}

	double **x = NULL;
	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(1);
	}

	r[0] = 1;
	r[d] = 1;
	n[0] = 3;
	for(int i = 1; i < d; ++i){
		r[i] =  2 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = 2 + 1 * i;
	}

	int nmax = 0;
	int rmax = 0;
	int rnrmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
		rnrmax = (r[i] * n[i] * r[i + 1] > rnrmax) ? r[i] * n[i] * r[i + 1] : rnrmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			exit(1);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}

	double *workd = NULL;
	workd = (double*) malloc((
													rnrmax + 2 * rmax * rmax // norm computation
													) * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(1);
	}

	/**
	 * Norm computation
	 */

	double norm = 0;
	norm = attacNorm(d, n, r, x, workd);
  if(fabs(norm - 116617.7948023104545427)/116617.7948023104545427 > 1e-15){
    fprintf(stderr, "Norm not equal to what expected\n");
    return 1;
  }
	for(int i = 0; i < d; ++i){
		free(x[i]);
	}
	free(x);
	free(workd);
	free(n);
	free(r);
  return 0;
}




