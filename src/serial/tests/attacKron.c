/** \file attacKron.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <math.h>
#include "utilities.h"

int main(int argc, char* argv[]){
	double *M = NULL;
	double *N = NULL;

  int mM = 4, nM = 2;
  int mN = 2, nN = 3;

	int ldm = 9;
	int ldn = 11;

	M = (double*) malloc(ldm * nM * sizeof(double));
	N = (double*) malloc(ldn * nN * sizeof(double));

  /*
     M = [0 4
          1 5
          2 6
          3 7];
  */
	for(int j = 0; j < nM; ++j){
		for(int i = 0; i < mM; ++i){
			M[i + j * ldm] = i + mM * j;
		}
	}
  /*
     N = [0 2 4
          1 3 5];
  */
	for(int j = 0; j < nN; ++j){
		for(int i = 0; i < mN; ++i){
			N[i + j * ldn] = i + mN * j;
		}
	}
	
	double *MKN = NULL;
	MKN = (double*) malloc(mM * mN * nM * nN * sizeof(double));
	attacKron(mM, nM, M, ldm, mN, nN, N, ldn, MKN, mM * mN);
  double ExactMKN[4*2*2*3] = { 0,  0,  0,  1,  0,  2,  0,  3, 
                               0,  0,  2,  3,  4,  6,  6,  9,
                               0,  0,  4,  5,  8, 10, 12, 15,
                               0,  4,  0,  5,  0,  6,  0,  7,
                               8, 12, 10, 15, 12, 18, 14, 21,
                              16, 20, 20, 25, 24, 30, 28, 35};
                              
  for(int j = 0; j < nN * nM * mM * mN; j++){
    MKN[j] -= ExactMKN[j];
    if(fabs(MKN[j]) > 1e-15){
      fprintf(stderr, "Kronecker product incorrect\n");
      return 1;
    }
  }
  return 0;
}
