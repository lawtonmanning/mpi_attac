/** \file attacOrtho.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <time.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 10;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(0);
	}

	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(0);
	}

	double **x = NULL;
	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(0);
	}

	r[0] = 1;
	r[d] = 1;
	n[0] = 100;
	for(int i = 1; i < d; ++i){
		r[i] = 5 + abs(d/2 - abs(d/2 - i));
		n[i] = 100 + i * 10;
	}

	printf("**********************************\n");
	printf("*             attacOrtho           *\n");
	printf("**********************************\n");
	printf("d = %d\n", d);
	printf("**********************************\n");
	for(int i = 0; i < d - 1; ++i){
		printf("%d, ", n[i]);
	}
	printf("%d\n", n[d - 1]);
	printf("**********************************\n");
	for(int i = 0; i < d; ++i){
		printf("%d, ", r[i]);
	}
	printf("%d\n", r[d]);
	printf("**********************************\n");

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			exit(0);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}

	int *worki = NULL;
	worki = (int*) malloc( (d + 1) * sizeof(int));
	if(worki == NULL){
		fprintf(stderr, "worki was not well allocated\n");
		exit(0);
	}

	double *workd = NULL;
	workd = (double*) malloc((
													2 * rmax * 2 * rmax // svd in compression (u or vt)
													+ 2 * 2 * rmax // svd in compression (sig and superb) or compute TT value
													+ nmax // norm computation
													+ (2 * rmax) * (2 * rmax) * nmax // core work space in svd
													) * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(0);
	}

	/**
	 * Runtime comparison: Left and Right orthogonlaization 
	 */

	attacLOrthogonalization(d, n, r, x, workd);

	clock_t begin, end;
	double timing1 = 0, timing2 = 0;
	double nruns = 10.;

	for(int i = 0; i < nruns; ++i){
		begin = clock();
		attacLOrthogonalization(d, n, r, x, workd);
		end = clock();
		timing1 += ((double) end - begin)/(CLOCKS_PER_SEC * nruns);
		attacScale(1., 0, d, n, r, x);
		attacScale(1., d - 1, d, n, r, x);
	}

	for(int i = 0; i < nruns; ++i){
		begin = clock();
		attacROrthogonalization(d, n, r, x, workd);
		end = clock();
		timing2 += ((double) end - begin)/(CLOCKS_PER_SEC * nruns);
		attacScale(1., 0, d, n, r, x);
		attacScale(1., d - 1, d, n, r, x);
	}

	printf("/*************************/\n");
	printf("*     Time comparison     *\n");
	printf("/*************************/\n");
	printf("Left orthogonalization: %f\n", timing1);
	printf("Right orthogonalization: %f\n", timing2);
	printf("Number of runs = %d\n", (int)nruns);
	for(int i = 0; i < d; ++i){
		free(x[i]);
	}
	free(x);
	free(workd);
	free(worki);
	free(n);
	free(r);
  return 0;
}
