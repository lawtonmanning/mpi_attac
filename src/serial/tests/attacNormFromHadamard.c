/** \file attacNormFromHadamard.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 10;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	r = (int*) malloc((d + 1) * sizeof(int));

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	r[0] = 1;
	r[d] = 1;
	n[0] = 1000;
	for(int i = 1; i < d; ++i){
		r[i] =  5 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = 1000 + 10 * i;
	}

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}

	int *worki = NULL;
	worki = (int*) malloc( (d + 1) * sizeof(int));

	double *workd = NULL;

	workd = (double*) malloc((rmax * rmax + nmax +  (2 * rmax) * (2 * rmax) * (2 * rmax) * (2 * rmax) * nmax * d) * sizeof(double));

	/**
	 * Runtime comparison: Left and Right compression 
	 */

	double **xHx = NULL;
	xHx = (double**) malloc(d * sizeof(double*));

	int *rxHx = NULL;
	rxHx = (int*) malloc((d + 1) * sizeof(int));

	for(int i = 0; i < d + 1; ++i){
		rxHx[i] = r[i] * r[i];
	}
	for(int i = 0; i < d; ++i){
		xHx[i] = (double*) malloc(n[i] * rxHx[i] * rxHx[i + 1] * sizeof(double));
	}

	printf("Hadamard product of x by itself\n");
	attacHadamard(d, n, r, x, r, x, rxHx, xHx);

	for(int i = 0; i < d; ++i){
		printf("x[%d] is %d x %d x %d \n", i, r[i], n[i], r[i + 1]);
		printMatrix('C', n[i] * r[i], r[i + 1], x[i], n[i] * r[i]);
		printf("xHx[%d] is %d x %d x %d \n", i, rxHx[i], n[i], rxHx[i + 1]);
		printMatrix('C', n[i] * rxHx[i], rxHx[i + 1], xHx[i], n[i] * rxHx[i]);
	}

	printf("Compute the norm of x by forming the Hadamard product of x by itself\n");
	double norm = attacNormFromHadamard(d, n, rxHx, xHx, workd);
	printf("norm x = %f \n", norm);

	printf("Right orthogonlaization of x\n");
	attacROrthogonalization(d, n, r, x, workd);
	printf("Hadamard product of x (R orthogonalized) by itself\n");
	attacHadamard(d, n, r, x, r, x, rxHx, xHx);
	printf("Compute the norm of x (R orthogonalized) by forming the Hadamard product of x (R orthogonalized) by itself\n");
	norm = attacNormFromHadamard(d, n, rxHx, xHx, workd);

	printf("norm x = %f \n", norm);
	free(rxHx);
	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(xHx[i]);
	}
	free(x);
	free(xHx);
	free(workd);
	free(worki);
	free(n);
	free(r);
  return 0;
}



