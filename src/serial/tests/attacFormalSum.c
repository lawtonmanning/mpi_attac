/** \file attacFormalSum.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	printf("Hello world\n");
	int d = 4;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(0);
	}

	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(0);
	}

	double **x = NULL;
	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(0);
	}

	r[0] = 1;
	r[d] = 1;
	n[0] = 4;
	for(int i = 1; i < d; ++i){
		r[i] = 2;
		n[i] = 4;
	}

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			exit(0);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}

	int *worki = NULL;
	worki = (int*) malloc( (d + 1) * sizeof(int));
	if(worki == NULL){
		fprintf(stderr, "worki was not well allocated\n");
		exit(0);
	}

	/*
	double *workd = NULL;
	workd = (double*) malloc((
													 2 * rmax * 2 * rmax // svd in compression (u or vt)
													+ 2 * 2 * rmax // svd in compression (sig and superb) or compute TT value
													+ nmax // norm computation
													+ (2 * rmax) * (2 * rmax) * nmax // core work space in svd
													) * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(0);
	}
	*/

	/**
	 * Formal Sum And AXPBY
	 */
	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		exit(0);
	}

	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		exit(0);
	}

	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; ++i){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; ++i){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			exit(0);
		}
	}

	attacFormalSum(d, n, r, x, r, x, rxpx, xpx);
	printf("Formal Sum result\n");
	for(int i = 0; i < d; ++i){
		printf("x[%d] is %d x %d x %d \n", i, r[i], n[i], r[i + 1]);
		printMatrix('C', n[i] * r[i], r[i + 1], x[i], n[i] * r[i]);
		printf("xpx[%d] is %d x %d x %d \n", i, rxpx[i], n[i], rxpx[i + 1]);
		printMatrix('C', n[i] * rxpx[i], rxpx[i + 1], xpx[i], n[i] * rxpx[i]);
	}

	attacFormalAXPBY(d, n, 1., r, x, 1., r, x, rxpx, xpx);
	printf("Formal AXPBY result (alpha = %f, beta = %f)\n", 1., 1.);
	for(int i = 0; i < d; ++i){
		printf("x[%d] is %d x %d x %d \n", i, r[i], n[i], r[i + 1]);
		printMatrix('C', n[i] * r[i], r[i + 1], x[i], n[i] * r[i]);
		printf("xpx[%d] is %d x %d x %d \n", i, rxpx[i], n[i], rxpx[i + 1]);
		printMatrix('C', n[i] * rxpx[i], rxpx[i + 1], xpx[i], n[i] * rxpx[i]);
	}
	free(rxpx);
	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(xpx[i]);
	}
	free(x);
	free(xpx);
	//free(workd);
	free(worki);
	free(n);
	free(r);
  return 0;
}

