/** \file attacGetTreeDepth.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 11/06/2020
 * \details
*/
#include <string.h>
#include <math.h>
#include "utilities.h"

int main(int argc, char* argv[]){
  int *s = NULL,  *TD = NULL;
  int N = 16;
  s = (int*) malloc( N * sizeof(int));
  TD = (int*) malloc( N * sizeof(int));
  if(s == NULL || TD == NULL){
    fprintf(stderr, "Memory allocation issue\n");
  }

  for(int i = 0; i < N; i++){
    s[i] = i + 1;
  }

  for(int i = 0; i < N; i++){
    getTreeDepth(s[i], &TD[i]);
    printf("ceiling(Log2(%d)) = %d\n", s[i], TD[i]);
  }
  for(int i = 0; i < N; i++){
    getTreeDepth(s[i], &TD[i]);
    printf("ceil(Log2(%d)) = %d\n", s[i], (int)ceil(log2((double)s[i])));
  }
  for(int i = 0; i < N; i++){
    getTreeDepth(s[i], &TD[i]);
    printf("floor(Log2(%d)) = %d\n", s[i], (int)floor(log2((double)s[i])));
  }
  free(s);
  free(TD);
  return 0;
}
