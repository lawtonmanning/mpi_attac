/** \file attacNormOutplace.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 01/07/2020
 * \details
*/
#include <string.h>
#include <time.h>
#include <math.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 10;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(1);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(1);
	}

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(1);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = 1000;
	for(int i = 1; i < d; ++i){
		r[i] =  5 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = 1000 + 10 * i;// + 100 * i;
	}

  printf("**************************\n");
  printf("* Tensor Order: %d\n", d);
  printf("* Tensor Mode Sizes: \n");
  for(int i = 0; i < d - 1; ++i){
    printf("n[%d] = %d, ", i, n[i]);
  }
	printf("n[%d] = %d\n", d - 1, n[d - 1]);
  printf("**************************\n");

	int nmax = 0;
	int rmax = 0;
	int Srr = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}
	for(int i = 1; i < d; ++i){
		Srr += 4 * r[i] * r[i];
	}
	Srr += (r[d - 1] > r[1]) ? 4 * r[d - 1] * r[d - 1] : 4 * r[1] * r[1];

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			exit(1);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}
	for(int i = 0; i < d; ++i){
		double normxi = 0;
    int rn = r[i] * n[i];
	  normxi = dlange_("F", &rn, &r[i + 1], x[i], &rn, NULL);
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}

	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (4 * n[i] * r[i] * r[i + 1] > maxnrr) ? 4 * n[i] * r[i] * r[i + 1] : maxnrr;
  }

	int lworkd =   Srr                      // lv
               + (2 * rmax) * (2 * rmax)  // work
               + (2 * rmax) * (2 * rmax)  // R
               + 2 * (2 * rmax)           // Singular values
               + maxnrr;                  // work for Orthogonalization/compression/NormHadamardOpt
	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(1);
	}

	int *indices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		exit(1);
	}
	for( int i = 0; i < d; ++i){
		indices[i] = i % n[i];
	}
	double val = 0; 
	/**
	 * Runtime comparison: Left and Right compression 
	 */

  double normInnerProd = attacNormSquaredHadamardSymOpt(d, n, r, x, workd);
  printf("attacNormSquaredHadamardSymOpt = %f\n", sqrt(normInnerProd));
	attacScale(1./sqrt(normInnerProd), 0, d, n, r, x);

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		exit(1);
	}

  // Allocate space for xpx and rxpx
	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		exit(1);
	}
	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; ++i){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; ++i){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			exit(1);
		}
	}
  // Set xpx to x
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j){
		  xpx[i][j] = x[i][j];
    }
  }
	for(int i = 0; i < d + 1; ++i){
    rxpx[i] = r[i];
  }

	/**< Normalizing the tensor */
	double norm = attacNorm(d, n, rxpx, xpx, workd);
  printf("norm = %f\n", norm);


	double alpha = 2.0;
	double beta = -1.0;

	/* Right Orthogonalization */
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	val = attacValue(indices, d, n, r, x, workd);
  /************************************************************
    *
    *
    *
    *
    *
    **********************************************************/
  normInnerProd = attacNormSquaredHadamardSymOpt(d, n, rxpx, xpx, workd);
  printf("attacNormSquaredHadamardSymOpt = %.16f\n", sqrt(normInnerProd));

  normInnerProd = attacNormSquaredHadamardOpt(d, n, rxpx, xpx, workd);
  printf("attacNormSquaredHadamardOpt = %.16f\n", sqrt(normInnerProd));

	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);
  /************************************************************
    *
    *
    *
    *
    *
    **********************************************************/
	double timing_SymNorm = 0, timing_GNorm = 0, timing_OrthoNorm = 0;
	double val_SymNorm = 0, val_GNorm = 0, val_OrthoNorm = 0;

  clock_t begin, end;
	printf("Ranks of x\n");
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);

  int nrun = 50;

  for(int i = 0; i < nrun; ++i){
	  begin = clock();
    val_SymNorm = attacNormSquaredHadamardSymOpt(d, n, rxpx, xpx, workd);
    end = clock() - begin;
	  timing_SymNorm += (double) end/CLOCKS_PER_SEC;
  }
  timing_SymNorm = timing_SymNorm/((double)nrun);
	/*****************/

  for(int i = 0; i < nrun; ++i){
	  begin = clock();
    val_GNorm = attacNormSquaredHadamardOpt(d, n, rxpx, xpx, workd);
    end = clock() - begin;
	  timing_GNorm += (double) end/CLOCKS_PER_SEC;
  }
  timing_GNorm = timing_GNorm/((double)nrun);
	/*****************/

  for(int i = 0; i < nrun; ++i){
	  begin = clock();
    val_OrthoNorm = attacNorm(d, n, rxpx, xpx, workd);
    end = clock() - begin;
	  timing_OrthoNorm += (double) end/CLOCKS_PER_SEC;

	  attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);
  }
  timing_OrthoNorm = timing_OrthoNorm/((double)nrun);
	/*****************/
	printf("/*************************/\n");
	printf("*     Time comparison     *\n");
	printf("/*************************/\n");
	printf("attacNormSquaredHadamardSymOpt: %f\n", timing_SymNorm);
	printf("attacNormSquaredHadamardOpt   : %f\n", timing_GNorm);
	printf("attacNorm                     : %f\n", timing_OrthoNorm);
	printf("/*************************/\n");
	printf("/*************************/\n");
	printf("/*************************/\n");
	printf("*           Values        *\n");
	printf("/*************************/\n");
	printf("attacNormSquaredHadamardSymOpt: %f\n", val_SymNorm);
	printf("attacNormSquaredHadamardOpt   : %f\n", val_GNorm);
	printf("attacNorm                     : %f\n", val_OrthoNorm);
	printf("/*************************/\n");
	printf("/*************************/\n");
	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(xpx[i]);
	}
	free(xpx);
	free(rxpx);
	free(x);
	free(workd);
	free(n);
	free(r);
  return 0;
}
