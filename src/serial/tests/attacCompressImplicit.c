/** \file attacCompressImplicit.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <time.h>
#include <math.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 10;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(0);
	}

	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(0);
	}

	double **x = NULL;
	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(0);
	}

	r[0] = 1;
	r[d] = 1;
	n[0] = 500;
	for(int i = 1; i < d; ++i){
		r[i] = 5 + abs(d/2 - abs(d/2 - i));
		n[i] = 500;// + 100 * i;
	}

	printf("**********************************\n");
	printf("*       attacCompressImplicit      *\n");
	printf("**********************************\n");
	printf("d = %d\n", d);
	printf("**********************************\n");
	for(int i = 0; i < d - 1; ++i){
		printf("%d, ", n[i]);
	}
	printf("%d\n", n[d - 1]);
	printf("**********************************\n");
	for(int i = 0; i < d; ++i){
		printf("%d, ", r[i]);
	}
	printf("%d\n", r[d]);
	printf("**********************************\n");

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	int Sr = 0;
	for(int i = 1; i < d; ++i){
		Sr += r[i];
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			exit(0);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}

	int *worki = NULL;
	worki = (int*) malloc( (d + 1) * sizeof(int));
	if(worki == NULL){
		fprintf(stderr, "worki was not well allocated\n");
		exit(0);
	}

	double *workd = NULL;
	workd = (double*) malloc((
													Sr // v for implicit Qs
													+ 2 * rmax * 2 * rmax // svd in compression (u or vt)
													+ 2 * 2 * rmax // svd in compression (sig and superb) or compute TT value
													+ nmax // norm computation
													+ (2 * rmax) * (2 * rmax) * nmax // core work space in svd
													) * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(0);
	}

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		exit(0);
	}

	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		exit(0);
	}

	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; ++i){
		rxpx[i] = 2 * r[i];
	}

	for(int i = 0; i < d; ++i){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			exit(0);
		}
	}

	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j){
		  xpx[i][j] = x[i][j];
		}
	}
	
	int* indices = NULL;
	indices = (int*) malloc(d * sizeof(int));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		exit(0);
	}

	for(int i = 0; i < d; ++i){
		indices[i] = 2 * i % n[i];
	}

	double val = 0, val2 = 0, val3 = 0, val4 = 0;
	/**
	 * Runtime comparison: Left and Right compression 
	 */

	/**< Normalizing the tensor */
	double norm = attacNorm(d, n, r, xpx, workd);
	attacScale(1./norm, 0, d, n, r, x);

	val = attacValue(indices, d, n, r, x, workd);

	double alpha = 2.;
	double beta = -1.0;

	// Left side
	// Explicit Qs
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);
	val2 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(x) = %e, val(2 x - x) = %e \n", val, val2);

	clock_t begin, end;
	double timing1 = 0, timing2 = 0, timing3 = 0, timing4 = 0;
	double threshold = 1e-5/sqrt((double)(d - 1));

	printf("Ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	/**< Compression Explicit */
	begin = clock();
	attacCompress(d, n, rxpx, xpx, threshold, 'L', workd);
	end = clock();
	timing1 = ((double) end - begin)/CLOCKS_PER_SEC;

	val3 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(L explicit compressed xpx) = %e\n", val3);
	printf("L explicit Compressed ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	// Implict Qs
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	/**< Compression Explicit */
	begin = clock();
	attacCompressImplicitQs(d, n, rxpx, xpx, threshold, 'L', workd);
	end = clock();
	timing2 = ((double) end - begin)/CLOCKS_PER_SEC;

	val4 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(L implicit compressed xpx) = %e\n", val4);
	printf("L implicit Compressed ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	// Right side
	// Explicit Qs
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);
	val2 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(x) = %e, val(2 x - x) = %e \n", val, val2);

	printf("Ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	/**< Compression Explicit */
	begin = clock();
	attacCompress(d, n, rxpx, xpx, threshold, 'R', workd);
	end = clock();
	timing3 = ((double) end - begin)/CLOCKS_PER_SEC;

	val3 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(R explicit compressed xpx) = %e\n", val3);
	printf("R explicit Compressed ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	// Implicit Qs
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	/**< Compression Explicit */
	begin = clock();
	attacCompressImplicitQs(d, n, rxpx, xpx, threshold, 'R', workd);
	end = clock();
	timing4 = ((double) end - begin)/CLOCKS_PER_SEC;

	val4 = attacValue(indices, d, n, rxpx, xpx, workd);
	printf("val(R implicit compressed xpx) = %e\n", val4);
	printf("R implicit Compressed ranks of formal %f x - (%f) x \n", alpha, beta);
	for(int i = 0; i < d + 1; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("\n");

	printf("/*************************/\n");
	printf("*     Time comparison     *\n");
	printf("/*************************/\n");
	printf("attacCompress Left compression Explicit: %f\n", timing1);
	printf("attacCompress Left compression Implicit: %f\n", timing2);
	printf("attacCompress Right compression Explicit: %f\n", timing3);
	printf("attacCompress Right compression Implicit: %f\n", timing4);

	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(xpx[i]);
	}
	free(xpx);
	free(rxpx);
	free(x);
	free(workd);
	free(worki);
	free(n);
	free(r);
  return 0;
}
