# MPI_ATTAC

This is the GIT repository for the work on Algorithms for Tensor Train Arithmetic and Computations.

For more information:
- [Hussam Al Daas](mailto:hussam.al-daas@stfc.ac.uk)
- [Grey Ballard](mailto:ballard@wfu.edu)
- [Peter Benner](mailto:benner@mpi-magdeburg.mpg.de)

# Warning

This code is still in development and we welcome evaluation from expert users. If you have a question, please contact us. You can also submit an issue if you find a bug or request a specific feature.

# Requirements

- MPI implementation
- LAPACK and BLAS (currently require MKL)
- C compiler
- CMake

# Papers

[Parallel Algorithms for Tensor Train Arithmetic.](https://arxiv.org/abs/2011.06532)

**Bibtex:**

```
@TechReport{ABB20,
  author      = {Hussam Al Daas and Grey Ballard and Peter Benner},
  title       = {Parallel Algorithms for Tensor Train Arithmetic},
  institution = {arXiv},
  year        = {2020},
  number      = {2011.06532},
  url         = {https://arxiv.org/abs/2011.06532},
}
```

# Install
    cd mpi_attac
    mkdir build
    export MKL_INCLUDE=< path-to-the-include-directory-of-mkl-on-your-machine>
    cd build
    cmake ../src -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpic++
    make

# Running tests
    make test

# Running an executable
    mpirun -n 4 mpi/tests/bin/pattacGramCompression
